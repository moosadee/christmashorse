# The Christmas Horse Saga

## Download and play

You can download the game for free at itchio: https://moosadee.itch.io/christmas-horse

## Building Christmas Horse from source

*Dependencies (Linux):*

* libxrandr-dev 
* libxcursor-dev 
* libudev-dev 
* libfreetype-dev 
* libopenal-dev 
* libflac-dev 
* libvorbis-dev 
* libgl1-mesa-dev 
* libegl1-mesa-dev
* libsfml-dev

## Screenshots

Christmas Horse 1

![Screenshot](screenshots/horse.png)

Christmas Horse 2

![Screenshot](screenshots/horse2.png)

Christmas Horse 3

![Screenshot](screenshots/horse3.png)

Christmas Horse 4

![Screenshot](screenshots/horse4.png)
