#include "ConfigLoader.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include <fstream>
#include <vector>
#include <stdexcept>

namespace chalo
{

std::map< std::string, std::string > LoadConfig( std::string filename )
{
    std::ifstream input( filename );
    std::map<std::string, std::string> data;

    if ( input.fail() )
    {
        Logger::Error( "Could not open file \"" + filename + "\"!", "LoadConfig" );
        throw std::invalid_argument( "File not found" );
    }

    std::string line;
    while ( getline( input, line ) )
    {
        std::vector<std::string> keyval = Helper::Split( line, "=" );
        data[ keyval[0] ] = keyval[1];
    }

    return data;
}

}
