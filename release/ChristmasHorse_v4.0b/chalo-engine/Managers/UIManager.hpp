// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

// CONVERTING INTO MenuManager

#ifndef _UIMANAGER_HPP
#define _UIMANAGER_HPP

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

#include "../UI/UILabel.hpp"
#include "../UI/UIImage.hpp"
#include "../UI/UIButton.hpp"
#include "../UI/UIShape.hpp"

namespace chalo
{

class UIManager
{
public:
    static std::string CLASSNAME;

    static void Clear();

    // Labels
    static void AddLabel( const std::string& key, UILabel label );
    static void AddLabel( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, const std::string& text );
    static UILabel& GetLabel( const std::string& key );
    static std::map<std::string, UILabel>& GetLabels();

    // Images
    static void AddImage( const std::string& key, UIImage image );
    static void AddImage( const std::string& key, const sf::Texture& texture, sf::Vector2f position );
    static UIImage& GetImage( const std::string& key );
    static std::map<std::string, UIImage>& GetImages();

    // Buttons
    static void AddButton( const std::string& key, UIButton button );
    static UIButton& GetButton( const std::string& key );
    static std::map<std::string, UIButton>& GetButtons();

    // Geometry
    static void AddRectangle( const std::string& key, sf::RectangleShape shape );
    static void AddRectangle( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size );
    static UIRectangleShape& GetRectangle( const std::string& key );
    static std::map<std::string, UIRectangleShape>& GetRectangles();

    // Actions
    static std::string GetClickedButton();

    // Other
    static void RemoveElementsWith( const std::string& str );

private:
    static std::map<std::string, UILabel> m_labels;
    static std::map<std::string, UIImage> m_images;
    static std::map<std::string, UIButton> m_buttons;
    static std::map<std::string, UIRectangleShape> m_rectangleShapes;
};

}

#endif
