// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _CONFIG_MANAGER_HPP
#define _CONFIG_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <map>

namespace chalo
{

class ConfigManager
{
public:
    static std::string CLASSNAME;

    static void Setup( std::string configFilename = "chalo_config.chacha" );
    static void Save();
    static void Cleanup();

    static std::string Get( const std::string& key );
    static int GetInt( const std::string& key );
    static std::map< std::string, std::string > GetPartial( const std::string& partialKey );
    static void Set( const std::string& key, const std::string& value );
    static void Set( const std::string& key, int value );

private:
    static std::map< std::string, std::string > m_configValues;
    static std::string m_configPath;

    static void Create();
};

}

#endif
