// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "TextureManager.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"

#include <stdexcept>

namespace chalo
{

std::string TextureManager::CLASSNAME = "TextureManager";
std::map<std::string, sf::Texture> TextureManager::m_assets;

void TextureManager::Setup()
{
  Add( "DEFAULT", "../chalo-engine/bad-texture.png" );
}

void TextureManager::Add( const std::string& key, const std::string& path )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    sf::Texture texture;
    if ( !texture.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load texture at path \"" + path + "\"", "TextureManager::Add" );
        m_assets[ Helper::ToLower( key ) ] = Get( "DEFAULT" );
        Logger::OutFuncEnd( "nonfatal error", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    m_assets[ Helper::ToLower( key ) ] = texture;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

const sf::Texture& TextureManager::AddAndGet( const std::string& key, const std::string& path )
{
    Add( Helper::ToLower( key ), path );
    return Get( key );
}

void TextureManager::Clear()
{
    m_assets.clear();
}

const sf::Texture& TextureManager::Get( const std::string& key )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    if ( m_assets.find( Helper::ToLower( key ) ) == m_assets.end() )
    {
        // Not found
        std::string allKeys = "<ol>";
        for ( auto& asset : m_assets )
        {
            allKeys += "<li>\"" + asset.first + "\"</li>";
        }
        allKeys += "</ol>";
        Logger::Error( "Could not find texture with key \"" + key + "\"... Current keys are: " + allKeys, "TextureManager::Get" );
        Logger::OutFuncEnd( "nonfatal error", CLASSNAME + "::" + std::string( __func__ ) );
        return Get( "DEFAULT" );
        //throw std::runtime_error( "Could not find texture with key \"" + key + "\" - TextureManager::Get" );
    }

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
    return m_assets[ Helper::ToLower( key ) ];
}

void TextureManager::DebugOut()
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutIntValue( "Total textures", m_assets.size(), CLASSNAME + "::" + std::string( __func__ ) );
  for ( auto& tx : m_assets )
  {
    Logger::OutValue( "Texture key", tx.first );
  }
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

}
