// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _AUDIO_MANAGER
#define _AUDIO_MANAGER

#include <SFML/Audio.hpp>

#include <map>
#include <string>

namespace chalo
{

class AudioManager
{
public:
    static std::string CLASSNAME;

    static void AddMusic( const std::string& key, const std::string& path );
    static void AddSoundBuffer( const std::string& key, const std::string& path );

    static void Clear();

    static const sf::Music& GetMusic( const std::string& key );
    static const sf::SoundBuffer& GetSoundBuffer( const std::string& key );

    static void PlayMusic( const std::string& key, bool repeat = true );
    static void PlaySound( const std::string& key );

private:
    static std::map<std::string, sf::Music> m_music;
    static std::map<std::string, sf::SoundBuffer> m_soundBuffer;
    static std::vector<sf::Sound> m_sfx;
};

}

#endif
