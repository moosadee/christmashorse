#include "MenuManager.hpp"

#include "../Application/Application.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities/CsvParser.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "FontManager.hpp"
#include "ConfigManager.hpp"
#include "LanguageManager.hpp"

#include <fstream>
#include <exception>

namespace chalo
{

std::string MenuManager::CLASSNAME = "MenuManager";
std::string MenuManager::m_menuPathBase;
std::map< std::string, UILayer > MenuManager::m_layers;
std::stack< std::string > MenuManager::m_menuStack;
UITextBox* MenuManager::m_ptrActiveTextbox;

void MenuManager::Setup( const std::string& pathBase )
{
    Logger::OutFuncBegin( "pathBase=" + pathBase, CLASSNAME + "::" + std::string( __func__ ) );
    m_menuPathBase = pathBase;
    m_ptrActiveTextbox = nullptr;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::Cleanup()
{
    m_layers.clear();
}

void MenuManager::DyslexiaFontReload()
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    for ( auto& layer : MenuManager::m_layers )
    {
        for ( auto& button : layer.second.buttons )
        {
            button.second.ToggleDyslexiaFont();
        }
        for ( auto& label : layer.second.labels )
        {
            label.second.ToggleDyslexiaFont();
        }
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

std::string MenuManager::GetClickedButton()
{
    if ( chalo::InputManager::IsLeftClickRelease() )
    {
        // TODO: Could call the GetHoveredButton here
        sf::Vector2i localPosition = chalo::InputManager::GetMousePosition();

        for ( auto& layer : MenuManager::m_layers )
        {
            if ( !layer.second.visible ) { continue; }
            for ( auto& button : layer.second.buttons )
            {
                if ( SFMLHelper::PointInBoxCollision( localPosition, button.second.GetPositionRect() ) )
                {
                    chalo::InputManager::DisableClick();
                    return button.second.GetId();
                }
            }
        }
    }

    return "";
}

std::string MenuManager::GetHoveredButton( sf::Vector2f position )
{
    for ( auto& layer : MenuManager::m_layers )
    {
        if ( !layer.second.visible ) { continue; }
        for ( auto& button : layer.second.buttons )
        {
            if ( SFMLHelper::PointInBoxCollision( position, button.second.GetPositionRect() ) )
            {
//                chalo::InputManager::DisableClick();
                return button.second.GetId();
            }
        }
    }

    return "";
}

std::string MenuManager::GetTextboxValue( const std::string& textboxName )
{
    for ( auto& layer : MenuManager::m_layers )
    {
        if ( !layer.second.visible ) { continue; }
        if ( layer.second.textboxes.find( textboxName ) != layer.second.textboxes.end() )
        {
            // Found
            return layer.second.textboxes[ textboxName ].GetEnteredText();
        }
    }

    return "";
}

void MenuManager::Update()
{
    InputManager::Update();

    // Check to see if any textboxes have been selected
    if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_ptrActiveTextbox = nullptr;
        sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

        for ( auto& layer : MenuManager::m_layers )
        {
            if ( !layer.second.visible ) { continue; }
            for ( auto& element : layer.second.textboxes )
            {
                if ( SFMLHelper::PointInBoxCollision( localPosition, element.second.GetPositionRect() ) )
                {
                    // This textbox has been selected
                    Logger::Out( "Textbox " + element.second.GetId() + " received focus", "MenuManager::Update" );
                    element.second.SetFocus( true );
                    m_ptrActiveTextbox = &element.second;
                }
                else
                {
                    element.second.SetFocus( false );
                }
            }
        }
    }

    // Check for keyboard presses
    if ( m_ptrActiveTextbox != nullptr )
    {
        // Input method :(
        if ( InputManager::IsKeyPressed( sf::Keyboard::BackSpace ) )
        {
            m_ptrActiveTextbox->EraseCharacter();
        }

        std::string typedCharacter = InputManager::GetTypedLetter();
        if ( typedCharacter != "" )
        {
            m_ptrActiveTextbox->AddCharacter( typedCharacter );
        }
    }
}

void MenuManager::LoadCsvMenu( const std::string& filename )
{
    Logger::OutFuncBegin( "filename=" + filename + ", m_menuPathBase=" + m_menuPathBase, CLASSNAME + "::" + std::string( __func__ ) );
    Cleanup();
    m_menuStack.push( filename );

    rach::CsvDocument data;

    try
    {
        data = rach::CsvParser::Parse( m_menuPathBase + filename );
    }
    catch( const runtime_error& ex )
    {
        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        Logger::Error( ex.what(), CLASSNAME + "::" + std::string( __func__ ) );
    }

    Logger::Out( "Pull data from CSV file \"" + m_menuPathBase + filename + "\"", CLASSNAME + "::" + std::string( __func__ ) );
    for ( auto& row : data.rows )
    {
        std::map<std::string, std::string> keyVals;

        std::string logInfo = "Row: <ul>";
        for ( size_t i = 0; i < row.size(); i++ )
        {
            if ( row[i] == "" ) { continue; }
            keyVals[ data.header[i] ] = row[i];
            logInfo += "<li>" + Helper::ToString( i ) + ": " + data.header[i] + "=" + row[i], CLASSNAME + "::" + std::string( __func__ ) + "</li>";
        }
        logInfo += "</ul>";

        if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
        {
          Logger::Out( logInfo, CLASSNAME + "::" + std::string( __func__ ) );
          Logger::Out( "Create UI object from data. Element type: " + keyVals["ELEMENT_TYPE"] + ", name: " + keyVals["ELEMENT_NAME"], CLASSNAME + "::" + std::string( __func__ ) );
        }

        keyVals["ELEMENT_TEXT"] = Helper::Replace( keyVals["ELEMENT_TEXT"], "\"", "" );

        InitLayer( keyVals["LAYER_NAME"] );
        if ( keyVals["ELEMENT_TYPE"] == "rectangleshape" )
        {
            bool visible = true;
            float elementX = 0;
            float elementY = 0;
            int elementWidth = 0;
            int elementHeight = 0;
            int elementBorderThickness = 0;

            try
            {
              visible = ( keyVals[ "IS_VISIBLE" ] == "" ) ? true : Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
              elementX = ( keyVals[ "ELEMENT_X" ] == "" ) ? 0 : Helper::StringToFloat( keyVals[ "ELEMENT_X" ] );
              elementY = ( keyVals[ "ELEMENT_Y" ] == "" ) ? 0 : Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] );
              elementWidth = ( keyVals[ "ELEMENT_WIDTH" ] == "" ) ? 0 : Helper::StringToFloat( keyVals[ "ELEMENT_WIDTH" ] );
              elementHeight = ( keyVals[ "ELEMENT_HEIGHT" ] == "" ) ? 0 : Helper::StringToFloat( keyVals[ "ELEMENT_HEIGHT" ] );
              elementBorderThickness = ( keyVals[ "ELEMENT_BORDER_THICKNESS" ] == "" ) ? 0 : Helper::StringToFloat( keyVals[ "ELEMENT_BORDER_THICKNESS" ] );
            }
            catch( const invalid_argument& ex )
            {
              Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
            }

            AddRectangleShape(
                keyVals["LAYER_NAME"], keyVals["ELEMENT_NAME"],
                GetColorOrDefault( keyVals[ "ELEMENT_BORDER_COLOR_R" ], keyVals[ "ELEMENT_BORDER_COLOR_G" ], keyVals[ "ELEMENT_BORDER_COLOR_B" ], keyVals[ "ELEMENT_BORDER_COLOR_A" ], sf::Color::Black ),
                elementBorderThickness,
                GetColorOrDefault( keyVals[ "ELEMENT_FILL_COLOR_R" ], keyVals[ "ELEMENT_FILL_COLOR_G" ], keyVals[ "ELEMENT_FILL_COLOR_B" ], keyVals[ "ELEMENT_FILL_COLOR_A" ], sf::Color::White ),
                sf::Vector2f( elementX, elementY ),
                sf::Vector2f( elementWidth, elementHeight )
            );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "button" )
        {
            bool asciiText = false;
            std::string elementText = keyVals["ELEMENT_TEXT"];
            std::string fontName = "";
            CreateElementText( elementText, asciiText, fontName );
            if ( fontName == "" ) { fontName = keyVals["ELEMENT_TEXT_FONT"]; }

            bool visible = true;
            float elementX = 0;
            float elementY = 0;
            int elementWidth = 0;
            int elementHeight = 0;
            float elementBackgroundX = 0;
            float elementBackgroundY = 0;
            float elementTextX = 0;
            float elementTextY = 0;
            int elementBackgroundFrameX = 0;
            int elementBackgroundFrameY = 0;
            int elementBackgroundFrameW = 0;
            int elementBackgroundFrameH = 0;
            int elementIconX = 0;
            int elementIconY = 0;
            int elementIconFrameX = 0;
            int elementIconFrameY = 0;
            int elementIconFrameWidth =  0;
            int elementIconFrameHeight = 0;
            int elementTextSize = 0;

            try
            {
              visible                 = ( keyVals[ "IS_VISIBLE" ] == "" )                       ? true : Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
              elementX                = ( keyVals[ "ELEMENT_X" ] == "" )                        ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_X" ] );
              elementY                = ( keyVals[ "ELEMENT_Y" ] == "" )                        ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] );
              elementWidth            = ( keyVals[ "ELEMENT_WIDTH" ] == "" )                    ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_WIDTH" ] );
              elementHeight           = ( keyVals[ "ELEMENT_HEIGHT" ] == "" )                   ? -1  : Helper::StringToFloat( keyVals[ "ELEMENT_HEIGHT" ] );
              elementBackgroundX      = ( keyVals[ "ELEMENT_BACKGROUND_X" ] == "" )             ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_X" ] );
              elementBackgroundY      = ( keyVals[ "ELEMENT_BACKGROUND_Y" ] == "" )             ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_Y" ] );
              elementTextX            = ( keyVals[ "ELEMENT_TEXT_X" ] == "" )                   ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_X" ] );
              elementTextY            = ( keyVals[ "ELEMENT_TEXT_Y" ] == "" )                   ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_Y" ] );
              elementBackgroundFrameX = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] == "" )       ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] );
              elementBackgroundFrameY = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] == "" )       ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] );
              elementBackgroundFrameW = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] == "" )   ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] );
              elementBackgroundFrameH = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] == "" )  ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] );
              elementIconX            = ( keyVals[ "ELEMENT_ICON_X" ] == "" )                   ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_X" ] );
              elementIconY            = ( keyVals[ "ELEMENT_ICON_Y" ] == "" )                   ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_Y" ] );
              elementIconFrameX       = ( keyVals[ "ELEMENT_ICON_FRAME_X" ] == "" )             ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_X" ] );
              elementIconFrameY       = ( keyVals[ "ELEMENT_ICON_FRAME_Y" ] == "" )             ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_Y" ] );
              elementIconFrameWidth   = ( keyVals[ "ELEMENT_ICON_FRAME_WIDTH" ] == "" )         ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_WIDTH" ] );
              elementIconFrameHeight  = ( keyVals[ "ELEMENT_ICON_FRAME_HEIGHT" ] == "" )        ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_HEIGHT" ] );
              elementTextSize         = ( keyVals[ "ELEMENT_TEXT_SIZE" ] == "" )                ? -1 : Helper::StringToInt( keyVals[ "ELEMENT_TEXT_SIZE" ] );
            }
            catch( const invalid_argument& ex )
            {
              Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
            }

            AddButton(
                keyVals["LAYER_NAME"], keyVals["ELEMENT_NAME"],
                sf::Vector2f( elementX, elementY ),
                sf::IntRect( 0, 0, elementWidth, elementHeight ),
                keyVals[ "ELEMENT_BACKGROUND_TEXTURE" ],
                sf::Vector2f( elementBackgroundX, elementBackgroundY ),
                sf::IntRect( elementBackgroundFrameX, elementBackgroundFrameY, elementBackgroundFrameW, elementBackgroundFrameH ),
                fontName,
                elementTextSize,
                GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::White ),
                sf::Vector2f( elementTextX, elementTextY ),
                elementText,
                asciiText,
                keyVals[ "ELEMENT_ICON_TEXTURE" ],
                elementIconX,
                elementIconY,
                elementIconFrameX,
                elementIconFrameY,
                elementIconFrameWidth,
                elementIconFrameHeight,
                visible
            );

        }
        else if ( keyVals["ELEMENT_TYPE"] == "label" )
        {
            bool asciiText = false;
            std::string elementText = keyVals["ELEMENT_TEXT"];
            std::string fontName = "";
            CreateElementText( elementText, asciiText, fontName );
            if ( fontName == "" ) { fontName = keyVals["ELEMENT_TEXT_FONT"]; }

            bool visible = true;
            float elementX = 0;
            float elementY = 0;
            int elementTextSize = 0;

            try
            {
              visible = ( keyVals[ "IS_VISIBLE" ] == "" )                 ? true : Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
              elementTextSize = ( keyVals[ "ELEMENT_TEXT_SIZE" ] == "" )  ? 20 : Helper::StringToInt( keyVals[ "ELEMENT_TEXT_SIZE" ] );
              elementX = ( keyVals[ "ELEMENT_X" ] == "" )                 ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_X" ] );
              elementY = ( keyVals[ "ELEMENT_Y" ] == "" )                 ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] );
            }
            catch( const invalid_argument& ex )
            {
              Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
            }

            AddLabel(
                keyVals["LAYER_NAME"], keyVals["ELEMENT_NAME"],
                GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::White ),
                fontName,
                elementTextSize,
                sf::Vector2f( elementX, elementY ),
                elementText,             // const std::string& elementText
                asciiText,
                visible
            );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "image" )
        {
            bool visible = true;
            float elementX = 0;
            float elementY = 0;
            int elementWidth = 0;
            int elementHeight = 0;
            int elementBackgroundFrameX = 0;
            int elementBackgroundFrameY = 0;
            int elementBackgroundFrameW = 0;
            int elementBackgroundFrameH = 0;

            try
            {
              visible                 = ( keyVals[ "IS_VISIBLE" ] == "" )                       ? true : Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
              elementX                = ( keyVals[ "ELEMENT_X" ] == "" )                        ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_X" ] );
              elementY                = ( keyVals[ "ELEMENT_Y" ] == "" )                        ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] );
              elementWidth            = ( keyVals[ "ELEMENT_WIDTH" ] == "" )                    ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_WIDTH" ] );
              elementHeight           = ( keyVals[ "ELEMENT_HEIGHT" ] == "" )                   ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_HEIGHT" ] );
              elementBackgroundFrameX = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] == "" )       ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] );
              elementBackgroundFrameY = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] == "" )       ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] );
              elementBackgroundFrameW = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] == "" )   ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] );
              elementBackgroundFrameH = ( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] == "" )  ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] );
            }
            catch( const invalid_argument& ex )
            {
              Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
            }

            AddImage(
                keyVals["LAYER_NAME"], keyVals["ELEMENT_NAME"],
                keyVals[ "ELEMENT_BACKGROUND_TEXTURE" ],
                sf::Vector2f(elementX, elementY ),
                sf::IntRect( elementBackgroundFrameX, elementBackgroundFrameY, elementBackgroundFrameW, elementBackgroundFrameH ),
                visible
            );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "textbox" )
        {
            bool asciiText = false;
            std::string elementText = keyVals["ELEMENT_TEXT"];
            std::string fontName = "";
            CreateElementText( elementText, asciiText, fontName );

            bool  visible = true;
            float elementX                = 0;
            float elementY                = 0;
            int   elementWidth            = 0;
            int   elementHeight           = 0;
            int   elementBorderThickness  = 0;
            int   elementTextSize         = 0;
            float elementTextX            = 0;
            float elementTextY            = 0;

            try
            {
              visible         = ( keyVals[ "IS_VISIBLE" ] == "" )         ? true : Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
              elementX        = ( keyVals[ "ELEMENT_X" ] == "" )          ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_X" ] );
              elementY        = ( keyVals[ "ELEMENT_Y" ] == "" )          ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] );
              elementWidth    = ( keyVals[ "ELEMENT_WIDTH" ] == "" )      ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_WIDTH" ] );
              elementHeight   = ( keyVals[ "ELEMENT_HEIGHT" ] == "" )     ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_HEIGHT" ] );
              elementTextSize = ( keyVals[ "ELEMENT_TEXT_SIZE" ] == "" )  ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_SIZE" ] );
              elementTextX    = ( keyVals[ "ELEMENT_TEXT_X" ] == "" )     ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_X" ] );
              elementTextY    = ( keyVals[ "ELEMENT_TEXT_Y" ] == "" )     ? -1 : Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_Y" ] );
            }
            catch( const invalid_argument& ex )
            {
              Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
            }

            AddTextBox(
                keyVals["LAYER_NAME"], keyVals["ELEMENT_NAME"],
                sf::Vector2f( elementX, elementY ),
                sf::IntRect( 0, 0, elementWidth, elementHeight ),
                GetColorOrDefault( keyVals[ "ELEMENT_BORDER_COLOR_R" ], keyVals[ "ELEMENT_BORDER_COLOR_G" ], keyVals[ "ELEMENT_BORDER_COLOR_B" ], keyVals[ "ELEMENT_BORDER_COLOR_A" ], sf::Color::Black ),
                elementBorderThickness,
                GetColorOrDefault( keyVals[ "ELEMENT_FILL_COLOR_R" ], keyVals[ "ELEMENT_FILL_COLOR_G" ], keyVals[ "ELEMENT_FILL_COLOR_B" ], keyVals[ "ELEMENT_FILL_COLOR_A" ], sf::Color::White ),
                GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::Black ),
                fontName,
                elementTextSize,
                sf::Vector2f( elementTextX, elementTextY ),
                elementText
            );
        }
    }

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::LoadTextMenu( const std::string& filename )
{
    Logger::OutFuncBegin( "filename=" + filename + ", m_menuPathBase=" + m_menuPathBase, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::Error( "THIS FUNCTION IS DEPRECATED! USE LoadCsvMenu INSTEAD!", CLASSNAME + "::" + std::string( __func__ ) );
//    Cleanup();
//
//    m_menuStack.push( filename );
//
//    std::ifstream infile( m_menuPathBase + filename );
//    if ( infile.fail() )
//    {
//        Logger::Error( "Could not load menu " + m_menuPathBase + filename, CLASSNAME + "::" + std::string( __func__ ) );
//        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
//        return;
//    }
//
//    std::string buffer;
//    std::string type;
//    std::string elementName;
//    std::string elementType;
//    std::string layerName;
//    std::string elementText;
//    std::wstring elementTextW;
//    std::string elementBackgroundTextureName;
//    std::string fontName;
//    bool visible = true;
//    int x, y, width, height;
//    int backgroundOffsetX, backgroundOffsetY;
//    int textOffsetX, textOffsetY;
//    int borderR, borderG, borderB, borderA;
//    int fillR, fillG, fillB, fillA;
//    int textR, textG, textB, textA;
//    int imageLeft, imageTop, imageWidth, imageHeight;
//    int borderThickness;
//    int textSize;
//    std::string iconTexture;
//    int iconX, iconY;
//    int iconLeft, iconTop, iconWidth, iconHeight;
//    bool asciiText = true;
//
//    while ( infile >> buffer )
//    {
//        if          ( buffer == "LAYER_NAME" )                  { infile >> layerName; }
//        else if     ( buffer == "ELEMENT_NAME" )                { infile >> elementName; }
//        else if     ( buffer == "ELEMENT_TYPE" )                { infile >> elementType; }
//        else if     ( buffer == "ELEMENT_VISIBLE" )             { infile >> visible; }
//        else if     ( buffer == "ELEMENT_X" )                   { infile >> x; }
//        else if     ( buffer == "ELEMENT_Y" )                   { infile >> y; }
//        else if     ( buffer == "ELEMENT_WIDTH" )               { infile >> width; }
//        else if     ( buffer == "ELEMENT_HEIGHT" )              { infile >> height; }
//        else if     ( buffer == "ELEMENT_BORDER_COLOR_R" )      { infile >> borderR; }
//        else if     ( buffer == "ELEMENT_BORDER_COLOR_G" )      { infile >> borderG; }
//        else if     ( buffer == "ELEMENT_BORDER_COLOR_B" )      { infile >> borderB; }
//        else if     ( buffer == "ELEMENT_BORDER_COLOR_A" )      { infile >> borderA; }
//        else if     ( buffer == "ELEMENT_FILL_COLOR_R" )        { infile >> fillR; }
//        else if     ( buffer == "ELEMENT_FILL_COLOR_G" )        { infile >> fillG; }
//        else if     ( buffer == "ELEMENT_FILL_COLOR_B" )        { infile >> fillB; }
//        else if     ( buffer == "ELEMENT_FILL_COLOR_A" )        { infile >> fillA; }
//        else if     ( buffer == "ELEMENT_TEXT_COLOR_R" )        { infile >> textR; }
//        else if     ( buffer == "ELEMENT_TEXT_COLOR_G" )        { infile >> textG; }
//        else if     ( buffer == "ELEMENT_TEXT_COLOR_B" )        { infile >> textB; }
//        else if     ( buffer == "ELEMENT_TEXT_COLOR_A" )        { infile >> textA; }
//        else if     ( buffer == "ELEMENT_BORDER_THICKNESS" )    { infile >> borderThickness; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_TEXTURE" )  { infile >> elementBackgroundTextureName; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_X" )        { infile >> backgroundOffsetX; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_Y" )        { infile >> backgroundOffsetY; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_X" )          { infile >> imageLeft; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_Y" )          { infile >> imageTop; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_WIDTH" )      { infile >> imageWidth; }
//        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_HEIGHT" )     { infile >> imageHeight; }
//
//        else if     ( buffer == "ELEMENT_ICON_TEXTURE" )        { infile >> iconTexture; }
//        else if     ( buffer == "ELEMENT_ICON_X" )              { infile >> iconX; }
//        else if     ( buffer == "ELEMENT_ICON_Y" )              { infile >> iconY; }
//        else if     ( buffer == "ELEMENT_ICON_FRAME_X" )        { infile >> iconLeft; }
//        else if     ( buffer == "ELEMENT_ICON_FRAME_Y" )        { infile >> iconTop; }
//        else if     ( buffer == "ELEMENT_ICON_FRAME_WIDTH" )    { infile >> iconWidth; }
//        else if     ( buffer == "ELEMENT_ICON_FRAME_HEIGHT" )   { infile >> iconHeight; }
////        else if     ( buffer == "ELEMENT_ASCII" )               { infile >> asciiText; }
//
//        else if     ( buffer == "ELEMENT_TEXT" )
//        {
//            infile.ignore();
//
//            }
//        }
//        else if     ( buffer == "ELEMENT_TEXT_FONT" )           { infile >> fontName; }
//        else if     ( buffer == "ELEMENT_TEXT_X" )              { infile >> textOffsetX; }
//        else if     ( buffer == "ELEMENT_TEXT_Y" )              { infile >> textOffsetY; }
//        else if     ( buffer == "ELEMENT_TEXT_SIZE" )           { infile >> textSize; }
//        else if     ( buffer == "ELEMENT_IMAGECLIP_LEFT" )      { infile >> imageLeft; }
//        else if     ( buffer == "ELEMENT_IMAGECLIP_TOP" )       { infile >> imageTop; }
//        else if     ( buffer == "ELEMENT_IMAGECLIP_WIDTH" )     { infile >> imageWidth; }
//        else if     ( buffer == "ELEMENT_IMAGECLIP_HEIGHT" )    { infile >> imageHeight; }
//
//        else if ( buffer == "ELEMENT_END" )
//        {
//            InitLayer( layerName );
//
//            if ( elementType == "rectangleshape" )
//            {
//                sf::Color borderColor = sf::Color( borderR, borderG, borderB, borderA );
//                sf::Color fillColor = sf::Color( fillR, fillG, fillB, fillA );
//
//                // AddRectangleShape( const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::vector2f dimensions )
//                AddRectangleShape( layerName, elementName, borderColor, borderThickness, fillColor, sf::Vector2f( x, y ), sf::Vector2f( width, height ) );
//            }
//            else if ( elementType == "button" )
//            {
//                sf::Color fillColor = sf::Color( textR, textG, textB, textA );
//
//                AddButton(
//                    layerName,                                                      // const std::string& layerName
//                    elementName,                                                    // const std::string& elementName
//                    sf::Vector2f( x, y ),                                           // sf::Vector2f position
//                    sf::IntRect( 0, 0, width, height ),                             // sf::IntRect dimensions
//                    elementBackgroundTextureName,                                   // const std::string& backgroundTexture
//                    sf::Vector2f( backgroundOffsetX, backgroundOffsetY ),           // sf::Vector2f bgOffset
//                    sf::IntRect( imageLeft, imageTop, imageWidth, imageHeight ),    // sf::IntRect frameRect
//                    fontName,                                                       // const std::string& fontName
//                    textSize,                                                       // int textSize
//                    fillColor,                                                      // sf::Color textColor
//                    sf::Vector2f( textOffsetX, textOffsetY ),                       // sf::Vector2f textOffset
//                    elementText,                                                    // const std::string& elementText
//                    asciiText,
//                    iconTexture,
//                    iconX,
//                    iconY,
//                    iconLeft,
//                    iconTop,
//                    iconWidth,
//                    iconHeight,
//                    visible
//                );
//            }
//            else if ( elementType == "label" )
//            {
//                sf::Color fillColor = sf::Color( textR, textG, textB, textA );
//
//                AddLabel(
//                    layerName,              // const std::string& layerName
//                    elementName,            // const std::string& elementName
//                    fillColor,              // sf::Color textColor
//                    fontName,               // const std::string& fontName
//                    textSize,               // int textSize
//                    sf::Vector2f( x, y ),   // sf::Vector2f position
//                    elementText,             // const std::string& elementText
//                    asciiText,
//                    visible
//                );
//            }
//            else if ( elementType == "image" )
//            {
//                AddImage(
//                    layerName,                                                      // const std::string& layerName
//                    elementName,                                                    // const std::string& elementName
//                    elementBackgroundTextureName,                                   // const std::string& textureName
//                    sf::Vector2f( x, y ),                                           // sf::Vector2f position
//                    sf::IntRect( imageLeft, imageTop, imageWidth, imageHeight ),    // sf::IntRect frameRect
//                    visible
//                );
//            }
//            else if ( elementType == "textbox" )
//            {
//                sf::Color borderColor = sf::Color( borderR, borderG, borderB, borderA );
//                sf::Color fillColor = sf::Color( fillR, fillG, fillB, fillA );
//                sf::Color textColor = sf::Color( textR, textG, textB, textA );
//
//                // AddTextBox( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Color textColor, int textSize, sf::Vector2f textOffset, const std::string& elementText )
//                AddTextBox( layerName, elementName, sf::Vector2f( x, y ), sf::IntRect( 0, 0, width, height ), borderColor, borderThickness, fillColor, textColor, fontName, textSize, sf::Vector2f( textOffsetX, textOffsetY ), elementText );
//            }
//
//            // Resets
//            elementText = "";
//            iconTexture = "";
//            imageLeft = -1;
//            imageTop = -1;
//            imageWidth = -1;
//            imageHeight = -1;
//            visible = true;
//            asciiText = true;
//        }
//    }
//
//    int totalCount;
//
//    totalCount = 0;
//
//    for ( auto& layer : MenuManager::m_layers )
//    {
//        totalCount += layer.second.shapes.size();
//        totalCount += layer.second.buttons.size();
//        totalCount += layer.second.images.size();
//        totalCount += layer.second.labels.size();
//        totalCount += layer.second.textboxes.size();
//    }
//
//    Logger::Out( "Total layers: " + Helper::ToString( m_layers.size() ), CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::InitLayer( std::string key )
{
    if ( m_layers[key].name == "" )
    {
        m_layers[key].name = key;
        m_layers[key].visible = true;
    }
}

bool MenuManager::GoBack()
{
    if ( m_menuStack.size() > 1 )
    {
        m_menuStack.pop();
        LoadTextMenu( m_menuStack.top() );
        return true;
    }

    return false;
}

void MenuManager::AddRectangleShape( const std::string& layerName, const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f dimensions )
{
    UIRectangleShape rectangle;

    rectangle.Setup( elementName, borderColor, borderThickness, fillColor, position, dimensions );
    m_layers[layerName].shapes[elementName] = rectangle;

    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
    {
      Logger::Out( "Adding new RectangleShape element: [" + layerName + "][" + elementName + "]", "MenuManager::AddRectangleShape", "file-parsing" );
    }
}

void MenuManager::AddButton(
    const std::string& layerName,
    const std::string& elementName,
    sf::Vector2f position,
    sf::IntRect dimensions,
    std::string backgroundTexture,
    sf::Vector2f bgOffset,
    sf::IntRect frameRect,
    std::string fontName,
    int textSize,
    sf::Color textColor,
    sf::Vector2f textOffset,
    std::string elementText,
    bool asciiText,
    std::string iconTexture,
    int iconX,
    int iconY,
    int iconLeft,
    int iconTop,
    int iconWidth,
    int iconHeight,
    bool visible /* = true */
)
{
    Logger::OutFuncBegin( "backgroundTexture=" + backgroundTexture + ", iconTexture=" + iconTexture, CLASSNAME + "::" + std::string( __func__ ) );
    UIButton button;

    button.Setup( elementName, position, dimensions );
    button.SetupBackground( chalo::TextureManager::Get( backgroundTexture ), bgOffset, frameRect );

    if ( elementText != "" )
    {
        button.SetOriginalFont( fontName );
        if ( asciiText )
        {
            button.SetupText( fontName, textSize, textColor, textOffset, elementText );
        }
        else
        {
            button.SetupTextW( fontName, textSize, textColor, textOffset, Helper::StringToWString( elementText ) );
        }
    }

    if ( iconTexture != "" )
    {
        sf::Vector2f offsetPosition;
        offsetPosition.x = iconX;
        offsetPosition.y = iconY;
        sf::IntRect imageClip;
        imageClip.left = iconLeft;
        imageClip.top = iconTop;
        imageClip.width = iconWidth;
        imageClip.height = iconHeight;
        button.SetupIcon( chalo::TextureManager::Get( iconTexture ), offsetPosition, imageClip );
    }

    button.SetIsVisible( visible );

    m_layers[layerName].buttons[elementName] = button;

    Logger::OutFuncEnd( "", CLASSNAME + "::" + std::string( __func__ ) );
}


void MenuManager::AddButton(
    const std::string& layerName,
    const std::string& elementName,
    UIButton button
)
{
    m_layers[layerName].buttons[elementName] = button;
//    Logger::Out( "Adding new Button element: [" + layerName + "][" + elementName + "]", "MenuManager::AddButton", "file-parsing" );
}

void MenuManager::AddLabel(
    const std::string& layerName,
    const std::string& elementName,
    sf::Color textColor,
    const std::string& fontName,
    int textSize,
    sf::Vector2f position,
    const std::string& elementText,
    bool asciiText,
    bool visible
)
{
    Logger::OutFuncBegin( "elementName=" + elementName, CLASSNAME + "::" + std::string( __func__ ) );
    UILabel label;
    label.SetOriginalFont( fontName );

    if ( asciiText )
    {
        label.Setup( elementName, fontName, textSize, textColor, position, elementText );
    }
    else
    {
        label.SetupW( elementName, fontName, textSize, textColor, position, Helper::StringToWString( elementText ) );
    }
    label.SetIsVisible( visible );

//    Logger::Out( "Adding new Label element: [" + layerName + "][" + elementName + "]", "MenuManager::AddLabel", "file-parsing" );
    m_layers[layerName].labels[elementName] = label;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::AddImage(
    const std::string& layerName,
    const std::string& elementName,
    const std::string& textureName,
    sf::Vector2f position,
    sf::IntRect frameRect,
    bool visible
)
{
    UIImage image;

    // void Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position, sf::IntRect imageClip );
    image.Setup( elementName, chalo::TextureManager::Get( textureName ), position, frameRect );
    image.SetIsVisible( visible );

//    Logger::Out( "Adding new Image element: [" + layerName + "][" + elementName + "]", "MenuManager::AddImage", "file-parsing" );

    m_layers[layerName].images[elementName] = image;
}

void MenuManager::AddTextBox( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Color textColor, const std::string& fontName, int textSize, sf::Vector2f textOffset, const std::string& elementText )
{
    UITextBox textbox;

    textbox.Setup( elementName, position, dimensions );
    textbox.SetupBackground( borderColor, borderThickness, fillColor, position, sf::Vector2f( dimensions.width, dimensions.height ) );
    textbox.SetupText( fontName, textSize, textColor, textOffset, elementText );

//    Logger::Out( "Adding new TextBox element: [" + layerName + "][" + elementName + "]", "MenuManager::AddTextBox", "file-parsing" );

    m_layers[layerName].textboxes[elementName] = textbox;
}

UILabel& MenuManager::GetLabel( const std::string& layer, const std::string& name )
{
    return m_layers[layer].labels[name];
}

UILabel& MenuManager::GetLabel( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.labels.find( name ) != layer.second.labels.end() )
        {
            return layer.second.labels[name];
        }
    }

    Logger::Error( "Cannot find label with key \"" + name + "\"", "MenuManager::GetLabel" );
    throw std::out_of_range( "Cannot find label with key \"" + name + "\"" );
}

UIImage& MenuManager::GetImage( const std::string& layer, const std::string& name )
{
    return m_layers[layer].images[name];
}

UIImage& MenuManager::GetImage( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.images.find( name ) != layer.second.images.end() )
        {
            return layer.second.images[name];
        }
    }

    Logger::Error( "Cannot find image with key \"" + name + "\"", "MenuManager::GetImage" );
    throw std::out_of_range( "Cannot find image with key \"" + name + "\"" );
}

UIButton& MenuManager::GetButton( const std::string& layer, const std::string& name )
{
    return m_layers[layer].buttons[name];
}

UIButton& MenuManager::GetButton( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.buttons.find( name ) != layer.second.buttons.end() )
        {
            return layer.second.buttons[name];
        }
    }

    Logger::Error( "Cannot find button with key \"" + name + "\"", "MenuManager::GetButton" );
    throw std::out_of_range( "Cannot find button with key \"" + name + "\"" );
}

UIRectangleShape& MenuManager::GetRectangleShape( const std::string& layer, const std::string& name )
{
    return m_layers[layer].shapes[name];
}

UIRectangleShape& MenuManager::GetRectangleShape( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.shapes.find( name ) != layer.second.shapes.end() )
        {
            return layer.second.shapes[name];
        }
    }

    Logger::Error( "Cannot find shape with key \"" + name + "\"", "MenuManager::GetShape" );
    throw std::out_of_range( "Cannot find shape with key \"" + name + "\"" );
}

UITextBox& MenuManager::GetTextBox( const std::string& layer, const std::string& name )
{
    return m_layers[layer].textboxes[name];
}

UITextBox& MenuManager::GetTextBox( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.textboxes.find( name ) != layer.second.textboxes.end() )
        {
            return layer.second.textboxes[name];
        }
    }

    Logger::Error( "Cannot find textbox with key \"" + name + "\"", "MenuManager::GetTextBox" );
    throw std::out_of_range( "Cannot find textbox with key \"" + name + "\"" );
}

void MenuManager::ShowAllLayers()
{
    for ( auto& layer : m_layers )
    {
        layer.second.visible = true;
    }
}


void MenuManager::HideAllLayers()
{
    for ( auto& layer : m_layers )
    {
        layer.second.visible = false;
    }
}

void MenuManager::TurnOnLayers( std::vector< std::string > layerNames )
{
    for ( auto& layer : m_layers )
    {
        for ( auto& name : layerNames )
        {
            if ( name == layer.second.name )
            {
                layer.second.visible = true;
            }
        }
    }
}

void MenuManager::TurnOffLayers( std::vector< std::string > layerNames )
{
    for ( auto& layer : m_layers )
    {
        for ( auto& name : layerNames )
        {
            if ( name == layer.second.name )
            {
                layer.second.visible = false;
            }
        }
    }
}

std::vector< std::string > MenuManager::GetVisibleLayerNames()
{
    std::vector< std::string > visibleLayers;

    for ( auto& layer : m_layers )
    {
        if ( layer.second.visible )
        {
            visibleLayers.push_back( layer.second.name );
        }
    }

    return visibleLayers;
}

bool MenuManager::IsLayerVisible( std::string name )
{
    if ( m_layers.find( name ) == m_layers.end() )
    {
        return false;
    }

    return ( m_layers[ name ].visible );
}

sf::Color MenuManager::GetColorOrDefault( std::string r, std::string g, std::string b, std::string a, sf::Color def )
{
    if ( r == "" ) { return def; }

    int num_r = 0;
    int num_g = 0;
    int num_b = 0;
    int num_a = 0;

    try
    {
      num_r = ( r == "" ) ? 0 : Helper::StringToInt( r );
      num_g = ( g == "" ) ? 0 : Helper::StringToInt( g );
      num_b = ( b == "" ) ? 0 : Helper::StringToInt( b );
      num_a = ( a == "" ) ? 0 : Helper::StringToInt( a );
    }
    catch( const invalid_argument& ex )
    {
      Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
    }

    return sf::Color( num_r, num_g, num_b, num_a );
}

void MenuManager::CreateElementText( std::string& elementText, bool& asciiText, std::string& fontName )
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    asciiText = false; // Just load everything to work with unicode??

    if ( elementText == "" )
    {
        // Nothing to build
        Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }
    elementText = Helper::Trim( elementText );

    if ( Helper::Contains( elementText, "<<" ) )
    {
        // Replace variable values with config data
        std::string original = elementText;
        int varStart = elementText.find( "<<" ) + 2;
        int varEnd = elementText.find( ">>" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = ConfigManager::Get( variableName );
        elementText = Helper::Replace( elementText, "<<" + variableName + ">>", value );
    }
    else if ( Helper::Contains( elementText, "{{" ) )
    {
        // Replace variable values with language items
        std::string original = elementText;
        int varStart = elementText.find( "{{" ) + 2;
        int varEnd = elementText.find( "}}" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), variableName );
        elementText = Helper::Replace( elementText, "{{" + variableName + "}}", value );

        // Change font
        fontName = chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" );
    }
    else if ( Helper::Contains( elementText, "[[" ) )
    {
        // Replace variable values with language items
        std::string original = elementText;
        int varStart = elementText.find( "[[" ) + 2;
        int varEnd = elementText.find( "]]" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), variableName );
        elementText = Helper::Replace( elementText, "[[" + variableName + "]]", value );

        // Change font
        fontName = chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" );
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

}
