#include "ConfigManager.hpp"

#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"
#include "../Data/ConfigLoader.hpp"

#include <fstream>

namespace chalo
{

std::string ConfigManager::CLASSNAME = "ConfigManager";
std::map< std::string, std::string > ConfigManager::m_configValues;
std::string ConfigManager::m_configPath;

void ConfigManager::Setup( std::string configFilename /*= "chalo_config.chacha"*/ )
{
    Logger::OutFuncBegin( "configFilename=" + configFilename, "ConfigManager::" + std::string( __func__ ) );
    m_configPath = configFilename;

    try
    {
        m_configValues = LoadConfig( m_configPath );
    }
    catch( const invalid_argument& ex )
    {
        Create();
    }

    Logger::OutFuncEnd( "normal", "ConfigManager::" + std::string( __func__ ) );
}

void ConfigManager::Save()
{

    std::ofstream output( m_configPath );
    for ( auto& setting : m_configValues )
    {
        output << setting.first << "=" << setting.second << std::endl;
    }
    output.close();

}

void ConfigManager::Cleanup()
{

    Save();

}

void ConfigManager::Create()
{

    Logger::Out( "No config found, create new one...", "ConfigManager::Create" );

    // Default values
    m_configValues["WINDOW_WIDTH"] = "1280";
    m_configValues["WINDOW_HEIGHT"] = "720";
    m_configValues["MENU_PATH"] = "../Content/Menus/";
    m_configValues["LANGUAGE_PATH"] = "../Content/Languages/";
    m_configValues["FULLSCREEN"] = "0";
    m_configValues["SOUND_VOLUME"] = "100";
    m_configValues["MUSIC_VOLUME"] = "50";
    m_configValues["USE_OPEN_DYSLEXIC"] = "0";
    m_configValues["USE_SUBTITLES"] = "1";
    m_configValues["USE_CAPTIONS"] = "0";
    m_configValues["PLATFORM"] = "COMPY";
    m_configValues["PREVENT_JOYSTICK"] = "1";
    m_configValues["LANGUAGE"] = "0";
    m_configValues["LANGUAGE_MAIN_FONT"] = "0";
    m_configValues["DEBUG_LOG"] = "0";

    Save();


}

std::string ConfigManager::Get( const std::string& key )
{
    if ( m_configValues.find( key ) != m_configValues.end() )
    {
        // Found
        return m_configValues[ key ];
    }
    return "";
}

int ConfigManager::GetInt( const std::string& key )
{
    std::string value = Get( key );
    return Helper::StringToInt( value );
}

std::map< std::string, std::string > ConfigManager::GetPartial( const std::string& partialKey )
{
    std::map< std::string, std::string > matchingOptions;

    for ( auto& option : m_configValues )
    {
        if ( Helper::Contains( option.first, partialKey ) )
        {
            matchingOptions[ option.first ] = option.second;
        }
    }

    return matchingOptions;
}

void ConfigManager::Set( const std::string& key, const std::string& value )
{
    m_configValues[ key ] = value;
    Save();
}

void ConfigManager::Set( const std::string& key, int value )
{
    Set( key, Helper::ToString( value ) );
}


};
