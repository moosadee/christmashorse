// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "UIManager.hpp"

#include "../Application/Application.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"

namespace chalo
{

std::string UIManager::CLASSNAME = "UIManager";
std::map<std::string, UILabel> UIManager::m_labels;
std::map<std::string, UIImage> UIManager::m_images;
std::map<std::string, UIButton> UIManager::m_buttons;
std::map<std::string, UIRectangleShape> UIManager::m_rectangleShapes;

void UIManager::Clear()
{
    m_labels.clear();
}

// ------------------------------------------------------
// Labels
// ------------------------------------------------------
void UIManager::AddLabel( const std::string& key, UILabel label )
{
//    Logger::Out( "Adding label with key \"" + key + "\"", "UIManager::AddLabel" );
    m_labels[ key ] = label;
}

void UIManager::AddLabel( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, const std::string& text  )
{
    UILabel label;
    label.Setup( key, fontName, characterSize, fillColor, position, text );
    m_labels[ key ] = label;
}

UILabel& UIManager::GetLabel( const std::string& key )
{
    return m_labels[ key ];
}

std::map<std::string, UILabel>& UIManager::GetLabels()
{
    return m_labels;
}


// ------------------------------------------------------
// Images
// ------------------------------------------------------
void UIManager::AddImage( const std::string& key, UIImage image )
{
//    Logger::Out( "Adding image with key \"" + key + "\"", "UIManager::AddImage" );
    m_images[ key ] = image;
}

void UIManager::AddImage( const std::string& key, const sf::Texture& texture, sf::Vector2f position )
{
    UIImage image;
    image.Setup( key, texture, position );
    m_images[ key ] = image;
}

UIImage& UIManager::GetImage( const std::string& key )
{
    return m_images[ key ];
}

std::map<std::string, UIImage>& UIManager::GetImages()
{
    return m_images;
}


// ------------------------------------------------------
// Buttons
// ------------------------------------------------------
void UIManager::AddButton( const std::string& key, UIButton button )
{
//    Logger::Out( "Adding button with key \"" + key + "\"", "UIManager::AddButton" );
    m_buttons[ key ] = button;
}

UIButton& UIManager::GetButton( const std::string& key )
{
    return m_buttons[ key ];
}

std::map<std::string, UIButton>& UIManager::GetButtons()
{
    return m_buttons;
}

// ------------------------------------------------------
// Geometry
// ------------------------------------------------------

void UIManager::AddRectangle( const std::string& key, sf::RectangleShape shape )
{
//    Logger::Out( "Adding shape with key \"" + key + "\"", "UIManager::AddRectangle" );
    UIRectangleShape rectShape;
    rectShape.Setup( key, shape );
    m_rectangleShapes[ key ] = rectShape;
}

void UIManager::AddRectangle( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size )
{
    UIRectangleShape shape;
    shape.Setup( key, borderColor, borderThickness, fillColor, position, size );
    m_rectangleShapes[ key ] = shape;
}

UIRectangleShape& UIManager::GetRectangle( const std::string& key )
{
    return m_rectangleShapes[ key ];
}

std::map<std::string, UIRectangleShape>& UIManager::GetRectangles()
{
    return m_rectangleShapes;
}

std::string UIManager::GetClickedButton()
{
    if ( sf::Mouse::isButtonPressed(sf::Mouse::Left) )
    {
        sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

        for ( auto& button : m_buttons )
        {
            if ( SFMLHelper::PointInBoxCollision( localPosition, button.second.GetPositionRect() ) )
            {
                return button.second.GetId();
            }
        }
    }

    return "";
}

void UIManager::RemoveElementsWith( const std::string& str )
{
    std::vector<std::string> elementNames;

    // Labels
    for ( auto& element : m_labels )
    {
        if ( Helper::Contains( element.first, str ) )
        {
            // Remove this element
            elementNames.push_back( element.first );
        }
    }

    for ( auto& key : elementNames )
    {
        m_labels.erase( key );
    }

    elementNames.clear();

    // Buttons
    for ( auto& element : m_buttons )
    {
        if ( Helper::Contains( element.first, str ) )
        {
            // Remove this element
            elementNames.push_back( element.first );
        }
    }

    for ( auto& key : elementNames )
    {
        m_buttons.erase( key );
    }

    elementNames.clear();
}

}
