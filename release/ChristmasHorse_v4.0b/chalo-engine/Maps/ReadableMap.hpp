//// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine
//
//#ifndef _MAP
//#define _MAP
//
//#include "Tile.hpp"
//#include "../Utilities/Logger.hpp"
//#include "../Utilities/Helper.hpp"
//#include "../Managers/TextureManager.hpp"
//
//#include "Warp.hpp"
//#include "TileLayer.hpp"
//#include "Shadow.hpp"
//#include "Collision.hpp"
//#include "Placement.hpp"
//
//#include <vector>
//#include <map>
//#include <string>
//#include <fstream>
//#include <iostream>
//#include <stack>
//#include <list>
//
//#include "Shadow.hpp"
//
//// enum LayerType { UNDEFINEDLAYER = 0, BELOWTILELAYER = 1, ABOVETILELAYER = 2, WARPLAYER = 3, PLACEMENTLAYER = 4, COLLISIONLAYER = 5, SHADOWLAYER = 6 };
//
//namespace chalo
//{
//
//namespace Map
//{
//
//class ReadableMap
//{
//public:
//    ReadableMap();
//
//    void LoadTextMap( const std::string& mapPath, const std::string& filename );
//
//    void Reset();
//    void Update();
//
//    const std::map<int, Map::TileLayer >& GetBelowTileLayers() const;
//    const std::map<int, Map::TileLayer >& GetAboveTileLayers() const;
//    const std::map<int, Map::ShadowLayer>& GetShadows() const;
//    const std::vector<Map::Collision>& GetCollisions() const;
//    const std::vector<Map::Warp>& GetWarps() const;
//    const std::vector<Map::Placement>& GetPlacements() const;
//
//    sf::Vector2f GetPlayerStartingPosition() const;
//    std::string GetTilesetName() const;
//    int GetWidth() const;
//    int GetHeight() const;
//    int GetTileWidth() const;
//
//    bool IsCollision( sf::IntRect desiredPosition, sf::IntRect collisionRegion ) const;
//    bool PlayerAtWarp( sf::IntRect collisionRegion );
//    Map::Warp GetWarpTo( sf::IntRect collisionRegion ) const;
//
//protected:
//    int m_mapWidth;
//    int m_mapHeight;
//    int m_tileWidth;
//
//    std::string m_mapPath;
//    std::string m_mapFile;
//
//    std::string m_tileset;
//    std::string m_tilesetCollisions;
//    std::string m_tilesetShadows;
//    std::string m_tilesetPlacements;
//
//    std::map<int, Map::TileLayer> m_belowTileLayers;
//    std::map<int, Map::TileLayer> m_aboveTileLayers;
//    std::map<int, Map::ShadowLayer> m_shadows;
//    std::vector<Map::Collision> m_collisions;
//    std::vector<Map::Warp> m_warps;
//    std::vector<Map::Placement> m_placements;
//
//    bool debugFirst;
//    int m_currentlyAtWarp;
//    sf::Vector2f m_playerStartingPosition;
//};
//
//}
//
//}
//
//#endif
