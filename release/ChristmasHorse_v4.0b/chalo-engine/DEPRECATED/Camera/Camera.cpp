#include "Camera.hpp"
#include "../../Utilities/Logger.hpp"
#include "../../Utilities_SFML/SFMLHelper.hpp"

namespace chalo
{

Camera::Camera()
{
    m_speed = 5;

    m_visibleRegion.width = 640;
    m_visibleRegion.height = 480;
    m_visibleRegion.left = 0;
    m_visibleRegion.top = 0;

    m_position.x = 0;
    m_position.y = 0;

    m_regionBound.left = -1;
    m_regionBound.top = -1;
    m_regionBound.width = -1;
    m_regionBound.height = -1;
}

void Camera::SetDimensions( int width, int height )
{
    m_visibleRegion.width = width;
    m_visibleRegion.height = height;
    m_visibleRegion.left = 0;
    m_visibleRegion.top = 0;
}

void Camera::SetDimensions( sf::IntRect viewRectangle )
{
    m_visibleRegion = viewRectangle;
}


void Camera::SetPosition( float x, float y )
{
    SetPosition( sf::Vector2f( x, y ) );
}

void Camera::SetPosition( sf::Vector2f position )
{
    m_position = position;
}

void Camera::SetSpeed( float speed )
{
    m_speed = speed;
}

//! Set the region where the camera can be within; don't let it go outside these bounds.
void Camera::SetRegionBound( sf::IntRect region )
{
    m_regionBound = region;
}

void Camera::CenterOn( float x, float y )
{
    CenterOn( sf::Vector2f( x, y ) );
}

void Camera::CenterOn( sf::Vector2f point )
{
    m_position.x = point.x - m_visibleRegion.width / 2;
    m_position.y = point.y - m_visibleRegion.height / 2;
    AdjustToRegionBound();
}

void Camera::Pan( Direction direction )
{
    if ( direction == NORTH )
    {
        m_position.y -= m_speed;
    }
    else if ( direction == SOUTH )
    {
        m_position.y += m_speed;
    }
    else if ( direction == WEST )
    {
        m_position.x -= m_speed;
    }
    else if ( direction == EAST )
    {
        m_position.x += m_speed;
    }
}

sf::Vector2f Camera::GetPosition() const
{
    return m_position;
}

float Camera::GetX() const
{
    return m_position.x;
}

float Camera::GetY() const
{
    return m_position.y;
}

sf::IntRect Camera::GetCameraViewRect() const
{
    return sf::IntRect(
        GetCameraLeftSide(),
        GetCameraTopSide(),
        m_visibleRegion.width,
        m_visibleRegion.height
        );
}

void Camera::AdjustToRegionBound()
{
    if ( GetCameraLeftSide() < m_regionBound.left )
    {
        SetCameraLeftSide( m_regionBound.left );
    }

    if ( GetCameraRightSide() > m_regionBound.left + m_regionBound.width )
    {
        SetCameraRightSide( m_regionBound.left + m_regionBound.width );
    }

    if ( GetCameraTopSide() < m_regionBound.top )
    {
        SetCameraTopSide( m_regionBound.top );
    }

    if ( GetCameraBottomSide() > m_regionBound.top + m_regionBound.height )
    {
        SetCameraBottomSide( m_regionBound.top + m_regionBound.height );
    }

    Logger::Out( "Camera view rectangle: " + SFMLHelper::RectangleToString( GetCameraViewRect() ), "Camera::AdjustToRegionBound" );

    Logger::Out( "View bounding region: " + SFMLHelper::RectangleToString( m_regionBound ), "Camera::AdjustToRegionBound" );
}

int Camera::GetCameraLeftSide() const
{
    return m_position.x + m_visibleRegion.left;
}

int Camera::GetCameraRightSide() const
{
    return m_position.x + m_visibleRegion.left + m_visibleRegion.width;
}

int Camera::GetCameraTopSide() const
{
    return m_position.y + m_visibleRegion.top;
}

int Camera::GetCameraBottomSide() const
{
    return m_position.y + m_visibleRegion.top + m_visibleRegion.height;
}

void Camera::SetCameraLeftSide( int value )
{
    m_position.x = value;
}

void Camera::SetCameraRightSide( int value )
{
//    m_position.x = value;
}

void Camera::SetCameraTopSide( int value )
{
    m_position.y = value;
}

void Camera::SetCameraBottomSide( int value )
{
//    m_position.y = value;
}


}
