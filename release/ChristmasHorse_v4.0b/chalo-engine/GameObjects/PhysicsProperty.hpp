#ifndef _PHYSICS_PROPERTY
#define _PHYSICS_PROPERTY

#include <SFML/Graphics.hpp>

namespace chalo
{

class PhysicsProperty
{
    public:
    PhysicsProperty();
    void Setup( const sf::Vector2f& accRate, const sf::Vector2f& deaccRate, const sf::Vector2f& maxVelocity );
    void SetMaxVelocity( const sf::Vector2f& maxVelocity );
    void SetVelocityX( float value );
    void SetVelocityY( float value );
    void Accelerate( const sf::Vector2f& directions );
    void Deaccelerate( bool horizontally, bool vertically );

    const sf::Vector2f& GetVelocity();

    private:
    sf::Vector2f m_deaccelerationRate;
    sf::Vector2f m_accelerationRate;
    sf::Vector2f m_maxVelocity;
    sf::Vector2f m_velocity;
};

}

#endif
