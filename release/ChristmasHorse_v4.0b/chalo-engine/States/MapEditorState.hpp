//#ifndef _MAPEDITORSTATE
//#define _MAPEDITORSTATE
//
//#include "CursorState.hpp"
//#include "../Managers/TextureManager.hpp"
//#include "../Managers/FontManager.hpp"
//#include "../Maps/Brush.hpp"
//#include "../Maps/WritableMap.hpp"
//
//#include <vector>
//
//class MapEditorState : public chalo::CursorState
//{
//public:
//    MapEditorState();
//
//    virtual void Init( const std::string& name );
//    virtual void Setup();
//    virtual void Cleanup();
//    virtual void Update();
//    virtual void Draw( sf::RenderWindow& window );
//
//private:
//    void ResetTabs();
//    void OpenTab( std::string name );
//    void SetupTileset( std::string name );
//
//    void CheckClickTools();
//
//    void InitNewMap();
//    void LoadMap();
//    void SaveMap();
//
//    void SetBrush( sf::IntRect imageClip );
//
//    Brush m_brush;
//    std::string m_activeTab;
//    std::string m_mapName;
//
////    chalo::Map::WritableMap m_map;
//};
//
//#endif
