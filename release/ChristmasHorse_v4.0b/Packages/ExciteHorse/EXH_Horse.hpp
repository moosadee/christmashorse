#ifndef _HORSE_HPP
#define _HORSE_HPP

#include <SFML/Graphics.hpp>

//#include "../../Objects/ProfileCharacter.hpp"
#include "../../chalo-engine/Enums/Types.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"

namespace ExciteHorse
{

enum class ClipClop { CLIP, CLOP };

class Horse : public chalo::Character
{
    public:
    Horse();
    void Update();
    void Clip();
    void Clop();
    void SetScrollRate( float rate );
    bool IsJumping();
    void BeginJump();
    void EndJump();
    void Rotate( float amount );
    void EnforceBoundary();
    void SetBoundary( sf::IntRect boundary );
    void OnIncline();
    void DrawDebug( sf::RenderWindow& window );
    void Draw();
    void IsColliding( bool val );
    void Move( chalo::Direction direction, const std::vector<int>& m_trackYs );
    void Move( chalo::Direction direction  );
    void HitGround();
    void Bounce( sf::IntRect otherPos );
    void Bounce();
    void FallBack( float screenScroll );
    void Speedup();
    void Slowdown();
    void CrossFinishline();
    bool CanDrop();
    void Dropped();
    void SetRotation( float amount );

    sf::Text& GetNameText();
    void SetProfileKey( std::string profileKey );
    void SetNameOffset( sf::Vector2f offset );
    std::string GetProfileName();

    protected:
    sf::Vector2f m_acceleration;
    sf::Vector2f m_velocity;
    sf::Vector2f m_maxAcceleration;
    sf::Vector2f m_maxVelocity;
    sf::Vector2f m_accelerateStep;
    sf::Vector2f m_inclineAccelerate;
    float m_rotateSpeed;
    float m_accelerationDecay;
    float m_velocityDecay;
    float m_gravity;
    float m_screenScrollRate;
    float m_jumpTimer;
    float m_jumpInitialY;
    float m_speedupTimer;
    sf::IntRect m_moveBoundary;
    ClipClop m_clipclop;
    std::string m_state;
    bool m_isColliding;
    bool m_badJump;
    bool m_crossedFinishLine;
    float m_dropTimer;
    float m_angle;

    void Accelerate();

    void SetupNameText();

    std::string m_profileKey;
    std::string m_profileName;
    sf::Text m_nameText;
    sf::Vector2f m_namePositionOffset;
};

}

#endif
