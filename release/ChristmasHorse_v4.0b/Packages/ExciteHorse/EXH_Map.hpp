#ifndef _EXCITE_HORSE_MAP
#define _EXCITE_HORSE_MAP

#include "EXH_Incline.hpp"

#include <vector>

namespace ExciteHorse
{

struct ObstaclePlacement
{
    ObstaclePlacement();
    ObstaclePlacement( int nx, int ny, ObstacleType nt );

    int x, y;
    ObstacleType type;
};

struct Map
{
    Map();

    std::vector<ObstaclePlacement> placements;
};

}

#endif
