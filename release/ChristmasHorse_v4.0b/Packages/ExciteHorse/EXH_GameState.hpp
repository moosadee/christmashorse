//#ifndef _EXCITE_HORSE_GAMESTATE
//#define _EXCITE_HORSE_GAMESTATE
//
//#include "../../chalo-engine/States/IState.hpp"
//#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/Maps/WritableMap.hpp"
//#include "../../chalo-engine/Managers/TextureManager.hpp"
//#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/Managers/DrawManager.hpp"
//#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
//
//#include "EXH_Horse.hpp"
//#include "EXH_Incline.hpp"
//#include "EXH_Map.hpp"
//
//#include <SFML/Audio.hpp>
//#include <vector>
//
//class ExciteHorseGameState : public chalo::IState //: public VamooseState
//{
//public:
//    ExciteHorseGameState();
//
//    virtual void Init( const std::string& name );
//    virtual void Setup();
//    virtual void Cleanup();
//    virtual void Update();
//    virtual void Draw( sf::RenderWindow& window );
//
//private:
//    std::vector<ExciteHorse::Horse> m_players;
//    std::vector<sf::Sprite> m_track;
//    std::vector<ExciteHorse::TrackObstacle> m_obstacles;
//    ExciteHorse::Map m_mapSpec;
//    float m_screenScrollRate;
//    std::vector<int> m_trackYs;
//    int m_firstToFinish;
//
//    std::map<std::string, sf::SoundBuffer> m_soundBuffers;
//    std::map<std::string, sf::Sound> m_sounds;
//
//    void BuildMap();
//    void AddObstacle( ExciteHorse::ObstacleType type, sf::Vector2f pos );
//
//    std::string GetProfileAvatarSheet();
//};
//
//#endif
