#ifndef _ENUMS
#define _ENUMS

enum class GamePhase
{
    BUILDING,
    WAVERUN,
    WAVEENDING,
    GAMELOST,
    GAMEWON
};

#endif
