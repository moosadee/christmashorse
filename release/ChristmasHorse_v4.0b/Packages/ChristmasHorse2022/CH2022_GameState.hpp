#ifndef _CH2022_GAME_STATE
#define _CH2022_GAME_STATE

#include <SFML/Audio.hpp>
#include <vector>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include "CH2022_Character.hpp"
#include "CH2022_Enums.hpp"

class CH2022_GameState : public EscapeyState//public chalo::IState
{
public:
    CH2022_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void SetupUI();
    void NextWave();
    void EndWave();
    void GameOver();
    void YouWin();

    void UpdateBuildingPhase();
    void UpdateWavePhase();

    void SpawnActor( string type, int x, int y );
    void SpawnActor( string type, sf::IntRect position );
    int GetEnemyCount();

private:
    std::string CLASSNAME;
    sf::Music m_music;

    CH2022_Actor m_base;
    std::vector<CH2022_Actor> m_allies;
    std::vector<CH2022_Actor> m_enemies;

    float m_timer;
    float m_unitSpawnCounter;
    float m_unitSpawnCounterMax;
    int m_endWaveCooldown;
    int m_endWaveCooldownMax;
    int m_wave;
    int m_waveMax;
    GamePhase m_phase;

    sf::Sprite m_background;
    sf::Sprite m_brush;
    std::vector<sf::Text> m_statusTexts;
    sf::Text m_statusName;
    sf::Text m_timerText;
    sf::RectangleShape m_statusBackground;
    sf::Sprite m_statusImage;
    sf::Text m_brushText;

    sf::IntRect m_playArea;

    void SetStatus( const std::vector<std::string>& texts, string image );

    std::map<int, std::string> m_unitNames;
    std::map<int, int> m_unitCounts;
    std::map<std::string, sf::SoundBuffer> m_soundBuffer;
    std::map<std::string, sf::Sound> m_soundEffects;

    int m_currentUnitBrush;
};

#endif
