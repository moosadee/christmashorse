#include "CH2024_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Managers/EffectManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

CH2024_GameState::CH2024_GameState()
    : m_accRate( 0.5 ), m_accRateMax( 20 )
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2024_GameState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2024_GameState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();
    std::string gfxPath = "Packages/ChristmasHorse2024/Graphics/";
    std::string sndPath = "Packages/ChristmasHorse2024/Audio/";

    srand( time( NULL ) );

    chalo::DrawManager::SetBackgroundColor( sf::Color( 182, 201, 216 ) );

    m_background.setPosition( 0, 0 );
    m_background.setTexture( chalo::TextureManager::AddAndGet( "background",        gfxPath + "UI/background.png" ) );
    m_foreground.setTexture( chalo::TextureManager::AddAndGet( "foreground",        gfxPath + "UI/just-foreground.png" ) );
    m_holly.setTexture( chalo::TextureManager::AddAndGet(      "holly",             gfxPath + "UI/holly.png" ) );
    chalo::TextureManager::Add(                                "game-icons",        "Packages/ProgramBase/Graphics/UI/game-icons.png" );
    chalo::TextureManager::Add(                                "btn-tabs",          gfxPath + "UI/btn-tabs.png" );
    chalo::TextureManager::Add(                                "bg-clipboard",      gfxPath + "UI/bg-clipboard.png" );
    chalo::TextureManager::Add(                                "bg-balloon",        gfxPath + "UI/bg-balloon.png" );
    chalo::TextureManager::Add(                                "items-bg",          gfxPath + "UI/items-bg.png" );
    chalo::TextureManager::Add(                                "bg-led",            gfxPath + "UI/bg-led.png" );
    chalo::TextureManager::Add(                                "trophies",          gfxPath + "UI/trophies.png" );
    chalo::TextureManager::Add(                                "items-grid",        gfxPath + "Items/items-grid.png" );
    chalo::TextureManager::Add(                                "default-cursor",    "Packages/ChristmasHorse2023/Graphics/items/mouse-cursor.png" );

    m_holly.setPosition( 1155,  559 );
    m_horse.setTexture( chalo::TextureManager::AddAndGet(      "horse",             gfxPath + "Horse/horse.png" ) );
    m_horse.setTextureRect( sf::IntRect( 0, 0, 226, 286 ) );
    m_horse.setColor( sf::Color::White );

    m_player.money = 0;
    m_horseStats.cloneCount = 0;
    HorseReset();

    m_player.cursor.setTexture( chalo::TextureManager::Get( "default-cursor" ) );

    chalo::MenuManager::LoadCsvMenu( "ch2024_gameui.csv" );

    m_menuColors["Tab1"] = sf::Color( 252, 186, 199 );
    m_menuColors["Tab2"] = sf::Color( 252, 223, 186 );
    m_menuColors["Tab3"] = sf::Color( 252, 246, 186 );
    m_menuColors["Tab4"] = sf::Color( 208, 252, 186 );
    m_menuColors["Tab5"] = sf::Color( 186, 247, 252 );
    m_menuColors["Tab6"] = sf::Color( 210, 186, 252 );
    m_menuColors["Tab7"] = sf::Color( 252, 186, 219 );

    m_windowRegion = sf::IntRect( 50, 50, 850, 450 );

    HollyTip( "We want to get Horse as fit, smart, and beautiful as possible! Once we win all competitions we will know that Horse is the ultimate Horse and will be ready to deploy!" );

    chalo::MenuManager::TurnOffLayers( { "btnTab1", "btnTab2", "btnTab3", "btnTab4", "btnTab5", "btnTab6", "btnTab7" } );
    chalo::MenuManager::TurnOnLayers( { "btnTab1" } );

    chalo::Effect defaultEffect;
    defaultEffect.lifeCounter = 200;
    defaultEffect.behavior = chalo::Behavior::FLOAT_UP;
    defaultEffect.text.setFont( chalo::FontManager::Get( "main" ) );
    defaultEffect.text.setFillColor( sf::Color::White );
    defaultEffect.text.setCharacterSize( 20 );
    defaultEffect.text.setOutlineColor( sf::Color::Black );
    defaultEffect.text.setOutlineThickness( 2 );
    chalo::EffectManager::SetupEffectDefault( defaultEffect );

    m_globalTimer = 0;
    UpdateStatsLabels();

    chalo::AudioManager::AddSoundBuffer( "yay", sndPath + "yay.ogg" );
    chalo::AudioManager::AddSoundBuffer( "success", sndPath + "success.ogg" );
    chalo::AudioManager::AddSoundBuffer( "damage", sndPath + "damage.ogg" );
    chalo::AudioManager::AddSoundBuffer( "oof", sndPath + "oof.ogg" );
    chalo::AudioManager::AddSoundBuffer( "questlose", sndPath + "questlose.ogg" );
    chalo::AudioManager::AddSoundBuffer( "questwin", sndPath + "questwin.ogg" );
    chalo::AudioManager::AddSoundBuffer( "trophywin", sndPath + "trophywin.ogg" );
    chalo::AudioManager::AddSoundBuffer( "nom", sndPath + "nom.ogg" );
    chalo::AudioManager::AddSoundBuffer( "seeya", sndPath + "seeya.ogg" );
    chalo::AudioManager::AddSoundBuffer( "questtime", sndPath + "questtime.ogg" );
    chalo::AudioManager::AddSoundBuffer( "youwin", "ExciteHorse/audioyouwin.ogg" );

    chalo::AudioManager::AddMusic( "active", sndPath + "nb_chorse.ogg" );
    chalo::AudioManager::PlayMusic( "active" );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2024_GameState::Cleanup()
{
    Logger::Out( "", "CH2024::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::AudioManager::StopAllMusic();
    chalo::DrawManager::Reset();
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2024_GameState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();
    chalo::EffectManager::Update();

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) )// && !IsPaused() )
    {
        TogglePause( true );
    }

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    Logger::OutValue( "IsPaused()", ( IsPaused() ) ? "true" : "false", CLASSNAME + "::" + std::string( __func__ ) );
    // PAUSE MENU
    if ( IsPaused() )
    {
        if ( clickedButton == "btnPlay" )
        {
            EscapeyState::TogglePause( false );
            chalo::MenuManager::LoadCsvMenu( "ch2024_gameui.csv" );
            RestoreUnlocks();
            SwitchTab( "btnTab1" );
        }
        else if ( clickedButton == "btnMainMenu" )
        {
            Logger::OutHighlight( "Clicked main menu button", CLASSNAME + "::" + std::string( __func__ ) );
            SetGotoState( "startupstate" );
        }

        return;
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) ) // TODO: This all needs to be fixed up.
    {
        EscapeyState::TogglePause( true );
    }

    // GAME UPDATES
    sf::Vector2i mousePos = chalo::InputManager::GetMousePosition();

    if ( Helper::Contains( clickedButton, "Tab", false ) )
    {
        SwitchTab( clickedButton );
    }
    else if ( Helper::Contains( clickedButton, "btnFood" ) ||
              Helper::Contains( clickedButton, "btnToy" ) ||
              Helper::Contains( clickedButton, "btnHealth" ) ||
              Helper::Contains( clickedButton, "btnHygiene" )
            )
    {
        SpawnObject( clickedButton );
    }
    else if ( Helper::Contains( clickedButton, "btnQuest" ) )
    {
        HorseQuestStart( Helper::Replace( clickedButton, "btnQuest-", "" ) );
    }
    else if ( Helper::Contains( clickedButton, "btnCompetition" ) )
    {
        HorseCompetitionStart( Helper::Replace( clickedButton, "btnCompetition-", "" ) );
    }
    else if ( Helper::Contains( clickedButton, "btnHat" ) )
    {
        HorseWearHat( Helper::Replace( clickedButton, "btnHat-", "" ) );
    }

    if ( chalo::InputManager::IsRightClick() )
    {
        RestoreDefaultCursor();
    }

    m_player.cursor.setPosition( mousePos.x, mousePos.y );

    UpdateHorse();
    UpdateObjects();
    HorseQuestUpdate();
    HorseCompetitionUpdate();
    HorseDeathUpdate();

    m_globalTimer++;
    if ( int( m_globalTimer ) % 300 == 0 )
    {
        // Decrease Horse stats
        HorseEntropy();
    }

    // DEBUG
    chalo::MenuManager::GetLabel( "hud", "lblDebug2" ).SetText( "Action: " + m_horseStats.action + ", countdown: " + to_string( m_horseStats.countdown ) );
    chalo::MenuManager::GetLabel( "hud", "lblDebug" ).SetText( "State: " + m_horseStats.state + ", timer: " + to_string( m_horseStats.stateTimer ) );
}

void CH2024_GameState::Draw( sf::RenderWindow& window )
{
    if ( IsPaused() )
    {
        window.setMouseCursorVisible( true );
    }
    else
    {
        window.setMouseCursorVisible( false );
    }

    window.clear( sf::Color( 187, 187, 187, 255 ) );
    window.draw( m_background );
    chalo::DrawManager::AddMenu();
    for ( auto& tip : m_hollyTips )
    {
        chalo::DrawManager::AddUILabel( tip );
    }
    for ( auto& obj : m_objects )
    {
        chalo::DrawManager::AddSprite( obj.sprite );
    }
    chalo::DrawManager::AddSprite( m_horse );
    chalo::DrawManager::AddCursor( m_player.cursor );
    chalo::EffectManager::Draw( window );
    chalo::DrawManager::AddSprite( m_holly );
    chalo::DrawManager::AddSprite( m_foreground );
    chalo::DrawManager::Draw( window );

    EscapeyState::Draw( window );
}

void CH2024_GameState::RestoreDefaultCursor()
{
    // Restore the default cursor
    m_player.cursor.setTexture( chalo::TextureManager::Get( "default-cursor" ) );
    m_player.cursor.setTextureRect( sf::IntRect( 0, 0, 30, 30 ) );
}

void CH2024_GameState::SpawnObject( std::string btnName )
{
    string objectType = Helper::Replace( btnName, "btnFood-", "" );
    objectType = Helper::Replace( objectType, "btnToy-", "" );
    objectType = Helper::Replace( objectType, "btnHealth-", "" );
    objectType = Helper::Replace( objectType, "btnHygiene-", "" );
//    Logger::Out( "Spawn object, button: \"" + btnName + "\", object name: \"" + objectType + "\"", CLASSNAME + "::" + std::string( __func__ ) );

    int randX = rand() % m_windowRegion.width + m_windowRegion.left;
    CH2024_Object newObject;
    newObject.sprite.setOrigin( 45, 45 );
    newObject.velocity = sf::Vector2f( ( rand() % 3 - 2 ) * m_accRateMax, 0 );
    newObject.name = objectType;
    newObject.deleteMe = false;


    chalo::UIButton butt;
    try
    {
        butt = chalo::MenuManager::GetButton( btnName );
    }
    catch( const std::out_of_range& ex )
    {
        Logger::Error( "Error finding button: \"" + btnName + "\": " + ex.what(), CLASSNAME + "::" + std::string( __func__ ) );
    }

//    Logger::Out( "Frame Rect: " + SFMLHelper::RectangleToString( butt.GetFrameRect() ), CLASSNAME + "::" + std::string( __func__ ) );

    newObject.sprite.setTexture( chalo::TextureManager::Get( "items-grid" ) );
    newObject.sprite.setTextureRect( butt.GetIconImageClipRect() );

    newObject.sprite.setPosition( randX, m_windowRegion.top );
    m_objects.push_back( newObject );
    HollyTip( "Sending out " + objectType + "!" );
}

void CH2024_GameState::UpdateObjects()
{
    int groundY = m_windowRegion.top + m_windowRegion.height - 100;
    std::vector<size_t> removalIndices;
    for ( size_t o = 0; o < m_objects.size(); o++ )
    {
        // MOVEMENT / PHYSICS:
        // Deaccelerate
        if      ( m_objects[o].velocity.x < -m_accRate ) { m_objects[o].velocity.x += m_accRate; }
        else if ( m_objects[o].velocity.x > m_accRate  ) { m_objects[o].velocity.x -= m_accRate; }
        else                                             { m_objects[o].velocity.x = 0; }

        // Bind to regions
        if      ( m_objects[o].velocity.x < -m_accRateMax ) { m_objects[o].velocity.x = -m_accRateMax; }
        else if ( m_objects[o].velocity.x > m_accRateMax  ) { m_objects[o].velocity.x = m_accRateMax; }
        if      ( m_objects[o].velocity.y < -m_accRateMax ) { m_objects[o].velocity.y = -m_accRateMax; }
        else if ( m_objects[o].velocity.y > m_accRateMax  ) { m_objects[o].velocity.y = m_accRateMax; }

        // Update position
        sf::Vector2f pos = m_objects[o].sprite.getPosition();
        pos.x += m_objects[o].velocity.x;
        pos.y += m_objects[o].velocity.y;

        // Bounce off sides
        if ( pos.x < m_windowRegion.left )
        {
            m_objects[o].velocity.x = -m_objects[o].velocity.x;
            pos.x = m_windowRegion.left;
        }
        else if ( pos.x > m_windowRegion.left + m_windowRegion.width )
        {
            m_objects[o].velocity.x = -m_objects[o].velocity.x;
            pos.x = m_windowRegion.left + m_windowRegion.width;
        }

        // Detect ground
        if ( pos.y >= groundY )
        {
            // Bounce off ground
            pos.y = groundY;
            m_objects[o].velocity.y = -m_objects[o].velocity.y + ( m_accRate * 5 );
        }
        else
        {
            // Free fall
            m_objects[o].velocity.y += m_accRate;
        }

        m_objects[o].sprite.setPosition( pos.x, pos.y );

        // INTERACTION WITH HORSE:
        sf::Vector2f horseFeet = m_horse.getPosition();
        horseFeet.y += m_horse.getTextureRect().height / 2;
        if ( SFMLHelper::GetDistance( m_objects[o].sprite.getPosition(), horseFeet ) < 40 )
        {
            m_objects[o].deleteMe = true;
            HorseInteract( m_objects[o].name );
        }
    }

    for ( size_t o = 0; o < m_objects.size(); o++ )
    {
        if ( m_objects[o].deleteMe )
        {
            m_objects.erase( m_objects.begin() + o );
            break;
        }
    }
}

void CH2024_GameState::HorseQuestStart( string objectType )
{
    Logger::Out( "Start Quest: " + objectType );
    if ( Helper::Contains( m_horseStats.action, "quest" )
            || Helper::Contains( m_horseStats.action, "compet" )
            || m_horseStats.stats["health"] == 0 )
    {
        return;
    }
    if ( m_horseStats.stats["happy"] < 50 )
    {
        HollyTip( "Horse doesn't want to quest right now. Maybe try cheering 'em up!" );
        chalo::EffectManager::AddEffect( "Horse refuses", sf::Color::Cyan, m_horse.getPosition() );
        return;
    }

    int randSound = rand() % 2;
    if ( randSound == 0 )
    {
        chalo::AudioManager::PlaySound( "seeya" );
    }
    else
    {
        chalo::AudioManager::PlaySound( "questtime" );
    }
    // New quest
    m_horseStats.countdown = 200;
    m_horseStats.action = "quest-" + Helper::ToLower( objectType );
    sf::Vector2f tipPos( m_horse.getPosition().x - 100, m_horse.getPosition().y );
    if ( tipPos.x < 10 ) { tipPos.x = 10; }

    if ( objectType == "Union" )
    {
        HollyTip( "A new quest! Horse is heading off to bust some unions!" );
        chalo::EffectManager::AddEffect( "Quest: Union busting", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Poach" )
    {
        HollyTip( "A new quest! Horse is going to poach some eggs from nearby penguins." );
        chalo::EffectManager::AddEffect( "Quest: Egg poaching", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Repo" )
    {
        HollyTip( "A new quest! Horse is going to repossess items from someone to pay off their debt to us." );
        chalo::EffectManager::AddEffect( "Quest: Repo Horse", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Ghost" )
    {
        HollyTip( "A new quest! Horse is going to post ghost jobs to a job board in order to make the company's numbers look better - and threaten existing employees to work harder." );
        chalo::EffectManager::AddEffect( "Quest: Ghost job poster", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Monsan" )
    {
        HollyTip( "A new quest! Horse is going to inspect a farmer's crop to make sure they're not using patented seeds illegally." );
        chalo::EffectManager::AddEffect( "Quest: Mon-santa seed inspector", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Health" )
    {
        HollyTip( "A new quest! Horse is going to reject health care claims for our insurance branch." );
        chalo::EffectManager::AddEffect( "Quest: Health insurance denier", sf::Color::Yellow, tipPos );
    }
    else if ( objectType == "Influence" )
    {
        HollyTip( "A new quest! Horse is an internet influencer, you know!" );
        chalo::EffectManager::AddEffect( "Quest: Internet influencer", sf::Color::Yellow, tipPos );
    }
}

void CH2024_GameState::HorseQuestUpdate()
{
    if ( Helper::Contains( m_horseStats.action, "quest" ) == false ) { return; }

    if ( m_horseStats.countdown > 0 )
    {
        m_horseStats.countdown--;
        if ( m_horseStats.countdown == 0 )
        {
            // End quest
            sf::Vector2f pos = m_horse.getPosition();
            pos.x = 400;
            m_horse.setPosition( pos.x, pos.y );

            // Result?
            HorseQuestFinish();
        }
    }

    if ( m_horseStats.countdown > 50 )
    {
        // Move horse off screen
        sf::Vector2f pos = m_horse.getPosition();
        pos.x -= 10;
        m_horse.setPosition( pos.x, pos.y );
    }
}

void CH2024_GameState::HorseQuestFinish()
{
    // TODO: Update this to figure out success based on Horse stats
    int result = rand() % 5;
    int randomPay = rand() % 500 + 50;
    int randomProfit = rand() % 1000 + 100;
    sf::Vector2f tipPos( m_horse.getPosition().x - 100, m_horse.getPosition().y );
    if ( tipPos.x < 10 ) { tipPos.x = 10; }

    if ( m_horseStats.action == "quest-union" )
    {
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Union busting - Failure!", sf::Color::Red, tipPos );
            HollyTip( "Horse was unable to stop the elves from unionizing! We will have to pay higher wages!" );
            tipPos.y += 25; PayMoney( randomPay, tipPos );
            tipPos.y += 25; UpdateStat( "glam", -1, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else if ( result == 1 )
        {
            chalo::EffectManager::AddEffect( "Quest: Union busting - Failure!", sf::Color::Red, tipPos );
            HollyTip( "Horse was unable to hire scabs to replace our workforce. This will cost us $" + to_string( randomPay ) + "!" );
            tipPos.y += 25; PayMoney( randomPay, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Union busting - Success!", sf::Color::Green, tipPos );
            HollyTip( "Horse successfully stopped the elves from unionizing by buying them pizza! (And firing those trying to organize!) We saved $" + to_string( randomProfit ) + "!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }

        if ( result > 1 )
        {
            int randomUnlock = rand() % 3;
            if      ( randomUnlock == 0 ) { Unlock( "btnFood-PepPizza", "Unlocked PepPizza!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnToy-Puzzle", "Unlocked Puzzle!" ); }
            else if ( randomUnlock == 2 ) { Unlock( "btnHat-Monocle", "Unlocked Monocle (Hat)!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-poach" )
    {
        if ( result == 0 )
        {
            int healthLost = rand() % 2 + 1;
            chalo::EffectManager::AddEffect( "Quest: Egg poaching - Failure! (-" + to_string( healthLost ) + " health)", sf::Color::Red, tipPos );
            HollyTip( "The penguins chased Horse away from their nests! How brutish! Horse lost some health!" );
            tipPos.y += 25; UpdateStat( "health", -healthLost, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            tipPos.y += 25; UpdateStat( "hygiene", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questlose" );

            int statusChange = rand() % 2;
            if ( result == 0 )
            {
                tipPos.y += 25; UpdateState( "Got bitten!", "bitten", tipPos, sf::Color::Red );
            }
        }
        else if ( result == 1 )
        {
            chalo::EffectManager::AddEffect( "Quest: Egg poaching - Success! (+1 strength)", sf::Color::Green, tipPos );
            HollyTip( "We were able to \"obtain\" some penguin eggs! And Horse looks stronger!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "strength", 1, tipPos );
            tipPos.y += 25; UpdateStat( "hunger", -( rand() % 2 + 1 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Egg poaching - Success!", sf::Color::Green, tipPos );
            HollyTip( "We were able to \"obtain\" some penguin eggs!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "hunger", -( rand() % 2 + 1 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );

        }

        if ( result > 0 )
        {
            int randomUnlock = rand() % 3;
            if      ( randomUnlock == 0 ) { Unlock( "btnFood-PengyEgg", "Unlocked PengyEgg!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnHat-Penguin", "Unlocked Penguin Hat!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-repo" )
    {
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Repo Horse - Failure!", sf::Color::Red, tipPos );
            HollyTip( "Looks like the debtor didn't have anything of value to take!" );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else if ( result == 1 )
        {
            int healthLost = rand() % 2 + 1;
            chalo::EffectManager::AddEffect( "Quest: Repo Horse - Failure!", sf::Color::Red, tipPos );
            HollyTip( "The debtor fought Horse off! We got a little money." );
            tipPos.y += 25; EarnMoney( randomProfit / 2, tipPos );
            tipPos.y += 25; UpdateStat( "health", -healthLost, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            tipPos.y += 25; UpdateStat( "hygiene", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else if ( result == 2 )
        {
            chalo::EffectManager::AddEffect( "Quest: Repo Horse - Success!", sf::Color::Green, tipPos );
            HollyTip( "Horse was able to repossess some items with a good value! We made $" + to_string( randomProfit ) + ", and horse looks a little faster!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "agility", 1, tipPos );
            tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Repo Horse", sf::Color::Green, tipPos );
            HollyTip( "Horse was able to repossess some items with a good value! We made $" + to_string( randomProfit ) + "!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }

        if ( result >= 2 )
        {
            int randomUnlock = rand() % 6;
            if      ( randomUnlock == 0 ) { Unlock( "btnFood-Chocolate", "Unlocked Chocolate!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnFood-IceCream", "Unlocked IceCream!" ); }
            else if ( randomUnlock == 2 ) { Unlock( "btnToy-Bunny", "Unlocked Bunny Plushie!" ); }
            else if ( randomUnlock == 3 ) { Unlock( "btnHygiene-Dryer", "Unlocked Hair Dryer!" ); }
            else if ( randomUnlock == 4 ) { Unlock( "btnHat-ChrisR", "Unlocked \"Chris R\" Hat!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-ghost" )
    {
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Ghost job poster - Failure!", sf::Color::Red, tipPos );
            HollyTip( "We posted some ghost jobs but our employees didn't seem to care..." );
            tipPos.y += 25; UpdateStat( "strength", -1, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Ghost job poster - Success!", sf::Color::Green, tipPos );
            HollyTip( "We posted several ghost jobs and raised employee productivity without increasing pay! Saved $" + to_string( randomProfit ) + "!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        if ( result >= 0 )
        {
            int randomUnlock = rand() % 2;
            if ( randomUnlock == 0 ) { Unlock( "btnHat-Glasses", "Unlocked Glasses (Hat)!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-monsan" )
    {
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Mon-santa seed inspector - Failure!", sf::Color::Red, tipPos );
            HollyTip( "We were unable to prove that the farmer's seeds were our patented Mon-santa seeds." );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            tipPos.y += 25; UpdateStat( "hygiene", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Mon-santa seed inspector - Success!", sf::Color::Green, tipPos );
            HollyTip( "We found a farmer using our patented Mon-santa seeds and charged them a hefty fee for patent infringement! We made $" + to_string( randomProfit ) + "!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }

        if ( result >= 0 )
        {
            int randomUnlock = rand() % 5;
            if      ( randomUnlock == 0 ) { Unlock( "btnFood-Ponkin", "Unlocked Ponkin!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnFood-Clover", "Unlocked Clover!" ); }
            else if ( randomUnlock == 2 ) { Unlock( "btnHygiene-Water", "Unlocked Bucket-o-Water!" ); }
            else if ( randomUnlock == 3 ) { Unlock( "btnHat-ChrisR", "Unlocked \"Chris R\" Hat!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-health" )
    {
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Health insurance denier - Failure! (-" + to_string( randomPay ) + ")", sf::Color::Red, tipPos );
            HollyTip( "We couldn't find any loophole to deny coverage this time around... It cost us $" + to_string( randomPay ) + "!" );
            tipPos.y += 25; PayMoney( randomPay, tipPos );
            tipPos.y += 25; UpdateStat( "happy", -( rand() % 3 + 2 ), tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else if ( result == 1 )
        {
            chalo::EffectManager::AddEffect( "Quest: Health insurance denier - Success! (+$" + to_string( randomProfit ) + ", +1 smarts)", sf::Color::Green, tipPos );
            HollyTip( "We denied a bunch of claims and made $" + to_string( randomProfit ) + "! Horse also looks a little smarter!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "smarts", 1, tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Health insurance denier - Success! (+$" + to_string( randomProfit ) + ")", sf::Color::Green, tipPos );
            HollyTip( "We denied a bunch of claims and made $" + to_string( randomProfit ) + "!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            chalo::AudioManager::PlaySound( "questlose" );
        }

        if ( result >= 2 )
        {
            int randomUnlock = rand() % 3;
            if      ( randomUnlock == 0 ) { Unlock( "btnHygiene-Soap", "Unlocked Soap!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnHat-ChrisR", "Unlocked Glove Hat!" ); }
        }
    }
    else if ( m_horseStats.action == "quest-influence" )
    {
        vector<string> topics =
        {
            "being a Trad-Horse",
            "how to pick up mares with negging",
            "dropshipping",
            "Horseless AI content channels"
        };
        string randTopic = topics[ rand() % topics.size() ];
        randomProfit = rand() % 400 + 100;
        if ( result == 0 )
        {
            chalo::EffectManager::AddEffect( "Quest: Internet influencer - Failure!", sf::Color::Red, tipPos );
            HollyTip( "Nobody tuned in to our stream! We made no money." );
            tipPos.y += 25; PayMoney( randomPay, tipPos );

            int statusChange = rand() % 2;
            if ( result == 0 )
            {
                tipPos.y += 25; UpdateState( "Overworked!", "sorethroat", tipPos, sf::Color::Red );
            }
            chalo::AudioManager::PlaySound( "questlose" );
        }
        else if ( result == 1 )
        {
            chalo::EffectManager::AddEffect( "Quest: Internet influencer - Success!", sf::Color::Green, tipPos );
            HollyTip( "We sold an online course to teach about " + randTopic + ". We made $" + to_string( randomProfit ) + "! Horse also looks a little smarter!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "smarts", 1, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else if ( result == 2 )
        {
            chalo::EffectManager::AddEffect( "Quest: Internet influencer - Success!", sf::Color::Green, tipPos );
            HollyTip( "Lots of people tuned in for our stream about " + randTopic + "! We made $" + to_string( randomProfit ) + " in ad revenue! Horse looks a little more glamorous!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }
        else
        {
            chalo::EffectManager::AddEffect( "Quest: Internet influencer - Success!", sf::Color::Green, tipPos );
            HollyTip( "Lots of people tuned in for our stream about " + randTopic + "! We made $" + to_string( randomProfit ) + " in ad revenue!" );
            tipPos.y += 25; EarnMoney( randomProfit, tipPos );
            chalo::AudioManager::PlaySound( "questwin" );
        }

        if ( result >= 0 )
        {
            int randomUnlock = rand() % 6;
            if      ( randomUnlock == 0 ) { Unlock( "btnFood-Pretzel", "Unlocked Pretzel!" ); }
            else if ( randomUnlock == 1 ) { Unlock( "btnFood-Gingerbread", "Unlocked Gingerbread!" ); }
            else if ( randomUnlock == 2 ) { Unlock( "btnToy-Weight", "Unlocked Shaky-Weight!" ); }
            else if ( randomUnlock == 3 ) { Unlock( "btnHat-Duck", "Unlocked Duck Hat!" ); }
            else if ( randomUnlock == 4 ) { Unlock( "btnHat-Sunglasses", "Unlocked Duck Hat!" ); }
        }
    }

    m_horseStats.action = "";
    UpdateStatsLabels();
}

void CH2024_GameState::Unlock( std::string button, std::string message )
{
    try
    {
        sf::IntRect pos = chalo::MenuManager::GetButton( button ).GetPositionRect();
        if ( pos.top == 610 ) { return; }
        pos.top = 610;
        chalo::MenuManager::GetButton( button ).SetPosition( pos );
        chalo::EffectManager::AddEffect( message, sf::Color::Green, m_horse.getPosition() );
        m_unlockedButtons[ button ] = true;
    }
    catch( const out_of_range& ex )
    {
        Logger::Error( "Error getting button \"" + button + "\": " + ex.what(), CLASSNAME + "::" + std::string( __func__ ) );
    }
}

void CH2024_GameState::RestoreUnlocks()
{
    for ( auto& item : m_unlockedButtons )
    {
        sf::IntRect pos = chalo::MenuManager::GetButton( item.first ).GetPositionRect();
        pos.top = 610;
        chalo::MenuManager::GetButton( item.first ).SetPosition( pos );
    }
}

void CH2024_GameState::HorseCompetitionStart( string objectType )
{
    Logger::Out( "Start Competition: " + objectType );
    if ( Helper::Contains( m_horseStats.action, "quest" )
            || Helper::Contains( m_horseStats.action, "compet" )
            || m_horseStats.stats["health"] == 0 )
    {
        return;
    }
    if ( m_horseStats.stats["happy"] < 50 )
    {
        HollyTip( "Horse doesn't want to compete right now. Maybe try cheering 'em up!" );
        chalo::EffectManager::AddEffect( "Horse refuses", sf::Color::Cyan, m_horse.getPosition() );
        return;
    }

    int cost = 0;

    sf::Vector2f tipPos( m_horse.getPosition().x - 100, m_horse.getPosition().y );
    if ( tipPos.x < 10 ) { tipPos.x = 10; }

    if ( objectType == "Beauty" )
    {
        cost = 10000;
        if ( m_horseStats.bonus == "Lower prices boost" ) { cost = cost * 0.5; }

        if ( m_player.money < cost )
        {
            HollyTip( "Can't enter the beauty contest, we need at least $" + to_string( cost ) + "!" );
            chalo::EffectManager::AddEffect( "Beauty competition: Need $" + to_string( cost ) + "!", sf::Color::Red, tipPos );
            return;
        }

        HollyTip( "We've entered Horse in a beauty competition! Hope they win!" );
        chalo::EffectManager::AddEffect( "Competition: Beauty", sf::Color::Yellow, tipPos );
        tipPos.y += 25; PayMoney( cost, tipPos );

        m_horseStats.countdown = 200;
        m_horseStats.action = "compete-" + Helper::ToLower( objectType );
    }

    else if ( objectType == "Smarts" )
    {
        cost = 10000;
        if ( m_horseStats.bonus == "Lower prices boost" ) { cost = cost * 0.5; }

        if ( m_player.money < cost )
        {
            HollyTip( "Can't enter the beauty contest, we need at least $" + to_string( cost ) + "!" );
            chalo::EffectManager::AddEffect( "Smarts competition: Need $" + to_string( cost ) + "!", sf::Color::Red, tipPos );
            return;
        }

        HollyTip( "We've entered Horse in a smarts competition! Hope they win!" );
        chalo::EffectManager::AddEffect( "Competition: Smarts", sf::Color::Yellow, tipPos );
        tipPos.y += 25; PayMoney( cost, tipPos );

        m_horseStats.countdown = 200;
        m_horseStats.action = "compete-" + Helper::ToLower( objectType );
    }

    else if ( objectType == "Strength" )
    {
        cost = 20000;
        if ( m_horseStats.bonus == "Lower prices boost" ) { cost = cost * 0.5; }
        if ( m_player.money < cost )
        {
            HollyTip( "Can't enter the beauty contest, we need at least $" + to_string( cost ) + "!" );
            chalo::EffectManager::AddEffect( "Strength competition: Need $" + to_string( cost ) + "!", sf::Color::Red, tipPos );
            return;
        }

        HollyTip( "We've entered Horse in a strength competition! Hope they win!" );
        chalo::EffectManager::AddEffect( "Competition: Strength", sf::Color::Yellow, tipPos );
        tipPos.y += 25; PayMoney( cost, tipPos );

        m_horseStats.countdown = 200;
        m_horseStats.action = "compete-" + Helper::ToLower( objectType );
    }

    else if ( objectType == "Speed" )
    {
        cost = 20000;
        if ( m_horseStats.bonus == "Lower prices boost" ) { cost = cost * 0.5; }
        if ( m_player.money < cost )
        {
            HollyTip( "Can't enter the beauty contest, we need at least $" + to_string( cost ) + "!" );
            chalo::EffectManager::AddEffect( "Speed competition: Need $" + to_string( cost ) + "!", sf::Color::Red, tipPos );
            return;
        }

        HollyTip( "We've entered Horse in an agility competition! Hope they win!" );
        chalo::EffectManager::AddEffect( "Competition: Speed", sf::Color::Yellow, tipPos );
        tipPos.y += 25; PayMoney( cost, tipPos );

        m_horseStats.countdown = 200;
        m_horseStats.action = "compete-" + Helper::ToLower( objectType );
    }
}

void CH2024_GameState::HorseCompetitionUpdate()
{
//    Logger::Out( "start", "CH2024_GameState::HorseCompetitionUpdate" );
    if ( Helper::Contains( m_horseStats.action, "compete" ) == false ) { return; }

//    Logger::Out( "Countdown: " + to_string( m_horseStats.countdown ), "CH2024_GameState::HorseCompetitionUpdate" );
    if ( m_horseStats.countdown > 0 )
    {
//        Logger::Out( "Count down", "CH2024_GameState::HorseCompetitionUpdate" );
        m_horseStats.countdown--;
        if ( m_horseStats.countdown == 0 )
        {
//            Logger::Out( "Next phase", "CH2024_GameState::HorseCompetitionUpdate" );
            // End quest
            sf::Vector2f pos = m_horse.getPosition();
            pos.x = 400;
            m_horse.setPosition( pos.x, pos.y );

            // Result?
            HorseCompetitionFinish();
        }
    }

    if ( m_horseStats.countdown > 50 )
    {
//        Logger::Out( "Move off screen", "CH2024_GameState::HorseCompetitionUpdate" );
        // Move horse off screen
        sf::Vector2f pos = m_horse.getPosition();
        pos.x -= 10;
        m_horse.setPosition( pos.x, pos.y );
    }
}

void CH2024_GameState::HorseCompetitionFinish()
{
    sf::Vector2f tipPos( m_horse.getPosition().x - 100, m_horse.getPosition().y );
    if ( tipPos.x < 10 ) { tipPos.x = 10; }

    bool allWonBefore = ( m_horseStats.wonAgility && m_horseStats.wonBeauty && m_horseStats.wonSmarts && m_horseStats.wonStrength );

    if ( m_horseStats.action == "compete-beauty" )
    {
        if ( m_horseStats.stats["glam"] < 20 )
        {
            HollyTip( "Horse didn't qualify! They said it's not glamorous enough!" );
            chalo::EffectManager::AddEffect( "Beauty competition LOST: Not glamorous enough!", sf::Color::Red, tipPos );
        }
        else if ( m_horseStats.stats["hygiene"] < 90 )
        {
            HollyTip( "Horse was turned away! They said Horse is too stinky." );
            chalo::EffectManager::AddEffect( "Beauty competition LOST: Too stinky!", sf::Color::Red, tipPos );
        }
        else
        {
            HollyTip( "Horse won first place!!" );
            chalo::EffectManager::AddEffect( "Beauty competition WON!", sf::Color::Green, tipPos );
            m_horseStats.wonBeauty = true;
            chalo::AudioManager::PlaySound( "trophywin" );
        }
    }

    if ( m_horseStats.action == "compete-smarts" )
    {
        if ( m_horseStats.stats["smarts"] < 20 )
        {
            HollyTip( "Horse didn't qualify! Horse failed the entrance exam." );
            chalo::EffectManager::AddEffect( "Smarts competition LOST: Not smart enough!", sf::Color::Red, tipPos );
        }
        else if ( m_horseStats.stats["hygiene"] < 80 )
        {
            HollyTip( "Horse was turned away! They said Horse is too stinky." );
            chalo::EffectManager::AddEffect( "Smarts competition LOST: Too stinky!", sf::Color::Red, tipPos );
        }
        else
        {
            HollyTip( "Horse won first place!!" );
            chalo::EffectManager::AddEffect( "Smarts competition WON!", sf::Color::Green, tipPos );
            m_horseStats.wonSmarts = true;
            chalo::AudioManager::PlaySound( "trophywin" );
        }
    }

    if ( m_horseStats.action == "compete-strength" )
    {
        if ( m_horseStats.stats["strength"] < 20 )
        {
            HollyTip( "Horse didn't qualify! Horse was unable to pass the entrance round!" );
            chalo::EffectManager::AddEffect( "Strength competition LOST: Not strong enough!", sf::Color::Red, tipPos );
        }
        else if ( m_horseStats.stats["hygiene"] < 80 )
        {
            HollyTip( "Horse was turned away! They said Horse is too stinky." );
            chalo::EffectManager::AddEffect( "Strength competition LOST: Too stinky!", sf::Color::Red, tipPos );
        }
        else
        {
            HollyTip( "Horse won first place!!" );
            chalo::EffectManager::AddEffect( "Strength competition WON!", sf::Color::Green, tipPos );
            m_horseStats.wonStrength = true;
            chalo::AudioManager::PlaySound( "trophywin" );
        }
    }

    if ( m_horseStats.action == "compete-speed" )
    {
        if ( m_horseStats.stats["agility"] < 20 )
        {
            HollyTip( "Horse didn't qualify! Horse fell on their face during the preliminary race!" );
            chalo::EffectManager::AddEffect( "Speed competition LOST: Not glamorous enough!", sf::Color::Red, tipPos );
        }
        else if ( m_horseStats.stats["hygiene"] < 80 )
        {
            HollyTip( "Horse was turned away! They said Horse is too stinky." );
            chalo::EffectManager::AddEffect( "Speed competition LOST: Too stinky!", sf::Color::Red, tipPos );
        }
        else
        {
            HollyTip( "Horse won first place!!" );
            chalo::EffectManager::AddEffect( "Speed competition WON!", sf::Color::Green, tipPos );
            m_horseStats.wonAgility = true;
            chalo::AudioManager::PlaySound( "trophywin" );
        }
    }

    bool allWonAfter = ( m_horseStats.wonAgility && m_horseStats.wonBeauty && m_horseStats.wonSmarts && m_horseStats.wonStrength );

    if ( allWonBefore == false && allWonAfter == true )
    {
        chalo::MenuManager::GetLabel( "won", "lblPerfectHorse" ).SetPosition( sf::Vector2f( 250, 30 ) );
        tipPos.y += 25;
        chalo::EffectManager::AddEffect( "PERFECT HORSE!!!!", sf::Color::Yellow, tipPos );
        tipPos.y += 25;
        chalo::EffectManager::AddEffect( "YOU WIN!!!!", sf::Color::Yellow, tipPos );
        HollyTip( "We've made the PERFECT HORSE! Our mission is successful!!!" );
        Unlock( "btnHat-Crown", "Unlocked THE CROWN!!!!" );
        chalo::AudioManager::PlaySound( "youwin" );
    }

    UpdateStatsLabels();
}

void CH2024_GameState::HorseReset()
{
    m_horse.setOrigin( 226/2, 286/2 );
    m_horse.setPosition( 388, 285 );
    m_horseStats.countdown = 0;
    m_horseStats.speed = 2;
    m_horseStats.stats["happy"] = 100;
    m_horseStats.stats["health"] = 10;
    m_horseStats.stats["healthMax"] = 10;
    m_horseStats.stats["hunger"] = 0;
    m_horseStats.stats["hygiene"] = 100;
    m_horseStats.stats["strength"] = 1;
    m_horseStats.stats["agility"] = 1;
    m_horseStats.stats["glam"] = 1;
    m_horseStats.stats["smarts"] = 1;
    m_horseStats.hat = "";
    m_horseStats.bonus = "";
    m_horseStats.state = "";
    m_horseStats.stateTimer = 0;
    UpdateStatsLabels();
    m_horse.setRotation( 0 );
    m_horse.setColor( sf::Color::White );
    m_horse.setScale( sf::Vector2f( 1, 1 ) );
    m_horse.setTextureRect( sf::IntRect( 0, 0, 226, 286 ) );
    m_horseStats.wonBeauty = false;
    m_horseStats.wonSmarts = false;
    m_horseStats.wonStrength = false;
    m_horseStats.wonAgility = false;
}

void CH2024_GameState::HorseDeath()
{
    HollyTip( "Oh no! Horse died! Time to grab another one..." );
    chalo::EffectManager::AddEffect( "Horse death (-$500)", sf::Color::Red, sf::Vector2f( m_horse.getPosition().x, m_horse.getPosition().y + 20 ) );
    m_player.money -= 500;
    m_horseStats.countdown = 400;
    m_horse.setRotation( 180 );
    m_horse.setScale( sf::Vector2f( 1, 0.5 ) );
    m_horse.setColor( sf::Color::Red );
    m_horse.setPosition( m_horse.getPosition().x, m_horse.getPosition().y + 100 );
}

void CH2024_GameState::HorseDeathUpdate()
{
    if ( m_horseStats.stats["health"] > 0 ) { return; }
    if ( m_horseStats.countdown > 0 )
    {
        m_horseStats.countdown--;
        if ( m_horseStats.countdown == 0 )
        {
            m_horseStats.cloneCount++;
            HorseReset();
            HollyTip( "Make sure to take good care of this one!" );
        }
    }

    if ( m_horseStats.countdown >= 200 )
    {
        // Move horse off screen
        sf::Vector2f pos = m_horse.getPosition();
        pos.x -= 5;
        m_horse.setPosition( pos.x, pos.y );
    }
    else if ( m_horseStats.countdown == 199 )
    {
        m_horse.setScale( sf::Vector2f( 1, 1 ) );
        m_horse.setRotation( 0 );
        m_horse.setColor( sf::Color::White );
        m_horse.setPosition( sf::Vector2f( rand() % m_windowRegion.width, -100 ) );
    }
    else
    {
        m_horse.setRotation( m_horse.getRotation() + 5 );
        sf::Vector2f pos = m_horse.getPosition();
        if ( pos.x < 388-5 ) { pos.x += 5; }
        else if ( pos.x > 388+5 ) { pos.x -= 5; }
        if ( pos.y < 320-5 ) { pos.y += 5; }
        else if ( pos.y > 320+5 ) { pos.y -= 5; }
        m_horse.setPosition( pos.x, pos.y );
    }
}

void CH2024_GameState::HorseInteract( string objectType )
{
    Logger::Out( "Horse touches " + objectType, CLASSNAME + "::" + std::string( __func__ ) );

    sf::Vector2f tipPos( m_horse.getPosition().x - 100, m_horse.getPosition().y );
    if ( tipPos.x < 10 ) { tipPos.x = 10; }

    if ( m_horseStats.countdown > 0 && m_horseStats.stats["health"] == 0 ) { return; /* horse death transition */ }
    if ( objectType == "Clover" )
    {
        HollyTip( "I think " + objectType + " is bad for Horses..." );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Green;
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Red, tipPos );
        tipPos.y += 25; UpdateStat( "health", -2, tipPos );
        tipPos.y += 25; UpdateState( "Food poisoning", "tummyache", tipPos, sf::Color::Red );
        chalo::AudioManager::PlaySound( "damage" );
    }
    else if ( objectType == "PepPizza" )
    {
        HollyTip( objectType + " has meat, which is bad for Horses..." );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Green;
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Red, tipPos );
        tipPos.y += 25; UpdateStat( "health", -1, tipPos );
        tipPos.y += 25; UpdateState( "Food poisoning", "tummyache", tipPos, sf::Color::Red );
        chalo::AudioManager::PlaySound( "damage" );
    }
    else if ( objectType == "Chocolate" )
    {
        HollyTip( "I think " + objectType + " is bad for Horses..." );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Green;
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Red, tipPos );
        tipPos.y += 25; UpdateStat( "health", -2, tipPos );
        tipPos.y += 25; UpdateState( "Food poisoning", "tummyache", tipPos, sf::Color::Red );
        chalo::AudioManager::PlaySound( "damage" );
    }
    else if ( objectType == "Apple" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -1, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "IceCream" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 2, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -1, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "PengyEgg" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -1, tipPos );
    }
    else if ( objectType == "Gingerbread" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 2, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -2, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "Pretzel" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 2, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -2, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "Sugar" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 2, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "Banana" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "Ponkin" )
    {
        chalo::EffectManager::AddEffect( "Ate " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "hunger", -2, tipPos );
        chalo::AudioManager::PlaySound( "nom" );
    }
    else if ( objectType == "Weight" )
    {
        chalo::EffectManager::AddEffect( "Played with " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "strength", 1, tipPos );
        chalo::AudioManager::PlaySound( "yay" );
    }
    else if ( objectType == "Ball" )
    {
        chalo::EffectManager::AddEffect( "Played with " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "agility", 1, tipPos );
        chalo::AudioManager::PlaySound( "yay" );
    }
    else if ( objectType == "Bunny" )
    {
        chalo::EffectManager::AddEffect( "Played with " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
        chalo::AudioManager::PlaySound( "yay" );
    }
    else if ( objectType == "Puzzle" )
    {
        chalo::EffectManager::AddEffect( "Played with " + objectType, sf::Color::Green, tipPos );
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
        tipPos.y += 25; UpdateStat( "smarts", 1, tipPos );
        chalo::AudioManager::PlaySound( "yay" );
    }
    else if ( objectType == "Pill" )
    {
        chalo::EffectManager::AddEffect( "Took " + objectType, sf::Color::Green, tipPos );
    }
    else if ( objectType == "Shot" )
    {
        chalo::EffectManager::AddEffect( "Received vaccine", sf::Color::Green, tipPos );

        if ( m_horseStats.state == "bitten" )
        {
            tipPos.y += 25; UpdateState( "No rabies", "", tipPos, sf::Color::Green );
        }
        else
        {
            UpdateState( "Unnecessary medicine", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Bandage" )
    {
        tipPos.y += 25; UpdateState( "Bandage applied", "", tipPos, sf::Color::Green );
    }
    else if ( objectType == "Medicine" )
    {
        chalo::EffectManager::AddEffect( "Took " + objectType, sf::Color::Green, tipPos );

        if ( m_horseStats.state == "tummyache" )
        {
            tipPos.y += 25; UpdateState( "Not nauseous", "", tipPos, sf::Color::Yellow );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Unnecessary medicine", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Coughdrop" )
    {
        chalo::EffectManager::AddEffect( "Took a " + objectType, sf::Color::Green, tipPos );
        if ( m_horseStats.state == "sorethroat" )
        {
            tipPos.y += 25; UpdateState( "Feeling better", "", tipPos, sf::Color::Green );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Unnecessary medicine", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "HeatingPad" )
    {
        chalo::EffectManager::AddEffect( "Used a " + objectType, sf::Color::Green, tipPos );

        if ( m_horseStats.state == "chilled" )
        {
            tipPos.y += 25; UpdateState( "Not chilled", "", tipPos, sf::Color::Green );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Hot hot Horse", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Brush" )
    {
        chalo::EffectManager::AddEffect( "Got brushed", sf::Color::Green, tipPos );

        if ( m_horseStats.state == "" )
        {
            tipPos.y += 25; UpdateState( "Styled up", "", tipPos, sf::Color::Green );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Unpleasant brushing", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Soap" )
    {
        chalo::EffectManager::AddEffect( "Got soaped up", sf::Color::Green, tipPos );

        if ( m_horseStats.state == "wet" )
        {
            tipPos.y += 25; UpdateState( "Soaped up", "soaped", tipPos, sf::Color::Green );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Dry soapy flakey Horse", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Water" )
    {
        chalo::EffectManager::AddEffect( "Got rinsed off", sf::Color::Green, tipPos );

        if ( m_horseStats.state == "soaped" )
        {
            tipPos.y += 25; UpdateState( "Rinsed off", "wet", tipPos, sf::Color::Green );
        }
        else if ( m_horseStats.state == "" )
        {
            tipPos.y += 25; UpdateState( "Wet Horse", "wet", tipPos, sf::Color::Green );
            chalo::AudioManager::PlaySound( "oof" );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Soggy Horse", "wet", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }
    else if ( objectType == "Dryer" )
    {
        chalo::EffectManager::AddEffect( "Got dried off", sf::Color::Green, tipPos );

        if ( m_horseStats.state == "wet" )
        {
            tipPos.y += 25; UpdateState( "Dried off", "", tipPos, sf::Color::Green );
        }
        else if ( m_horseStats.state == "soaped" )
        {
            tipPos.y += 25; UpdateState( "Itchy dry Horse", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
        else
        {
            tipPos.y += 25; UpdateState( "Overly dry Horse", "", tipPos, sf::Color::Yellow );
            chalo::AudioManager::PlaySound( "oof" );
        }
    }



    // Stat caps
    UpdateStatsLabels();

    if ( m_horseStats.stats["health"] == 0 )
    {
        HorseDeath();
    }
}

void CH2024_GameState::UpdateHorse()
{
    if ( m_horseStats.countdown > 0 && m_horseStats.stats["health"] == 0 ) { return; /* horse death transition */ }
    if ( m_horseStats.countdown > 0 && Helper::Contains( m_horseStats.action, "quest" ) ) { return; }
    if ( m_horseStats.countdown > 0 && Helper::Contains( m_horseStats.action, "compete" ) ) { return; }
    if ( m_horseStats.tintTimer > 0 )
    {
        m_horseStats.tintTimer--;

        if ( m_horseStats.tint.r < 255 ) { m_horseStats.tint.r++; }
        if ( m_horseStats.tint.g < 255 ) { m_horseStats.tint.g++; }
        if ( m_horseStats.tint.b < 255 ) { m_horseStats.tint.b++; }

        m_horse.setColor( m_horseStats.tint );
        if ( m_horseStats.tintTimer == 0 )
        {
            m_horseStats.tint = sf::Color::White;
        }
    }
    if ( m_horseStats.countdown == 0 )
    {
        // Decide next thing to do
        int random = rand() % 3;
        switch( random )
        {
        case 0: m_horseStats.action = "meander-left"; break;
        case 1: m_horseStats.action = "meander-right"; break;
        case 2: m_horseStats.action = "stand-still"; break;
        }

        m_horseStats.countdown = rand() % 100 + 100;

    }
    else
    {
        m_horseStats.countdown--;
    }

    sf::Vector2f currentPos = m_horse.getPosition();

    if ( m_horseStats.action == "meander-left" )
    {
        currentPos.x -= m_horseStats.speed;
        m_horse.setScale( 1, 1 );
    }

    else if ( m_horseStats.action == "meander-right" )
    {
        currentPos.x += m_horseStats.speed;
        m_horse.setScale( -1, 1 );
    }

    else if ( m_horseStats.action == "stand-still" )
    {
    }


    if ( m_horseStats.action == "meander-left" || m_horseStats.action == "meander-right" )
    {
        if ( m_horseStats.countdown % 40 < 10 )
        {
            m_horse.setRotation( m_horse.getRotation() - 0.5 );
        }
        else if ( m_horseStats.countdown % 40 < 20 )
        {
            m_horse.setRotation( m_horse.getRotation() + 0.5 );
        }
    }
    else
    {
        m_horse.setRotation( 0 );
    }

    // Horse stay on screen
    bool wasRestricted = Helper::RestrictBound( currentPos.x, float( m_windowRegion.left ), float( m_windowRegion.left + m_windowRegion.width - 100 ) );

    if ( wasRestricted )
    {
        m_horseStats.countdown = 0;
    }

    m_horse.setPosition( currentPos );

    if ( m_horseStats.state != "" )
    {
        m_horseStats.stateTimer++;

        if ( m_horseStats.stateTimer > 0 && m_horseStats.stateTimer % 500 == 0 )
        {
            UpdateState( "Still " + m_horseStats.state, m_horseStats.state, m_horse.getPosition(), sf::Color::Yellow );
        }
    }
}

void CH2024_GameState::HorseEntropy()
{
    m_horseStats.stats["hunger"]++;
    m_horseStats.stats["happy"]--;
    m_horseStats.stats["hygiene"]--;

    if ( m_horseStats.stats["hunger"] >= 50 )
    {
        m_horseStats.stats["health"]--;
        m_horseStats.stats["happy"]--;
    }
    if ( m_horseStats.stats["hygiene"] < 50 )
    {
        m_horseStats.stats["health"]--;
        m_horseStats.stats["happy"]--;
    }

    Helper::RestrictBound( m_horseStats.stats["hunger"], 0, 100 );
    Helper::RestrictBound( m_horseStats.stats["health"], 0, 100 );
    Helper::RestrictBound( m_horseStats.stats["happy"], 0, 100 );
    Helper::RestrictBound( m_horseStats.stats["hygiene"], 0, 100 );

    UpdateStatsLabels();
}

void CH2024_GameState::HorseWearHat( std::string objectType )
{
    sf::IntRect frame( 0, 0, 226, 286 );
    if      ( objectType == "Tophat"     ) { frame.left = 1 * 226; m_horseStats.bonus = "Lookin' good"; }
    else if ( objectType == "Monocle"    ) { frame.left = 2 * 226; m_horseStats.bonus = "Money earned boost"; }
    else if ( objectType == "Duck"       ) { frame.left = 3 * 226; m_horseStats.bonus = "Lower prices boost"; }
    else if ( objectType == "Glove"      ) { frame.left = 4 * 226; m_horseStats.bonus = "Hygiene boost"; }
    else if ( objectType == "ChrisR"     ) { frame.left = 5 * 226; m_horseStats.bonus = "Strength boost"; }
    else if ( objectType == "Penguin"    ) { frame.left = 6 * 226; m_horseStats.bonus = "Agility boost"; }
    else if ( objectType == "Sunglasses" ) { frame.left = 7 * 226; m_horseStats.bonus = "Glam boost"; }
    else if ( objectType == "Glasses"    ) { frame.left = 8 * 226; m_horseStats.bonus = "Smarts boost"; }
    else if ( objectType == "Crown"      ) { frame.left = 9 * 226; m_horseStats.bonus = "Proof of perfection"; }
    m_horse.setTextureRect( frame );
    sf::Vector2f tipPos = m_horse.getPosition();
    tipPos.x -= 100;
    chalo::EffectManager::AddEffect( objectType + ": " + m_horseStats.bonus, sf::Color::Yellow, tipPos );
}

void CH2024_GameState::EarnMoney( int amount, sf::Vector2f tipPos )
{
    int adjusted = amount;
    std::string message = "$" + to_string( amount );


    if ( m_horseStats.bonus == "Money earned boost" )
    {
        adjusted = amount + ( amount * 0.50 );
        message += " --> $" + to_string( adjusted ) + " (" + m_horseStats.bonus + ")";
    }
    m_player.money += adjusted;

    chalo::EffectManager::AddEffect( message, sf::Color::Green, tipPos );
}

void CH2024_GameState::PayMoney( int amount, sf::Vector2f tipPos )
{
    int adjusted = amount;
    std::string message = "-$" + to_string( amount );

    if ( m_horseStats.bonus == "Lower prices boost" )
    {
        adjusted = amount - ( amount * 0.50 );
        message += " --> -$" + to_string( adjusted ) + " (" + m_horseStats.bonus + ")";
    }
    m_player.money -= adjusted;

    chalo::EffectManager::AddEffect( message, sf::Color::Red, tipPos );
}

void CH2024_GameState::UpdateStat( std::string type, int amount, sf::Vector2f tipPos )
{
    int adjusted = 0;
    std::string message = "";

    if ( amount > 0 ) { message += "+"; }
    message += to_string( amount ) + " " + type;

    sf::Color col = sf::Color::Green;
    if ( amount < 0 && type != "hunger" )
    {
        col = sf::Color::Red;
        adjusted = amount * 0.5;
    }
    else if ( amount < 0 && type == "hunger" )
    {
        col = sf::Color::Green;
        adjusted = amount * 0.5;
    }
    else if ( amount > 0 && type != "hunger" )
    {
        col = sf::Color::Green;
        adjusted = amount * 1.5;
    }
    else if ( amount > 0 && type == "hunger" )
    {
        adjusted = amount * 1.5;
        col = sf::Color::Red;
    }

    if ( type == "hygiene" )
    {
        if ( m_horseStats.bonus == "Hygiene boost" ) { message += " --> " + to_string( amount + adjusted ) + " (" + m_horseStats.bonus + ")"; }
    }
    else if ( type == "strength" )
    {
        if ( m_horseStats.bonus == "Strength boost" ) { message += " --> " + to_string( amount + adjusted ) + " (" + m_horseStats.bonus + ")"; }
    }
    else if ( type == "agility" )
    {
        if ( m_horseStats.bonus == "Agility boost" ) { message += " --> " + to_string( amount + adjusted ) + " (" + m_horseStats.bonus + ")"; }
    }
    else if ( type == "glam" )
    {
        if ( m_horseStats.bonus == "Glam boost" ) { message += " --> " + to_string( amount + adjusted ) + " (" + m_horseStats.bonus + ")"; }
    }
    else if ( type == "smarts" )
    {
        if ( m_horseStats.bonus == "Smarts boost" ) { message += " --> " + to_string( amount + adjusted ) + " (" + m_horseStats.bonus + ")"; }
    }
    else if ( type == "happy" )
    {
    }
    else if ( type == "health" )
    {
    }

    if ( m_horseStats.bonus == "" ) { adjusted = 0; }
    m_horseStats.stats[ type ] += amount + adjusted;
    chalo::EffectManager::AddEffect( message, col, tipPos );
}

void CH2024_GameState::UpdateState( std::string message, std::string newState, sf::Vector2f tipPos, sf::Color color )
{
    if ( m_horseStats.state != newState ) { m_horseStats.stateTimer = 0; }
    m_horseStats.state = newState;

    if ( message != "" )
    {
        chalo::EffectManager::AddEffect( message, color, tipPos );
    }

    // Status updates
    if ( message == "Overly dry Horse" ||
            message == "Itchy dry Horse"
       )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Yellow;
    }
    else if (
        message == "Soggy Horse" ||
        message == "Wet Horse"
    )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
        tipPos.y += 25; UpdateStat( "health", -1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Cyan;
    }
    else if ( message == "Dried off" )
    {
        tipPos.y += 25; UpdateStat( "hygiene", 1, tipPos );
        tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Yellow;
    }
    else if ( message == "Rinsed off" )
    {
        tipPos.y += 25; UpdateStat( "hygiene", 1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Cyan;
    }
    else if ( message == "Soaped up" )
    {
        tipPos.y += 25; UpdateStat( "hygiene", 1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Magenta;
    }
    else if ( message == "Dry soapy flakey Horse" )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Magenta;
    }
    else if ( message == "Styled up" )
    {
        tipPos.y += 25; UpdateStat( "glam", 1, tipPos );
        tipPos.y += 25; UpdateStat( "hygiene", 1, tipPos );
    }
    else if ( message == "Unpleasant brushing" )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
    }
    else if ( message == "Not chilled" )
    {
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
    }
    else if ( message == "Hot hot Horse" )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
        m_horseStats.tintTimer = 255;
        m_horseStats.tint = sf::Color::Red;
    }
    else if ( message == "Unnecessary medicine" )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
    }
    else if ( message == "Feeling better" )
    {
        m_horseStats.tint = sf::Color::White;
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
    }
    else if ( message == "Not nauseous" )
    {
        m_horseStats.tint = sf::Color::White;
        tipPos.y += 25; UpdateStat( "happy", 1, tipPos );
    }
    else if ( message == "Bandage applied" )
    {
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
    }
    else if ( message == "No rabies" )
    {
        tipPos.y += 25; UpdateStat( "health", 1, tipPos );
        m_horseStats.tint = sf::Color::White;
    }
    else if ( m_horseStats.state == "chilled" )
    {
        tipPos.y += 25; UpdateStat( "happy", -1, tipPos );
        tipPos.y += 25; UpdateStat( "health", -1, tipPos );
    }
    else if ( m_horseStats.state == "tummyache" )
    {
        tipPos.y += 25; UpdateStat( "health", -1, tipPos );
    }


    // State change
    if ( m_horseStats.state == "wet" && m_horseStats.stateTimer > 200 )
    {
        tipPos.y += 25; UpdateState( "Chilled Horse", "chilled", tipPos, sf::Color::Red );
    }
    if ( m_horseStats.state == "bitten" && m_horseStats.stateTimer >= 1000 )
    {
        tipPos.y += 25; UpdateStat( "health", -100, tipPos );
        tipPos.y += 25; chalo::EffectManager::AddEffect( "Died of rabies", sf::Color::Red, tipPos );
        HorseDeath();
    }

    UpdateStatsLabels();
}

void CH2024_GameState::SwitchTab( std::string tabButton )
{
    Logger::OutValue( "Switch to tab", tabButton, CLASSNAME + "::" + __func__ );

    sf::Color newBgColor;
    for ( auto& mc : m_menuColors )
    {
        // Update tab image
        sf::IntRect backgroundRect( 0, 0, 135, 29 );
        if ( Helper::Contains( tabButton, mc.first ) )
        {
            backgroundRect.height = 29;
            newBgColor = mc.second;
        }
        else
        {
            backgroundRect.height = 30;
        }

        try
        {
            chalo::UIButton& button = chalo::MenuManager::GetButton( "1", "btn" + mc.first );
            backgroundRect.left = button.GetBackgroundClipRect().left;
            button.SetupBackgroundImageClipRect( backgroundRect );
        }
        catch( ... )
        {
        }
    }

    try
    {
        // Change background color
        chalo::UIRectangleShape& background = chalo::MenuManager::GetRectangleShape( "0", "bgShape" ); // Reminder to self: layer is a string not an int!
        background.SetFillColor( newBgColor );
    }
    catch( ... )
    {
        Logger::Error( "An exception occurred, fix it in the engine!!", "CH2024_GameState::SwitchTab" );
    }

    // Change which buttons are available
    chalo::MenuManager::TurnOffLayers( { "btnTab1", "btnTab2", "btnTab3", "btnTab4", "btnTab5", "btnTab6", "btnTab7" } );
    chalo::MenuManager::TurnOnLayers( { tabButton } );
}

void CH2024_GameState::HollyTip( std::string text, int maxWidth /*= 275*/ )
{
    m_hollyTips.clear();
    int x = 1000;
    int y = 355;
    int inc = 20;
    int lastWhitespace = 0;
    chalo::UILabel lbl;

    // width of text character is about 9 px
    int textWidth = maxWidth / 10;

    // Text is smaller than the width of the bubble
    if ( text.size() < textWidth )
    {
        lbl.Setup( "hollytext-" + to_string( m_hollyTips.size() ), "main", 18, sf::Color::Black, sf::Vector2f( x, y ), text );
        m_hollyTips.push_back( lbl );
        return;
    }

    // Put text on different lines
    while ( text != "" )
    {
        lastWhitespace = string::npos;
        // Find next whitespace before maxWidth
        for ( int i = textWidth-1; i >= 0; i-- )
        {
            if ( text[i] == ' ' )
            {
                lastWhitespace = i;
                break;
            }
        }

        if ( lastWhitespace == string::npos )
        {
            lastWhitespace = text.size()-1;
        }

        std::string substring = text.substr( 0, lastWhitespace );
        text.erase( 0, lastWhitespace );
        lbl.Setup( "hollytext-" + to_string( m_hollyTips.size() ), "main", 18, sf::Color::Black, sf::Vector2f( x, y ), substring );
        m_hollyTips.push_back( lbl );
        y += inc;
    }
}

void CH2024_GameState::UpdateStatsLabels()
{
    Helper::RestrictBound( m_horseStats.stats["happy"], 0, 100 );
    Helper::RestrictBound( m_horseStats.stats["health"], 0, m_horseStats.stats["healthMax"] );
    Helper::RestrictBound( m_horseStats.stats["hunger"], 0, 100 );
    Helper::RestrictBound( m_horseStats.stats["hygiene"], 0, 100 );

    chalo::MenuManager::GetLabel( "hud", "lblStatHappy-Value" ).SetText( to_string( m_horseStats.stats["happy"] ) + "%" );
    chalo::MenuManager::GetLabel( "hud", "lblStatHunger-Value" ).SetText( to_string( m_horseStats.stats["hunger"] ) + "%" );
    chalo::MenuManager::GetLabel( "hud", "lblStatHygiene-Value" ).SetText( to_string( m_horseStats.stats["hygiene"] ) + "%" );
    chalo::MenuManager::GetLabel( "hud", "lblStatHealth-Value" ).SetText( to_string( m_horseStats.stats["health"] ) + "/" + to_string( m_horseStats.stats["healthMax"] ) );
    chalo::MenuManager::GetLabel( "hud", "lblStatStrength-Value" ).SetText( to_string( m_horseStats.stats["strength"] ) );
    chalo::MenuManager::GetLabel( "hud", "lblStatAgility-Value" ).SetText( to_string( m_horseStats.stats["agility"] ) );
    chalo::MenuManager::GetLabel( "hud", "lblStatGlam-Value" ).SetText( to_string( m_horseStats.stats["glam"] ) );
    chalo::MenuManager::GetLabel( "hud", "lblStatSmarts-Value" ).SetText( to_string( m_horseStats.stats["smarts"] ) );

    string moneystr = "";
    sf::Color color = sf::Color::Green;
    if ( m_player.money < 0 ) { moneystr = "-$" + to_string( -m_player.money ); color = sf::Color::Red; }
    else { moneystr = "$" + to_string( m_player.money ); }
    chalo::MenuManager::GetLabel( "led", "lblMoney" ).SetText( moneystr );
    chalo::MenuManager::GetLabel( "led", "lblMoney" ).ChangeColor( color );


    string horseName = "Horse";
    if ( m_horseStats.cloneCount > 0 )
    {
        horseName += " (" + to_string( m_horseStats.cloneCount + 1 ) + ")";
    }
    chalo::MenuManager::GetLabel( "hud", "lblPetName" ).SetText( horseName );

    if ( m_horseStats.wonBeauty   ) { chalo::MenuManager::GetImage( "won", "imgTrophy-Beauty"   ).SetPosition( sf::Vector2f( 25,   515 ) ); }
    else                              { chalo::MenuManager::GetImage( "won", "imgTrophy-Beauty"   ).SetPosition( sf::Vector2f( -500, 515 ) ); }
    if ( m_horseStats.wonSmarts   ) { chalo::MenuManager::GetImage( "won", "imgTrophy-Smarts"   ).SetPosition( sf::Vector2f( 75,   515 ) ); }
    else                              { chalo::MenuManager::GetImage( "won", "imgTrophy-Smarts"   ).SetPosition( sf::Vector2f( -500, 515 ) ); }
    if ( m_horseStats.wonStrength ) { chalo::MenuManager::GetImage( "won", "imgTrophy-Strength" ).SetPosition( sf::Vector2f( 125,  515 ) ); }
    else                              { chalo::MenuManager::GetImage( "won", "imgTrophy-Strength" ).SetPosition( sf::Vector2f( -500, 515 ) ); }
    if ( m_horseStats.wonAgility  ) { chalo::MenuManager::GetImage( "won", "imgTrophy-Agility"  ).SetPosition( sf::Vector2f( 175,  515 ) ); }
    else                              { chalo::MenuManager::GetImage( "won", "imgTrophy-Agility"  ).SetPosition( sf::Vector2f( -500, 515 ) ); }
}



