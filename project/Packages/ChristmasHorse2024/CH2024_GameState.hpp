#ifndef _CH2024_GAME_STATE
#define _CH2024_GAME_STATE

#include <SFML/Audio.hpp>
#include <vector>
#include <map>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/EscapeyState.hpp"

struct CH2024_Object
{
    sf::Sprite sprite;
    std::string name;
    sf::Vector2f velocity;
    bool deleteMe;
};

class CH2024_GameState : public EscapeyState
{
public:
    CH2024_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    const float m_accRate;
    const float m_accRateMax;
    sf::IntRect m_windowRegion;
    sf::Sprite m_background;
    sf::Sprite m_foreground;
    sf::Sprite m_holly;
    sf::Sprite m_horse;
    std::map<std::string, sf::Color> m_menuColors;
    std::vector<chalo::UILabel> m_hollyTips;
    std::vector<CH2024_Object> m_objects;
    float m_globalTimer;

    void SwitchTab( std::string tabButton );
    void HollyTip( std::string text, int maxWidth = 275 );
    void RestoreDefaultCursor();
    void SpawnObject( string btnName );
    void UpdateObjects();
    void UpdateHorse();
    void HorseQuestStart( string objectType );
    void HorseQuestUpdate();
    void HorseQuestFinish();
    void HorseCompetitionStart( string objectType );
    void HorseCompetitionUpdate();
    void HorseCompetitionFinish();
    void UpdateStatsLabels();
    void HorseInteract( string objectType );
    void HorseReset();
    void HorseDeath();
    void HorseDeathUpdate();
    void HorseEntropy();
    void HorseWearHat( std::string objectType );

    void EarnMoney( int amount, sf::Vector2f tipPos );
    void PayMoney( int amount, sf::Vector2f tipPos );
    void UpdateStat( std::string type, int amount, sf::Vector2f tipPos );
    void UpdateState( std::string message, std::string newState, sf::Vector2f tipPos = sf::Vector2f( 0, 0 ), sf::Color color = sf::Color::White );
    void Unlock( std::string button, std::string message );
    void RestoreUnlocks();

    std::map< std::string, bool > m_unlockedButtons;

    struct
    {
        sf::Sprite cursor;
        std::string action;
        int money;
    } m_player;

    struct
    {
        std::map< std::string, int > stats;
        int cloneCount;
        std::string action;
        std::string state;
        int stateTimer;
        std::string hat;
        std::string bonus;
        int countdown;
        int speed;
        sf::Color tint;
        int tintTimer;
        bool wonBeauty;
        bool wonSmarts;
        bool wonStrength;
        bool wonAgility;
    } m_horseStats;
};




//    if ( objectType == "Apple"
//    if ( objectType == "PepPizza"
//    if ( objectType == "Clover"
//    if ( objectType == "Chocolate"
//    if ( objectType == "IceCream"
//    if ( objectType == "Peanut"
//    if ( objectType == "PengyEgg"
//    if ( objectType == "Pretzel"
//    if ( objectType == "Gingerbread"
//    if ( objectType == "Ponkin"
//    if ( objectType == "Sugar"
//    if ( objectType == "Banana"






















#endif
