#include "LoreState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Application/Application.hpp"

LoreState::LoreState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void LoreState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void LoreState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "button-long",             "Packages/ProgramBase/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Packages/ProgramBase/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Packages/ProgramBase/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Packages/ProgramBase/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "motorHorse",              "Packages/ProgramBase/Graphics/Lore/motor-horse.png" );
    chalo::TextureManager::Add( "ch1",                     "Packages/ProgramBase/Graphics/Lore/ch1.png" );
    chalo::TextureManager::Add( "ch2",                     "Packages/ProgramBase/Graphics/Lore/ch2.png" );

    chalo::MenuManager::LoadCsvMenu( "lore.csv" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void LoreState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void LoreState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void LoreState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}



