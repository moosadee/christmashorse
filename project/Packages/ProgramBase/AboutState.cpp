#include "AboutState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Application/Application.hpp"

AboutState::AboutState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void AboutState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void AboutState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "button-long",             "Packages/ProgramBase/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Packages/ProgramBase/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Packages/ProgramBase/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Packages/ProgramBase/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "horse",                   "Packages/ProgramBase/Graphics/UI/horse.png" );

    chalo::MenuManager::LoadCsvMenu( "about.csv" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void AboutState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void AboutState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void AboutState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}



