#include "ChaloEngineProgram.hpp"

// Engine files
#include "../../../chalo-engine/Managers/FontManager.hpp"
#include "../../../chalo-engine/Application/Application.hpp"
#include "../../../chalo-engine/Utilities/Messager.hpp"

#include "ChaloEngineProgram.hpp"

// Game states
#include "../StartupState.hpp"
#include "../AboutState.hpp"
#include "../LoreState.hpp"
#include "../OptionsState.hpp"

#include "../../MotorHorse/MH_GameState.hpp"

#include "../../ChristmasHorse2020/CH2020_GameState.hpp"
#include "../../ChristmasHorse2020/CH2020_GameState2.hpp"
#include "../../ChristmasHorse2020/CH2020_GameStateBoss.hpp"
#include "../../ChristmasHorse2020/CH2020_EpilogueState.hpp"

#include "../../ChristmasHorse2021/CH2021_GameState.hpp"
#include "../../ChristmasHorse2021/CH2021_PrologueState.hpp"
#include "../../ChristmasHorse2021/CH2021_EpilogueState.hpp"

#include "../../ChristmasHorse2022/CH2022_GameState.hpp"
#include "../../ChristmasHorse2022/CH2022_PrologueState.hpp"
#include "../../ChristmasHorse2022/CH2022_EpilogueState.hpp"

#include "../../ChristmasHorse2023/CH2023_GameState.hpp"
#include "../../ChristmasHorse2023/CH2023_EpilogueState.hpp"

#include "../../ChristmasHorse2024/CH2024_GameState.hpp"

#include "../EscapeyState.hpp"

#include "../StartupState.hpp"
#include "../../ExciteHorse/EXH_GameState.hpp"

ChaloEngineProgram::ChaloEngineProgram( bool fullscreen /* = false */ )
{
    CLASSNAME = std::string( typeid( *this ).name() );
    Logger::OutFuncBegin( "fullscreen=" + Helper::ToString( fullscreen ), CLASSNAME + "::" + std::string( __func__ ) );
    Setup( "The Christmas Horse Saga", fullscreen );
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

ChaloEngineProgram::~ChaloEngineProgram()
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
    Teardown();
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Setup( std::string programName, bool fullscreen /* = false */ )
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );

    chalo::ChaloProgram::Setup( programName );
    m_stateManager.InitManager();

    // CUSTOMIZE: Add additional states here as needed
    m_stateManager.AddInactiveState( "startupstate",                new StartupState );
    m_stateManager.AddInactiveState( "optionsstate",                new OptionsState );
    m_stateManager.AddInactiveState( "aboutstate",                  new AboutState );
    m_stateManager.AddInactiveState( "lorestate",                   new LoreState );

    // Christmas Horse 2020
    m_stateManager.AddInactiveState( "ch2020_gamestate",            new CH2020_GameState );
    m_stateManager.AddInactiveState( "ch2020_gamestate2",           new CH2020_GameState2 );
    m_stateManager.AddInactiveState( "ch2020_gamestateboss",        new CH2020_GameStateBoss );
    m_stateManager.AddInactiveState( "ch2020_epiloguestate",        new CH2020_EpilogueState );

    // Christmas Horse 2021
    m_stateManager.AddInactiveState( "ch2021_gamestate",            new CH2021_GameState );
    m_stateManager.AddInactiveState( "ch2021_prologuestate",        new CH2021_PrologueState );
    m_stateManager.AddInactiveState( "ch2021_epiloguestate",        new CH2021_EpilogueState );

    // Christmas Horse 2022
    m_stateManager.AddInactiveState( "ch2022_gamestate",            new CH2022_GameState );
    m_stateManager.AddInactiveState( "ch2022_prologuestate",        new CH2022_PrologueState );
    m_stateManager.AddInactiveState( "ch2022_epiloguestate",        new CH2022_EpilogueState );

    // Christmas Horse 2023
    m_stateManager.AddInactiveState( "ch2023_gamestate",            new CH2023_GameState );
    m_stateManager.AddInactiveState( "ch2023_epiloguestate",        new CH2023_EpilogueState );

    // Christmas Horse 2024
    m_stateManager.AddInactiveState( "ch2024_gamestate",            new CH2024_GameState );

    m_stateManager.ChangeActiveState( "startupstate" );
//    m_stateManager.ChangeActiveState( "ch2024_gamestate" );
//    m_stateManager.ChangeActiveState( "aboutstate" );

    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Teardown()
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
    chalo::ChaloProgram::Teardown();
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Run()
{
    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        chalo::Application::Update();
        m_stateManager.UpdateAndDraw( chalo::Application::GetWindow() );
        chalo::Application::EndDrawing();
    }

}
