#ifndef _CH2021_TYPES
#define _CH2021_TYPES

enum class Direction
{
    EAST,
    NORTHEAST,
    NORTH,
    NORTHWEST,
    WEST,
    SOUTHWEST,
    SOUTH,
    SOUTHEAST,
    CENTER,
    TALK,
    CURSOR
};

enum class ItemType
{
    TREE,
    CARROT,
    APPLE,
    LUMBER,
    RABBIT,
    SNOWBALL,
    DRABIS,
    RATCHET,
    SNEEZY,
    WABEKA
};

#endif
