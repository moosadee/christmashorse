#ifndef _CH2021_OFFSETABLE_SPRITE
#define _CH2021_OFFSETABLE_SPRITE

struct OffsetableSprite
{
    sf::Sprite sprite;
    sf::Vector2f originalPosition;
    ItemType type;

    void Draw( sf::RenderWindow& window, sf::Vector2f cameraOffset )
    {
        sprite.setPosition( originalPosition.x + cameraOffset.x, originalPosition.y + cameraOffset.y );
        window.draw( sprite );
    }
};

#endif
