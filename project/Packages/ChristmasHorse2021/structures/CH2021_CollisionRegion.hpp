#ifndef _CH2021_COLLISION_REGION
#define _CH2021_COLLISION_REGION

struct CollisionRegion
{
    sf::IntRect region;

    void Draw( sf::RenderWindow& window, sf::Vector2f cameraOffset )
    {
        sf::RectangleShape shape;
        shape.setPosition( region.left + cameraOffset.x, region.top + cameraOffset.y );
        shape.setFillColor( sf::Color( 255, 0, 0, 100 ) );
        shape.setSize( sf::Vector2f( region.width, region.height ) );
        window.draw( shape );
    }

    sf::IntRect GetOffsetRegion( sf::Vector2f cameraOffset )
    {
        sf::IntRect offset;
        offset = region;
        offset.left += cameraOffset.x;
        offset.top += cameraOffset.y;
        return offset;
    }

    sf::Vector2f GetCenterPoint( sf::Vector2f cameraOffset )
    {
        sf::IntRect offset = GetOffsetRegion( cameraOffset );
        sf::Vector2f center( offset.left + offset.width/2, offset.top + offset.height/2 );
        return center;
    }
};

#endif
