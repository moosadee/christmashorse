#ifndef _CH2021_GAME_STATE
#define _CH2021_GAME_STATE

#include <SFML/Audio.hpp>
#include <vector>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include "CH2021_Character.hpp"
#include "CH2021_Types.hpp"
#include "structures/CH2021_CollisionRegion.hpp"
#include "structures/CH2021_DialogBox.hpp"
#include "structures/CH2021_OffsetableSprite.hpp"
#include "structures/CH2021_Quest.hpp"

class CH2021_GameState : public EscapeyState//public chalo::IState
{
public:
    CH2021_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void ChangeDirection( Direction direction );
    void MovePlayer( Direction direction );
    void BumpHorse( CollisionRegion region );
    void ResetPosition();

    bool MouseOverHorse( OffsetableSprite horse, sf::Vector2i mousePos );

    void BeginTalkies( OffsetableSprite horse );

    bool HaveQuest( std::string questName );
    bool IsQuestComplete( std::string questName );
    int IndexOfQuest( std::string questName );
    bool AllQuestsCompleted();

    void Walkies();

private:
    std::string CLASSNAME;
    void UpdateCursorState();

    float m_velocityMax;
    float m_accelerationMax;
    sf::Vector2f m_velocity;
    sf::Vector2f m_acceleration;

    CH2021_Horse m_player;
    sf::Sprite m_cursor;
    Direction m_cursorState;

    std::vector<OffsetableSprite> m_items;
    std::vector<OffsetableSprite> m_horses;
    std::vector<CollisionRegion> m_collisionRegions;
    sf::Vector2f m_cameraOffset;
    float m_moveSpeed;
    float m_diagSpeed;

    DialogBox m_dialogBox;
    std::vector<Quest> m_quests;

    float m_mouseCooldown;
    float m_mouseCooldownMax;

    std::map<std::string, sf::SoundBuffer> m_soundBuffers;
    std::map<std::string, sf::Sound> m_sounds;
    int m_walkies;
    float m_walkiesCooldown;
    float m_walkiesCooldownMax;
    sf::Sprite m_hudBackgrounds[2];
    sf::Sprite m_hudPortrait;
    sf::RectangleShape m_hudMapBg;
    std::map<std::string, sf::Text> m_hudText;
};

#endif
