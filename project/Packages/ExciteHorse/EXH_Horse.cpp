#include "EXH_Horse.hpp"

#include "../../chalo-engine/Managers/DrawManager.hpp"

namespace ExciteHorse
{

Horse::Horse()
    : Character()
{
    m_acceleration.x = 0;
    m_acceleration.y = 0;
    m_accelerationDecay = 0.5;
    m_velocityDecay = 0.1;
    m_accelerateStep.x = 1.5;
    m_accelerateStep.y = 0.1;
    m_inclineAccelerate.x = 0;
    m_inclineAccelerate.y = 5;
    m_gravity = 0.1;
    m_maxAcceleration.y = 3;
    m_maxAcceleration.x = 3;
    m_maxVelocity.x = 3;
    m_maxVelocity.y = 2;
    m_velocity.x = 0;
    m_velocity.y = 0;
    m_screenScrollRate = 1;
    m_speed = 2; // vertical speed
    m_jumpTimer = 0;
    m_rotateSpeed = 5;
    m_clipclop = ClipClop::CLIP;
    m_state = "CLIP";
    m_speedupTimer = 0;
    m_dropTimer = 0;
    m_crossedFinishLine = false;
    SetBoundary( sf::IntRect( 100, 0, 1280, 720 ) );

    SetupNameText();
    m_namePositionOffset = sf::Vector2f( 0, -20 );
}

void Horse::SetScrollRate( float rate )
{
    m_screenScrollRate = rate;
}

void Horse::Update()
{
    Character::Update();

    sf::Vector2f pos = GetPosition();
    m_nameText.setPosition( pos.x + m_namePositionOffset.x, pos.y + m_namePositionOffset.y );

    if ( m_crossedFinishLine )
    {
        sf::Vector2f scale = m_sprite.getScale();
        scale.x += 0.02;
        scale.y += 0.01;
        if ( scale.x > 2 ) { scale.x = 2; }
        if ( scale.y > 2 ) { scale.y = 2; }
        m_sprite.setScale( scale );
        m_dropTimer = 0; // infinite drops

        m_angle++;
    }
    else if ( m_jumpTimer > 0 )
    {
        m_state = "JUMP";
        // Jumping
        m_jumpTimer++;

        if ( m_isColliding )
        {
            // Still colliding with incline
            m_acceleration.y -= m_inclineAccelerate.y;
            m_acceleration.x += m_inclineAccelerate.x;
        }
        else
        {
            // In the air, not colliding
            m_acceleration.y += m_gravity;
//            Rotate( 2 );
        }

        if ( m_position.y >= m_jumpInitialY )
        {
            // Hit the ground back in the same position
            HitGround();
        }

        if ( m_jumpTimer > 200 )
        {
            EndJump();
        }
    }
    else
    {
        // On ground
        if ( m_isColliding )
        {
            BeginJump();
        }

        // Decay acceleration
        if ( m_acceleration.x > 0 )
            m_acceleration.x -= m_accelerationDecay;
        if ( m_velocity.x > 0 )
            m_velocity.x -= m_velocityDecay;

        Animate();
        ForceSpriteUpdate();
    }

    if ( m_dropTimer > 0 )
    {
        m_dropTimer--;
    }

    EnforceBoundary();

    // Update velocity based on acceleration
    m_velocity.x += m_acceleration.x;
    m_velocity.y += m_acceleration.y;

    EnforceBoundary();

    // Update position based on velocity
    m_position.x += m_velocity.x;
    m_position.y += m_velocity.y;

    if ( m_speedupTimer > 0 )
    {
        m_speedupTimer--;
    }
}

void Horse::Clip()
{
    if ( m_clipclop == ClipClop::CLIP && m_jumpTimer <= 0 )
    {
        m_state = "CLIP";
        m_clipclop = ClipClop::CLOP;
        Accelerate();
    }
}

void Horse::Clop()
{
    if ( m_clipclop == ClipClop::CLOP && m_jumpTimer <= 0 )
    {
        m_state = "CLOP";
        m_clipclop = ClipClop::CLIP;
        Accelerate();
    }
}

void Horse::Accelerate()
{
    m_acceleration.x += m_accelerateStep.x;
//    std::cout << "POS:     " << m_position.x << ", " << m_position.y << "  "
//        << "VEL:     " << m_velocity.x << ", " << m_velocity.y << "  "
//        << "ACC: " << m_acceleration.x << ", " << m_acceleration.y << "  "
//        << "ROT: " << m_sprite.getRotation() << "  "
//        << "JT:  " << m_jumpTimer
//        << std::endl;
}

void Horse::Rotate( float amount )
{
    // Currently in the air?
    if ( m_jumpTimer > 0 )
    {
        m_angle = m_rotateSpeed * amount;  // ?? dunno
        std::cout << m_sprite.getRotation() << std::endl;
    }
}

void Horse::SetRotation( float amount )
{
  m_angle = amount;
}

void Horse::OnIncline()
{
    // Ignore a second collision if it's too soon!!
    if ( m_jumpTimer <= 0 || m_jumpTimer > 25 )
    {
        BeginJump();
        SetRotation( -45 );
    //    m_acceleration.y -= m_inclineAccelerate.y;
    //    m_acceleration.x += m_inclineAccelerate.x;
        IsColliding( true );
    }
}

void Horse::HitGround()
{
    int rotation = m_sprite.getRotation();
    if ( rotation < 45 || rotation > 360-45 )
    {
        EndJump();
        m_state = ( m_clipclop == ClipClop::CLIP ) ? "CLIP" : "CLOP";

        // speed boost
        if ( !m_badJump )
        {
            m_velocity.x = m_maxVelocity.x;
            m_acceleration.x = m_maxAcceleration.x;
        }
    }
    else
    {
        // Wipe out
        m_badJump = true;
        Bounce();
        m_state = "SKID";
    }
}

void Horse::BeginJump()
{
    std::cout << "Begin Jump" << std::endl;
    if ( m_jumpTimer == 0 )
    {
        // New jump
        m_jumpTimer = 1;
        m_jumpInitialY = m_position.y;
        m_position.y--;
        m_badJump = false;
    }
    else
    {
        m_position.y--;
    }
}

void Horse::EndJump()
{
    m_jumpTimer = 0;
    m_velocity.y = 0;
    m_acceleration.y = 0;
    SetRotation( 0 );
}

void Horse::EnforceBoundary()
{
    if ( m_position.x < m_moveBoundary.left )
    {
        m_position.x = m_moveBoundary.left;
    }
    else if ( m_position.x > m_moveBoundary.left + m_moveBoundary.width )
    {
        m_position.x = m_moveBoundary.left + m_moveBoundary.width;
    }

    if ( m_jumpTimer <= 0 && m_position.y < m_moveBoundary.top )
    {
        m_position.y = m_moveBoundary.top;
    }
    else if ( m_position.y > m_moveBoundary.top + m_moveBoundary.height )
    {
        m_position.y = m_moveBoundary.top + m_moveBoundary.height;
    }

    if ( m_speedupTimer > 0 )
    {
        return;
    }

    if      ( m_velocity.x > m_maxVelocity.x )    { m_velocity.x = m_maxVelocity.x; }
    else if ( m_velocity.x < -m_maxVelocity.x )   { m_velocity.x = -m_maxVelocity.x; }
    if      ( m_velocity.y > m_maxVelocity.y )    { m_velocity.y = m_maxVelocity.y; }
    else if ( m_velocity.y < -m_maxVelocity.y )   { m_velocity.y = -m_maxVelocity.y; }

    if      ( m_acceleration.x > m_maxAcceleration.x ) { m_acceleration.x = m_maxAcceleration.x; }
    else if ( m_acceleration.x < -m_maxAcceleration.x ) { m_acceleration.x = -m_maxAcceleration.x; }

    if      ( m_acceleration.y > m_maxAcceleration.y ) { m_acceleration.y = m_maxAcceleration.y; }
    else if ( m_acceleration.y < -m_maxAcceleration.y ) { m_acceleration.y = -m_maxAcceleration.y; }
}

void Horse::SetBoundary( sf::IntRect boundary )
{
    m_moveBoundary = boundary;
}


void Horse::DrawDebug( sf::RenderWindow& window )
{
    sf::IntRect collisionRegionAtPosition = GetObjectCollisionRegion();
//    collisionRegionAtPosition.left = m_position.x + region.left;
//    collisionRegionAtPosition.top = m_position.y + region.top;
//    collisionRegionAtPosition.width = region.width;
//    collisionRegionAtPosition.height = region.height;

    sf::RectangleShape shape;
    shape.setPosition( collisionRegionAtPosition.left, collisionRegionAtPosition.top );
    shape.setSize( sf::Vector2f( collisionRegionAtPosition.width, collisionRegionAtPosition.height ) );
    shape.setFillColor( sf::Color::Green );
//    shape.setFillColor( sf::Color::Transparent );
    window.draw( shape );

}

void Horse::Draw()
{
//    window.draw( m_sprite );

    // Draw info above head
    sf::Text text;
    text.setFont( chalo::FontManager::Get( "main" ) );
    text.setCharacterSize( 15 );
    text.setColor( sf::Color::White );

    sf::Vector2f pos = m_position;
    pos.x -= 15;
    if ( m_state == "CLIP" )
    {
        pos.y -= 40;
    }
    else
    {
        pos.y -= 60;
    }
    text.setPosition( pos );
    text.setString( m_state );
//    window.draw( text );
    chalo::DrawManager::AddHudText( text );
}

void Horse::IsColliding( bool val )
{
    m_isColliding = val;
}

void Horse::Move( chalo::Direction direction )
{
    if ( direction == chalo::Direction::WEST )
    {
        m_acceleration.x -= m_accelerateStep.x;
    }
}

void Horse::Move( chalo::Direction direction, const std::vector<int>& m_trackYs )
{

    // Can't move vertically while in the air
    if ( m_jumpTimer <= 0 )
    {
        chalo::Character::Move( direction );
    }
}

void Horse::Bounce( sf::IntRect otherPos )
{
    sf::IntRect myPos = GetObjectCollisionRegion();

    // Push down
    if ( myPos.top > otherPos.top )
    {
        m_position.y += 5;
    }
    else
    {
        m_position.y -= 5;
    }

    if ( myPos.left > otherPos.left )
    {
        m_position.x += 5;
    }
    else
    {
        m_position.x -= 5;
    }
}

void Horse::Bounce()
{
    m_acceleration.x = -m_acceleration.x;
    m_acceleration.y = -m_acceleration.y;
    m_velocity.x = -m_velocity.x;
    m_velocity.y = -m_velocity.y;
}

bool Horse::IsJumping()
{
    return ( m_jumpTimer > 0 );
}

void Horse::FallBack( float screenScroll )
{
    m_position.x -= screenScroll / 2;
}

void Horse::Speedup()
{
    m_speedupTimer = 10;
    m_velocity.x = m_maxVelocity.x * 2;
//    m_acceleration.x = m_maxAcceleration.x * 2;
}

void Horse::Slowdown()
{
    m_velocity.x = -m_maxVelocity.x;
    m_acceleration.x = -m_maxAcceleration.x;
}

void Horse::CrossFinishline()
{
    m_crossedFinishLine = true;
}

bool Horse::CanDrop()
{
    return ( m_dropTimer <= 0 );
}

void Horse::Dropped()
{
    m_dropTimer = 100;
    Speedup();
}



sf::Text& Horse::GetNameText()
{
    sf::Vector2f pos = GetPosition();
    m_nameText.setPosition( pos.x + m_namePositionOffset.x, pos.y + m_namePositionOffset.y );
    return m_nameText;
}

void Horse::SetProfileKey( std::string profileKey )
{
//    chalo::Logger::Out( "Set character profile key: " + profileKey, "ProfileCharacter::SetProfileKey" );
    m_profileKey = profileKey;
    // Set up the name information
//    std::string profileIndex = chalo::ConfigManager::Get( "PLAYER_" + m_profileKey + "_PROFILE" );
    std::string profileIndex = "1";
    std::string profileNameKey = "PROFILE_NAME_" + profileIndex;
//    m_profileName = chalo::ConfigManager::Get( profileNameKey );
    m_profileName = "Horse";//chalo::ConfigManager::Get( profileNameKey );
    m_nameText.setString( m_profileName );
//    chalo::Logger::Out( "Character name: " + m_profileName, "ProfileCharacter::SetProfileKey" );
    SetupNameText();
}

void Horse::SetNameOffset( sf::Vector2f offset )
{
    m_namePositionOffset = offset;
}

std::string Horse::GetProfileName()
{
    return m_profileName;
}

void Horse::SetupNameText()
{
//    chalo::Logger::Out( "Set up name text", "ProfileCharacter::SetupNameText" );
    m_nameText.setFont( chalo::FontManager::Get( "main" ) );
    m_nameText.setFillColor( sf::Color::White );
    m_nameText.setCharacterSize( 20 );
}

}

