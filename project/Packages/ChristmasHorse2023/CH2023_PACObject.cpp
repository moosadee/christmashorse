#include "CH2023_PACObject.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"

PointAndClickObject::PointAndClickObject()
{
  CLASSNAME = std::string( typeid( *this ).name() );

  m_mouseoverText.setFont( chalo::FontManager::Get( "main" ) );
  m_mouseoverText.setFillColor( sf::Color::White );
  m_mouseoverText.setOutlineColor( sf::Color::Black );
  m_mouseoverText.setOutlineThickness( 2 );
  m_mouseoverText.setCharacterSize( 15 );

  m_maxHP = 10;
  m_HP = 10;
  m_attackTimeoutMax = 50;
  m_attackTimeout = 0;
  m_hurtTimeoutMax = 50;
  m_hurtTimeout = 0;

  m_isPermanent = false;
}

void PointAndClickObject::SetStats( int maxHP )
{
  m_maxHP = maxHP;
  m_HP = m_maxHP;
}

int PointAndClickObject::GetHP() const
{
  return m_HP;
}

int PointAndClickObject::GetMaxHP() const
{
  return m_maxHP;
}

std::string PointAndClickObject::GetDefaultResultAction() const
{
  if ( m_dialogs.find( "dialog_default" ) == m_dialogs.end() ) { return ""; }
  return m_dialogs.at( "dialog_default" ).GetResultAction();
}

bool PointAndClickObject::IsAFighter() const
{
  if ( m_dialogs.find( "dialog_default" ) == m_dialogs.end() ) { return false; }
  return ( m_dialogs.at( "dialog_default" ).GetResultAction() == "fight" );
}

bool PointAndClickObject::CanAttack() const
{
  return ( m_attackTimeout <= 0 && m_HP > 0 );
}

void PointAndClickObject::BeginAttack()
{
  m_attackTimeout = m_attackTimeoutMax;
}

void PointAndClickObject::FullHeal()
{
  m_HP = m_maxHP;
}

void PointAndClickObject::SetHP( int hp )
{
  m_HP = hp;
}

void PointAndClickObject::Hurt( int damage )
{
  if ( m_hurtTimeout <= 0 )
  {
    m_HP -= damage;
    if ( m_HP < 0 ) { m_HP = 0; }
    m_hurtTimeout = m_hurtTimeoutMax;
  }
}

void PointAndClickObject::AddDialog( std::string key, CH2023_Dialog info )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
//  Logger::Out( "ADD DIALOG - Dialog key: " + key + "<br>"
//    + "text: " + Helper::VectorToString( info.text ) + "<br>"
//    + "action: " + info.action + "<br>"
//    + "color: [" + Helper::ToString( info.color.toInteger() ) + "]<br>"
//    + "timeout: " + Helper::ToString( info.timeout )
//    , "PointAndClickObject::AddDialog" );
  m_dialogs[ key ] = info;
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void PointAndClickObject::SetMouseoverText( std::string text )
{
  m_mouseoverText.setString( text );
}

InteractPayload PointAndClickObject::BeginInteraction( sf::Vector2f playerPos, const std::vector<std::string>& playerInventory )
{
  Logger::OutFuncBegin( "INTERACT - Object name: " + GetName(), CLASSNAME + "::" + std::string( __func__ ) );

  InteractPayload result;
  result.dialog = m_dialogs["dialog_default"];

  // Check conditional dialogs
  if ( Helper::Contains( m_dialogs["dialog_1"].GetTriggerCondition(), "have" ) )
  {
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Conditioned text", "PointAndClickObject::BeginInteraction" ); }

    // Inventory item required for this interaction, check for the item
    std::string mustHaveItem = Helper::Split( m_dialogs["dialog_1"].GetTriggerCondition(), "=" )[1];
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Must have item: " + mustHaveItem, "PointAndClickObject::BeginInteraction" ); }

    if ( find( playerInventory.begin(), playerInventory.end(), mustHaveItem ) != playerInventory.end() )
    {
      // Item is in inventory
      if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Found item in inventory", "PointAndClickObject::BeginInteraction" ); }
      result.dialog = m_dialogs["dialog_1"];
    }
  }
  else
  {
    //result.action = "NORESULT";
  }
  result.action = result.dialog.GetResultAction();

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
  return result;
}

void PointAndClickObject::DebugOut()
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutValue( "m_mouseoverText", m_mouseoverText.getString(), CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void PointAndClickObject::SetPermanentObject( bool value )
{
  m_isPermanent = value;
}

bool PointAndClickObject::IsPermanentObject() const
{
  return m_isPermanent;
}

sf::Color PointAndClickObject::GetColor() const
{
  return m_color;
}

void PointAndClickObject::SetColor( sf::Color color )
{
  m_color = color;
}

void PointAndClickObject::Update()
{
  chalo::Character::Update();

  if ( m_attackTimeout > 0 )
  {
    m_attackTimeout--;
  }
  if ( m_hurtTimeout > 0 )
  {
    m_hurtTimeout--;
    if ( m_hurtTimeout <= 0 )
    {
      m_sprite.setColor( sf::Color::White );
    }
  }
}

std::string PointAndClickObject::GetMouseoverText() const
{
  return m_mouseoverText.getString();
}

void PointAndClickObject::Draw( sf::RenderWindow& window, const sf::Vector2i& mousePosition )
{
  if ( m_HP <= 0 )
  {
    m_sprite.setOrigin( 0.5, 0.5 );
    m_sprite.setRotation( -90 );
    m_sprite.setPosition( sf::Vector2f( m_sprite.getPosition().x - 20, m_sprite.getPosition().y) );
  }

  if ( m_hurtTimeout > 0 )
  {
    float rat = float( m_hurtTimeout ) / m_hurtTimeoutMax;
    m_sprite.setColor( sf::Color( 255, 255 - 255 * rat, 255 - 255 * rat ) );
  }

  if ( m_attackTimeout > 0 )
  {
    float rat = float( m_attackTimeout ) / m_attackTimeoutMax;
    m_sprite.setScale( 0.2 * rat + 1, 0.2 * rat + 1 );
  }

   window.draw( GetSprite() );
   if ( SFMLHelper::PointInBoxCollision( mousePosition, GetPositionRegion() ) )
   {
     m_mouseoverText.setPosition( sf::Vector2f( mousePosition.x + 5, mousePosition.y - 15 ) );
     window.draw( m_mouseoverText );
   }
}

void PointAndClickObject::DebugDraw( sf::RenderWindow& window )
{
  sf::Text text;
  text.setFillColor( sf::Color::Yellow );
  text.setOutlineColor( sf::Color::Black );
  text.setOutlineThickness( 1 );
  text.setCharacterSize( 15 );
  text.setFont( chalo::FontManager::Get( "main" ) );
  text.setPosition( GetPosition() );
  text.setString( "m_name: \"" + m_name + "\"" );
  window.draw( text );
  text.setPosition( GetPosition().x, GetPosition().y + 15 );
  text.setString( "m_mouseoverText: \"" + m_mouseoverText.getString() + "\"" );
  window.draw( text );
}





