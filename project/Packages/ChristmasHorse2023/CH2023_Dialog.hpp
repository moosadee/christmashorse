#ifndef _CH2023_DIALOG
#define _CH2023_DIALOG

#include <SFML/Graphics.hpp>

struct CH2023_Dialog
{
  public:
  CH2023_Dialog();

  void Setup( CH2023_Dialog newDialog, std::string speakerKey, sf::Color speakerColor );
  std::string Update();
  void Draw( sf::RenderWindow& window, sf::Vector2f speakerPos );
  void Stop();

  std::string GetSpeakerKey() const;
  std::string GetTriggerCondition() const;
  std::string GetResultAction() const;
  bool IsCurrentlyTalking() const;
  std::string GetFirstLine() const;

  void SetText( std::vector<std::string> text );
  void SetTimeoutMax( int timeoutMax );
  void SetResultAction( std::string resultAction );
  void SetTriggerCondition( std::string triggerCondition );

  void DebugOut();

  private:
  std::string CLASSNAME;

  std::vector<std::string> m_text;
  std::string m_resultAction;
  std::string m_triggerCondition;
  std::string m_speakerKey;
  sf::Text m_renderText;
  int m_currentTextIndex;
  sf::Color m_color;
  float m_timeCounter;
  int m_timeoutMax;
};

#endif
