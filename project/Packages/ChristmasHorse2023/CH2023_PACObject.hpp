#ifndef _PACOBJECT
#define _PACOBJECT

#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "CH2023_Dialog.hpp"
#include <string>

struct InteractPayload
{
  std::string action;
  CH2023_Dialog dialog;
};

class PointAndClickObject : public chalo::Character
{
  public:
  PointAndClickObject();

  void SetStats( int maxHP );
  void SetHP( int hp );
  void Hurt( int damage );
  void FullHeal();
  int GetHP() const;
  int GetMaxHP() const;
  std::string GetDefaultResultAction() const;
  bool IsAFighter() const;
  bool CanAttack() const;
  void BeginAttack();

  void SetMouseoverText( std::string text );
  void Draw( sf::RenderWindow& window, const sf::Vector2i& mousePosition );
  InteractPayload BeginInteraction( sf::Vector2f playerPos, const std::vector<std::string>& playerInventory );
  void Update();
  void AddDialog( std::string key, CH2023_Dialog info );
  void DebugOut();
  bool IsPermanentObject() const;
  void SetPermanentObject( bool value );
  sf::Color GetColor() const;
  void SetColor( sf::Color color );
  std::string GetMouseoverText() const;
  void DebugDraw( sf::RenderWindow& window );

  private:
  std::string CLASSNAME;
  sf::Text m_mouseoverText;
  bool m_isPermanent;
  sf::Color m_color;
  std::map<std::string, CH2023_Dialog> m_dialogs;
  int m_HP, m_maxHP;
  int m_attackTimeout, m_attackTimeoutMax;
  int m_hurtTimeout, m_hurtTimeoutMax;
};

#endif
