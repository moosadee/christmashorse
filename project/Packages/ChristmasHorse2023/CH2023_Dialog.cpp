#include "CH2023_Dialog.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"

CH2023_Dialog::CH2023_Dialog()
{
  CLASSNAME = std::string( typeid( *this ).name() );
  m_currentTextIndex = 0;
  m_timeCounter = 0;
}

void CH2023_Dialog::DebugOut()
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );

  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
  {
    Logger::OutValue( "m_resultAction", m_resultAction, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutValue( "m_triggerCondition", m_triggerCondition, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutValue( "m_speakerKey", m_speakerKey, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutIntValue( "m_currentTextIndex", m_currentTextIndex, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutFloatValue( "m_timeCounter", m_timeCounter, CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutIntValue( "m_text.size()", m_text.size(), CLASSNAME + "::" + std::string( __func__ ) );
    for ( auto& str : m_text )
    {
      Logger::OutValue( "* text", str, CLASSNAME + "::" + std::string( __func__ ) );
    }
  }
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_Dialog::Stop()
{
  m_currentTextIndex = 0;
  m_timeCounter = 0;
}

void CH2023_Dialog::Setup( CH2023_Dialog newDialog, std::string speakerKey, sf::Color speakerColor )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );

  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Copy data from newDialog", CLASSNAME + "::" + std::string( __func__ ) ); }
  m_timeoutMax = newDialog.m_timeoutMax;
  m_text = newDialog.m_text;
  m_triggerCondition = newDialog.m_triggerCondition;
  m_resultAction = newDialog.m_resultAction;

  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Setup m_renderText", CLASSNAME + "::" + std::string( __func__ ) ); }
  m_renderText.setFont( chalo::FontManager::Get( "main" ) );
  m_renderText.setFillColor( sf::Color::Cyan );
  m_renderText.setOutlineColor( sf::Color::Black );
  m_renderText.setOutlineThickness( 2 );
  m_renderText.setCharacterSize( 20 );
  m_timeCounter = 0;

  // Perform this dialog operation
  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Setup m_renderText", CLASSNAME + "::" + std::string( __func__ ) ); }
  m_timeCounter = m_timeoutMax;
  m_currentTextIndex = 0;
  m_renderText.setString( m_text[ m_currentTextIndex ] );
  m_speakerKey = speakerKey;
  m_color = speakerColor;
  m_renderText.setFillColor( m_color );

//  DebugOut();

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

std::string CH2023_Dialog::GetSpeakerKey() const
{
  return m_speakerKey;
}

std::string CH2023_Dialog::GetTriggerCondition() const
{
  return m_triggerCondition;
}

std::string CH2023_Dialog::GetResultAction() const
{
  return m_resultAction;
}

bool CH2023_Dialog::IsCurrentlyTalking() const
{
  return ( m_timeCounter > 0 );
}

std::string CH2023_Dialog::GetFirstLine() const
{
  if ( m_text.size() > 0 )
  {
    return m_text[0];
  }
  return "";
}

void CH2023_Dialog::SetText( std::vector<std::string> text )
{
  m_text = text;
}

void CH2023_Dialog::SetTimeoutMax( int timeoutMax )
{
  m_timeoutMax = timeoutMax;
}

void CH2023_Dialog::SetResultAction( std::string resultAction )
{
  m_resultAction = resultAction;
}

void CH2023_Dialog::SetTriggerCondition( std::string triggerCondition )
{
  m_triggerCondition = triggerCondition;
}

std::string CH2023_Dialog::Update()
{
//  Logger::OutIntValue( "m_timeCounter", m_timeCounter, "CH2023_Dialog::Update" );
  if ( m_timeCounter > 0 )
  {
    m_timeCounter--;
    if ( m_timeCounter == 0 )
    {
      m_currentTextIndex++;
      // More dialog to show?
      if ( m_currentTextIndex < m_text.size() )
      {
        m_renderText.setString( m_text[ m_currentTextIndex ] );
        m_timeCounter = m_timeoutMax;
        return m_text[ m_currentTextIndex ];
      }
      else
      {
        return "DONE";
      }
    }
  }
  return "";
}

void CH2023_Dialog::Draw( sf::RenderWindow& window, sf::Vector2f speakerPos )
{
  if ( IsCurrentlyTalking() )
  {
    float textWidth = m_renderText.getLocalBounds().width;
    m_renderText.setPosition( sf::Vector2f( speakerPos.x - textWidth/2, speakerPos.y - 100 ) );
    window.draw( m_renderText );
  }
}


