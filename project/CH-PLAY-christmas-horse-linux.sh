#/bin/sh
echo ""
echo "Sorry if this doesn't work, I'm really tired."
echo "Check the \"CH-linux-dependencies.txt\" file for libraries needed to run the game."
echo "The easiest install would be like..."
echo "sudo apt-get install libsfml-audio2.5 libsfml-graphics2.5 libsfml-system2.5 libsfml-window2.5"
echo ""
echo "--Rachel"
echo ""
ln -s libsfml-audio.so.2.5.1 libsfml-audio.so.2.5
ln -s libsfml-graphics.so.2.5.1 libsfml-graphics.so.2.5
ln -s libsfml-system.so.2.5.1 libsfml-system.so.2.5
ln -s libsfml-window.so.2.5.1 libsfml-window.so.2.5
ln -s libFLAC.so.8.3.0 libFLAC.so.8
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:."
./christmas-horse-linux-exe
