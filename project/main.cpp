#include "Packages/ProgramBase/Application/ChaloEngineProgram.hpp"
#include "Packages/ProgramBase/StartupState.hpp"

#include "chalo-engine/Utilities/Helper.hpp"

int main()
{
    ChaloEngineProgram program;
    program.Run();
}
