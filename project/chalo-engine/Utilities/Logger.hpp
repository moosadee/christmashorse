#ifndef _KUKO_LOGGER
#define _KUKO_LOGGER

#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>

struct LogItems
{
    LogItems()
    {
      this->logFunctionBegin = false;
      this->logFunctionEnd = false;
      this->logMenuElements = false;
    }

    LogItems( bool logFunctionBegin, bool logFunctionEnd, bool logMenuElements )
    {
      this->logFunctionBegin = logFunctionBegin;
      this->logFunctionEnd = logFunctionEnd;
      this->logMenuElements = logMenuElements;
    }

    bool logFunctionBegin = false;
    bool logFunctionEnd = false;
    bool logMenuElements = false;
};

class Logger
{
    public:
    static void Setup( bool loud = false );
    static void Setup( int logLevel, const std::string& filter );
    static void Cleanup();
    static void SetLogLevel( int val );
    static void SetFilterWord( const std::string& filter );

    static void OutFuncBegin( const std::string& message = "", const std::string& location = "", const std::string& category = "function-trace" );
    static void OutFuncEnd( const std::string& message = "", const std::string& location = "", const std::string& category = "function-trace" );
    static void Out( const std::string& message, const std::string& location = "", const std::string& category = "", bool condition = true, int level = 0 );
    static void OutIntValue( const std::string& label, int value, const std::string& location = "", const std::string& category = "", bool condition = true, int level = 0 );
    static void OutFloatValue( const std::string& label, float value, const std::string& location = "", const std::string& category = "", bool condition = true, int level = 0 );
    static void OutBoolValue( const std::string& label, bool value, const std::string& location = "", const std::string& category = "", bool condition = true, int level = 0 );
    static void OutValue( const std::string& label, const std::string& value, const std::string& location = "", const std::string& category = "", bool condition = true, int level = 0 );
    static void OutHighlight( const std::string& message, const std::string& location = "", int color = 1);
    static void Error( const std::string& message, const std::string& location = "" );
    static void Debug( const std::string& message, const std::string& location = "" );

    static double GetTimestamp();
    static std::string GetFormattedTimestamp();

    static void Push( std::string stateName );
    static void Pop();
    static std::string GetStackString();

    static void SetLogItems( LogItems items );
    static LogItems GetLogItems();
    static std::string LogLocation( std::string className, const char functionName[] );

    private:
    static LogItems m_logItems;
    static std::ofstream m_file;
    static time_t m_startTime;
    static time_t m_lastTimestamp;
    static int m_logLevel;

    static std::string m_categoryFilter;
    static int m_rowCount;
    static bool m_isLoud;
    static std::vector<std::string> m_functionStack;
};

#endif
