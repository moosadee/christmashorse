// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "GameObject.hpp"

namespace chalo
{

GameObject::GameObject()
{
    m_mapCollisionRectangle.left = 0;
    m_mapCollisionRectangle.top = 0;
    m_mapCollisionRectangle.width = 32;
    m_mapCollisionRectangle.height = 32;
    m_objectCollisionRectangle.left = 0;
    m_objectCollisionRectangle.top = 0;
    m_objectCollisionRectangle.width = 32;
    m_objectCollisionRectangle.height = 32;
    m_debugRectColor = sf::Color( 255, 255, 255, 100 );
    m_activeState = ACTIVE;
    m_ptrWindow = nullptr;
    m_scale = sf::Vector2f( 1, 1 );
}

void GameObject::SetMapCollisionRectangle( sf::IntRect rect )
{
    m_mapCollisionRectangle = rect;
}

void GameObject::SetObjectCollisionRectangle( sf::IntRect rect )
{
    m_objectCollisionRectangle = rect;
}

void GameObject::Setup( int initialX /* = 0 */, int initialY /* = 0 */, const std::string& name /* = "unnamed" */ )
{
    // TODO: Should this set up the sprite too?
    m_position.x = initialX;
    m_position.y = initialY;
    m_name = name;
}

void GameObject::SetTexture( const sf::Texture& texture, sf::IntRect& textureCoordinates )
{
    m_sprite.setTexture( texture );
    m_textureCoordinates = textureCoordinates;
    m_sprite.setTextureRect( m_textureCoordinates );
}

void GameObject::SetTexture( const sf::Texture& texture, int textureLeft, int textureTop, int textureWidth, int textureHeight )
{
    sf::IntRect textureRect;
    textureRect.left = textureLeft;
    textureRect.top = textureTop;
    textureRect.width = textureWidth;
    textureRect.height = textureHeight;

    SetTexture( texture, textureRect );
}

void GameObject::SetTextureRegion( const sf::IntRect& textureCoordinates )
{
    m_textureCoordinates = textureCoordinates;
    m_sprite.setTextureRect( m_textureCoordinates );
    m_sprite.setOrigin( m_textureCoordinates.width/2, m_textureCoordinates.height/2 );
}

void GameObject::SetPosition( sf::Vector2f position )
{
    m_position = position;
    m_sprite.setPosition( m_position );
}

void GameObject::SetPosition( int x, int y )
{
    m_position.x = x;
    m_position.y = y;
    m_sprite.setPosition( m_position );
}

void GameObject::Update()
{
    m_sprite.setPosition( m_position );
}

const sf::Sprite& GameObject::GetSprite() const
{
    return m_sprite;
}

sf::Sprite GameObject::GetSpriteWithOffset( const Camera& camera ) const
{
    sf::Sprite adjustedSprite = m_sprite;
    adjustedSprite.setPosition( m_sprite.getPosition().x - camera.GetX(), m_sprite.getPosition().y - camera.GetY() );
    return adjustedSprite;
}

bool GameObject::IsOffscreen( int minX, int minY, int maxX, int maxY ) const
{
    return (
        m_position.x < minX ||
        m_position.x > maxX ||
        m_position.y < minY ||
        m_position.y > maxY
    );
}

/**
   Returns the collision region vs. tiles, relative to the screen (i.e., offset by the object's x, y position, not relative to the object's image.)
 */
sf::IntRect GameObject::GetMapCollisionRegion() const
{
    sf::IntRect adjustedRect( m_mapCollisionRectangle );
    adjustedRect.left += m_position.x;
    adjustedRect.top += m_position.y;

    return adjustedRect;
}

/**
   Returns the collision region vs. objects, relative to the screen (i.e., offset by the object's x, y position, not relative to the object's image.)
*/
sf::IntRect GameObject::GetObjectCollisionRegion() const
{
    sf::IntRect adjustedRect( m_objectCollisionRectangle );
    adjustedRect.left += m_position.x;
    adjustedRect.top += m_position.y;

    return adjustedRect;
}

sf::IntRect GameObject::GetPositionRegion() const
{
    sf::IntRect rect;
    rect.left = m_position.x;
    rect.top = m_position.y;
    rect.width = m_textureCoordinates.width;
    rect.height = m_textureCoordinates.height;
    return rect;
}

sf::Vector2f GameObject::GetPosition() const
{
    return m_position;
}

sf::Vector2f GameObject::GetDimensions() const
{
    return sf::Vector2f( m_textureCoordinates.width, m_textureCoordinates.height );
}

sf::Vector2f GameObject::GetCenterPosition() const
{
    return sf::Vector2f( m_position.x + m_textureCoordinates.width / 2, m_position.y + m_textureCoordinates.height / 2 );
}

sf::IntRect GameObject::GetTextureRegion() const
{
    return m_textureCoordinates;
}

std::string GameObject::GetName() const
{
    return m_name;
}

void GameObject::SetName( std::string name )
{
  m_name = name;
}

ActiveState GameObject::GetActiveState() const
{
    return m_activeState;
}

void GameObject::SetActiveState( ActiveState state )
{
    m_activeState = state;
}

void GameObject::SetScale( sf::Vector2f scale )
{
    m_scale = scale;
    m_sprite.setScale( m_scale );
}

void GameObject::SetScale( float x, float y )
{
    SetScale( sf::Vector2f( x, y ) );
}

sf::Vector2f GameObject::GetScale() const
{
    return m_scale;
}

}
