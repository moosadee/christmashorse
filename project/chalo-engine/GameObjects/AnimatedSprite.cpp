#include "AnimatedSprite.hpp"

#include "../Utilities/Logger.hpp"

namespace chalo
{

AnimatedSprite::AnimatedSprite()
{
    m_defaultActionName = "";
}

void AnimatedSprite::Setup( const sf::Texture& texture, int maxFrames, float animationSpeed, int width, int height )
{
    m_maxFrames = maxFrames;
    m_animationSpeed = animationSpeed;
    m_sprite.setTexture( texture );
    m_currentFrame = 0;
    m_frameRect.width = width;
    m_frameRect.height = height;
    m_sprite.setOrigin( sf::Vector2f( width/2, height/2 ) );
}

void AnimatedSprite::CreateAction( std::string name, int row, bool repeats, bool interruptable )
{
    m_actionRows[ name ].row = row;
    m_actionRows[ name ].repeats = repeats;
    m_actionRows[ name ].interruptable = interruptable;

    if ( m_defaultActionName == "" )
    {
        m_defaultActionName = name;
    }

    Logger::OutValue( "CREATE ACTION", m_currentActionName, "AnimatedSprite::CreateAction" );
    Logger::OutValue( "name", name, "AnimatedSprite::CreateAction" );
    Logger::OutIntValue( "row", m_actionRows[ name ].row, "AnimatedSprite::CreateAction" );
    Logger::OutIntValue( "repeats", m_actionRows[ name ].repeats, "AnimatedSprite::CreateAction" );
    Logger::OutIntValue( "interruptable", m_actionRows[ name ].interruptable, "AnimatedSprite::CreateAction" );
}

void AnimatedSprite::SetAction( std::string action )
{
    if ( action != m_currentActionName )
    {
        m_currentFrame = 0;
    }

    if ( m_actionRows[ m_currentActionName ].interruptable )
    {
        m_currentActionName = action;
    }
}

std::string AnimatedSprite::GetAction()
{
    return m_currentActionName;
}

void AnimatedSprite::IncrementFrame()
{
    m_currentFrame += m_animationSpeed;
    if ( m_currentFrame >= m_maxFrames )
    {
        if ( m_actionRows[ m_currentActionName ].repeats )
        {
            m_currentFrame = 0;
        }
        else
        {
            m_currentActionName = m_defaultActionName;
            m_currentFrame = 0;
        }
    }
}

void AnimatedSprite::SetPosition( const sf::Vector2f& position )
{
    m_sprite.setPosition( position );
}

const sf::Vector2f& AnimatedSprite::GetPosition()
{
    return m_sprite.getPosition();
}

void AnimatedSprite::SetScale( const sf::Vector2f& scale )
{
    m_sprite.setScale( scale );
}

float AnimatedSprite::GetCurrentFrame()
{
    return m_currentFrame;
}

const sf::Vector2f& AnimatedSprite::GetScale()
{
    return m_sprite.getScale();
}

sf::Sprite& AnimatedSprite::GetClippedSprite()
{
    if ( m_currentActionName == "" )
    {
        m_currentActionName = m_defaultActionName;
    }

    m_frameRect.left = int(m_currentFrame) * m_frameRect.width;
    m_frameRect.top = m_actionRows[ m_currentActionName ].row * m_frameRect.height;
    m_sprite.setTextureRect( m_frameRect );
    return m_sprite;
}

}
