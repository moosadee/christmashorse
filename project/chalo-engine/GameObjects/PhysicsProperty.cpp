#include "PhysicsProperty.hpp"

namespace chalo
{

PhysicsProperty::PhysicsProperty()
{
    m_velocity.x = 0;
    m_velocity.y = 0;
}

void PhysicsProperty::Setup( const sf::Vector2f& accRate, const sf::Vector2f& deaccRate, const sf::Vector2f& maxVelocity )
{
    m_velocity.x = 0;
    m_velocity.y = 0;
    m_deaccelerationRate = deaccRate;
    m_accelerationRate = accRate;
    m_maxVelocity = maxVelocity;
}

void PhysicsProperty::SetMaxVelocity( const sf::Vector2f& maxVelocity )
{
    m_maxVelocity = maxVelocity;
}

void PhysicsProperty::SetVelocityX( float value )
{
    m_velocity.x = value;
}

void PhysicsProperty::SetVelocityY( float value )
{
    m_velocity.y = value;
}

void PhysicsProperty::Accelerate( const sf::Vector2f& directions )
{
    m_velocity.x += ( directions.x * m_accelerationRate.x );
    m_velocity.y += ( directions.y * m_accelerationRate.y );

    if ( m_velocity.x > m_maxVelocity.x )
    {
        m_velocity.x = m_maxVelocity.x;
    }
    else if ( m_velocity.x < -m_maxVelocity.x )
    {
        m_velocity.x = -m_maxVelocity.x;
    }
    if ( m_velocity.y > m_maxVelocity.y )
    {
        m_velocity.y = m_maxVelocity.y;
    }
    else if ( m_velocity.y < -m_maxVelocity.y )
    {
        m_velocity.y = -m_maxVelocity.y;
    }
}

void PhysicsProperty::Deaccelerate( bool horizontally, bool vertically )
{
    if ( horizontally )
    {
        if ( m_velocity.x < -0.5 )
        {
            m_velocity.x += m_deaccelerationRate.x;
        }
        else if ( m_velocity.x > 0.5 )
        {
            m_velocity.x -= m_deaccelerationRate.x;
        }
    }

    if ( vertically )
    {
        if ( m_velocity.y < -0.5 )
        {
            m_velocity.y += m_deaccelerationRate.y;
        }
        else if ( m_velocity.y > 0.5 )
        {
            m_velocity.y -= m_deaccelerationRate.y;
        }
    }


    if ( -0.5 < m_velocity.x && m_velocity.x < 0.5 )
    {
        m_velocity.x = 0;
    }
    if ( -0.5 < m_velocity.y && m_velocity.y < 0.5 )
    {
        m_velocity.y = 0;
    }
}

const sf::Vector2f& PhysicsProperty::GetVelocity()
{
    return m_velocity;
}

}
