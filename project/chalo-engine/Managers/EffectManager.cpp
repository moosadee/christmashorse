#include "EffectManager.hpp"
#include "DrawManager.hpp"
#include <string>
#include <list>

namespace chalo
{

std::string EffectManager::CLASSNAME = "EffectManager";
std::vector<Effect> EffectManager::m_effects;
Effect EffectManager::m_defaultStyle;

void EffectManager::Update()
{
  std::list<int> removeIndices;
  // Update effects
  for ( size_t i = 0; i < m_effects.size(); i++ )
  {
    sf::Vector2f pos = m_effects[i].text.getPosition();
    if ( m_effects[i].behavior == Behavior::FLOAT_UP )
    {
      pos.y--;
    }
    m_effects[i].text.setPosition( pos );
    m_effects[i].lifeCounter--;
    if ( m_effects[i].lifeCounter <= 0 )
    {
      removeIndices.push_front( i );
    }
  }

  // Remove dead effects
  for ( auto& idx : removeIndices )
  {
    m_effects.erase( m_effects.begin() + idx );
  }
}

void EffectManager::Draw( sf::RenderWindow& window )
{
  for ( auto& effect : m_effects )
  {
    chalo::DrawManager::AddHudText( effect.text );
//    window.draw( effect.text );
  }
}

void EffectManager::AddEffect( Effect effect )
{
  m_effects.push_back( effect );
}

void EffectManager::AddEffect( std::string text, sf::Color color, sf::Vector2f pos )
{
    Effect effect = m_defaultStyle;
    effect.text.setString( text );
    effect.text.setColor( color );

    if ( m_effects.size() > 0 && m_effects[ m_effects.size()-1 ].text.getPosition().y > 200 )
    {
        pos.y = m_effects[ m_effects.size()-1 ].text.getPosition().y + 25;
    }

    if ( pos.x < 100 ) { pos.x = 100; }
    if ( pos.y > 600 ) { pos.y = 400; }

    effect.text.setPosition( pos );
    m_effects.push_back( effect );
}

void EffectManager::SetupEffectDefault( Effect effect )
{
    m_defaultStyle = effect;
}

}
