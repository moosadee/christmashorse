// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "AudioManager.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "ConfigManager.hpp"

#include <stdexcept>
#include <list>

namespace chalo
{

std::string AudioManager::CLASSNAME = "AudioManager";
std::map<std::string, sf::Music> AudioManager::m_music;
std::map<std::string, sf::SoundBuffer> AudioManager::m_soundBuffer;
std::vector<sf::Sound> AudioManager::m_sfx;

void AudioManager::AddMusic( const std::string& key, const std::string& path )
{
    Logger::Out( "Adding music with key \"" + key + "\" to path \"" + path + "\"", "AudioManager::AddMusic", "function-trace" );

    if ( !m_music[ key ].openFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load music at path \"" + path + "\"", "AudioManager::AddMusic" );
        return;
    }

    m_music[ key ].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music[ key ].setLoop( true );
}

void AudioManager::AddSoundBuffer( const std::string& key, const std::string& path )
{
    Logger::Out( "Adding sound buffer with key \"" + key + "\" to path \"" + path + "\"", "AudioManager::AddSoundBuffer", "function-trace" );
    sf::SoundBuffer buffer;
    if ( !buffer.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load sound bufferat path \"" + path + "\"", "AudioManager::AddSoundBuffer" );
        return;
    }

    m_soundBuffer[ key ] = buffer;
}

void AudioManager::Clear()
{
    Logger::Out( "", "AudioManager::Clear", "function-trace" );
    m_music.clear();
    m_soundBuffer.clear();
}

const sf::Music& AudioManager::GetMusic( const std::string& key )
{
    if ( m_music.find( key ) == m_music.end() )
    {
        // Not found
        Logger::Error( "Could not find music with key \"" + key + "\"", "AudioManager::GetMusic" );
        throw std::runtime_error( "Could not find music with key \"" + key + "\" + AudioManager::GetMusic" );
    }

    return m_music[ key ];
}

const sf::SoundBuffer& AudioManager::GetSoundBuffer( const std::string& key )
{
    if ( m_soundBuffer.find( key ) == m_soundBuffer.end() )
    {
        // Not found
        Logger::Error( "Could not find sound buffer with key \"" + key + "\"", "AudioManager::GetSoundBuffer" );
        throw std::runtime_error( "Could not find sound buffer with key \"" + key + "\" + AudioManager::GetSoundBuffer" );
    }

    return m_soundBuffer[ key ];
}

void AudioManager::PlayMusic( const std::string& key, bool repeat /* = true */ )
{
    if ( m_music.find( key ) == m_music.end() )
    {
        // Not found
        Logger::Error( "Could not find music with key \"" + key + "\"", "AudioManager::PlayMusic" );
        return;
    }

    m_music[ key ].setVolume( Helper::StringToInt( ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music[ key ].setLoop( repeat );
    m_music[ key ].play();
}

void AudioManager::PlaySound( const std::string& key )
{
    std::list<int> removeIndices;
    for ( size_t i = 0; i < m_sfx.size(); i++ )
    {
        if ( m_sfx[i].getStatus() == sf::SoundSource::Status::Stopped )
        {
          removeIndices.push_front( i );
        }
    }

    for ( auto& index : removeIndices )
    {
      m_sfx.erase( m_sfx.begin() + index );
    }

    if ( m_soundBuffer.find( key ) == m_soundBuffer.end() )
    {
        // Not found
        Logger::Error( "Could not find sound buffer with key \"" + key + "\"", "AudioManager::PlaySound" );
        return;
    }

    m_sfx.push_back( sf::Sound( m_soundBuffer[ key ] ) );
    m_sfx[ m_sfx.size()-1 ].setVolume( Helper::StringToInt( ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_sfx[ m_sfx.size()-1 ].play();
}

void AudioManager::StopAllMusic()
{
    for ( auto& musicPair : m_music )
    {
        Logger::Out( "Stop playing \"" + musicPair.first + "\"", "AudioManager::StopAllMusic" );
        musicPair.second.stop();
    }
}

}
