#include "PathManager.hpp"
#include "../Utilities/Logger.hpp"

#include <cstdlib>

std::string PathManager::basePath;

void PathManager::Setup()
{
    Logger::OutFuncBegin( "Function begin", "PathManager::" + std::string( __func__ ) );
    OperatingSystem os = Platform::GetOperatingSystem();

    basePath = "./";

//    if ( os == OperatingSystem::LINUX )
//    {
//        system( "mkdir ~/.moosadee" );
//        system( "mkdir ~/.moosadee/christmashorse" );
//        basePath = "~/.moosadee/christmashorse/";
//    }
//    else
//    {
//        basePath = "./";
//    }

    Logger::Out( "Base path is \"" + basePath + "\"", "PathManager::" + std::string( __func__ ) );
    CreateConfigs();

    Logger::OutFuncEnd( "normal", "PathManager::" + std::string( __func__ ) );
}

std::string PathManager::GetPath()
{
    return basePath;
}

void PathManager::CreateConfigs()
{
}
