// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "StateManager.hpp"
#include "InputManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Messager.hpp"
#include "../States/ErrorState.hpp"

namespace chalo
{
std::string StateManager::CLASSNAME = "StateManager";

void StateManager::InitManager()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    m_ptrCurrentState = nullptr;
    m_nameCurrentState = "";
    Logger::Out( "I'm gonna create a fallback \"error\" state to fall back to in case there is an error. This isn't an actual error happening. Just FYI.", Logger::LogLocation( CLASSNAME, __func__ ) );
    AddInactiveState( "error", new ErrorState );
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void StateManager::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    for ( auto& state : m_states )
    {
        if ( state.second != nullptr )
        {
            delete state.second;
            state.second = nullptr;
        }
    }
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

std::string StateManager::SanitizeStateName( std::string original ) const
{
    return Helper::ToLower( original );
}

void StateManager::AddInactiveState( std::string stateName, IState* state )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    Logger::Out( "State name: [" + stateName + "], size: " + to_string( sizeof( *state ) ) + " bytes", Logger::LogLocation( CLASSNAME, __func__ ) );
    stateName = SanitizeStateName( stateName );
    state->Init( stateName );
    m_states[stateName] = state;
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void StateManager::ClearState( std::string stateName )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    stateName = SanitizeStateName( stateName );
    Logger::Out( "Clear state name: \"" + stateName + "\"", Logger::LogLocation( CLASSNAME, __func__ ) );

    if ( m_states.find( stateName ) == m_states.end() )
    {
        Logger::Error( "Could not find state with name " + stateName + "!", Logger::LogLocation( CLASSNAME, __func__ ) );
        Logger::OutFuncEnd( "error", Logger::LogLocation( CLASSNAME, __func__ ) );
        return;
    }

    m_states[stateName]->Cleanup();

//    if ( m_states[stateName] != nullptr )
//    {
//        delete m_states[stateName];
//        m_states[stateName] = nullptr;
//    }
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

bool StateManager::ChangeState( std::string stateName )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    stateName = SanitizeStateName( stateName );
//    if ( m_ptrCurrentState != nullptr ) { Logger::OutHighlight( "Current state: \"" + m_ptrCurrentState->GetName() + "\"", Logger::LogLocation( CLASSNAME, __func__ ) ); }
//    Logger::Out( "Change state to: \"" + stateName + "\"", Logger::LogLocation( CLASSNAME, __func__ ) );

    // Disable the click for now.
    chalo::InputManager::DisableClick();

    if ( m_ptrCurrentState != nullptr )
    {
//        m_ptrCurrentState->Cleanup(); // Redundant
        ClearState( m_nameCurrentState );
    }

    if ( m_states.find( stateName ) == m_states.end() )
    {
        // State not found!
        Logger::Error( "FATAL ERROR! State key \"" + stateName + "\" not found in list of states!", Logger::LogLocation( CLASSNAME, __func__ ) );
        Messager::Set( "error", "FATAL ERROR! State key \"" + stateName + "\" not found in list of states! / StateManager::ChangeState" );

        if ( stateName == "error" )
        {
            // Let's not recurse a bunch of times.
            Logger::Error( "Error! Can't find the \"error\" state!", Logger::LogLocation( CLASSNAME, __func__ ) );
            Logger::Error( "Available state keys", Logger::LogLocation( CLASSNAME, __func__ ) );
            // std::map< std::string, IState* > m_states;
            for ( auto& state : m_states )
            {
                Logger::Error( "\"" + state.first + "\"", Logger::LogLocation( CLASSNAME, __func__ ) );
            }
        }
        else
        {
            ChangeState( "error" );
        }

        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        return false;
    }

    std::string stateKeys = "";
    for ( auto& state : m_states )
    {
        if ( stateKeys != "" ) { stateKeys += ", "; }
        stateKeys += "[" + state.first;
        if ( state.second == nullptr ) { stateKeys += "=nullptr"; }
        stateKeys += "]";
    }
    Logger::OutValue( "Available state keys", stateKeys, Logger::LogLocation( CLASSNAME, __func__ ) );

    m_nameCurrentState = stateName;
    m_ptrCurrentState = m_states[ m_nameCurrentState ];

    Logger::OutValue( "m_ptrCurrentState:", Helper::ToString( m_ptrCurrentState ), Logger::LogLocation( CLASSNAME, __func__ ) );

    if ( m_ptrCurrentState == nullptr )
    {
        Logger::Error( "Error! State was changed to nullptr when switching to \"" + stateName + "\"!", Logger::LogLocation( CLASSNAME, __func__ ) );
        Logger::OutFuncEnd( "error", Logger::LogLocation( CLASSNAME, __func__ ) );
        return false;
    }

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
    return true;
}

bool StateManager::HasState( std::string stateName ) const
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    stateName = SanitizeStateName( stateName );
    Logger::Out( "Is there a state named \"" + stateName + "\"?", Logger::LogLocation( CLASSNAME, __func__ ) );
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
    return ( m_states.find( stateName ) != m_states.end() );
}

bool StateManager::HasActiveState()
{
  return ( m_ptrCurrentState != nullptr );
}

void StateManager::ActivateState()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
//    Logger::Out( "Activate state BEFORE", Logger::LogLocation( CLASSNAME, __func__ ) );
    Logger::OutValue( "m_ptrCurrentState", Helper::ToString( m_ptrCurrentState ), Logger::LogLocation( CLASSNAME, __func__ ) );
    m_ptrCurrentState->Setup();
//    Logger::Out( "Activate state AFTER", Logger::LogLocation( CLASSNAME, __func__ ) );
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void StateManager::DeactivateState()
{
//    m_ptrCurrentState->Cleanup(); // Redundant; handled in ClearState.
}

void StateManager::SetupState()
{
    m_ptrCurrentState->Setup();
}

void StateManager::UpdateState()
{
    m_ptrCurrentState->Update();
}

void StateManager::DrawState( sf::RenderWindow& window )
{
    m_ptrCurrentState->Draw( window );
}

std::string StateManager::GetGotoState()
{
    return m_ptrCurrentState->GetGotoState();
}

void StateManager::ChangeActiveState( std::string stateName )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
//    Logger::OutHighlight( "CHANGE ACTIVE STATE BEGIN", Logger::LogLocation( CLASSNAME, __func__ ) );
    stateName = SanitizeStateName( stateName );

    if ( HasActiveState() )
    {
//        DeactivateState(); // Will get deactivated during ChangeState.
    }

    bool error = false;

    if ( HasState( stateName ) )
    {
//        Logger::OutHighlight( "Have state \"" + stateName + "\"; try to change state. BEFORE", Logger::LogLocation( CLASSNAME, __func__ ) );
        error = ( ChangeState( stateName ) != true );
//        Logger::OutHighlight( "Have state \"" + stateName + "\"; try to change state. AFTER", Logger::LogLocation( CLASSNAME, __func__ ) );
    }
    else
    {
        error = true;
    }

    if ( error )
    {
        Messager::Set( "ERROR", "Error trying to change to state \"" + stateName + "\"!" );
        Logger::Error( Messager::Get( "ERROR" ), Logger::LogLocation( CLASSNAME, __func__ ) );
        ChangeState( "error" );
    }

//    Logger::OutHighlight( "Activate state \"" + stateName + "\".", Logger::LogLocation( CLASSNAME, __func__ ) );
    ActivateState();

//    Logger::OutHighlight( "CHANGE ACTIVE STATE END", Logger::LogLocation( CLASSNAME, __func__ ) );
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void StateManager::UpdateAndDraw( sf::RenderWindow& window )
{
    UpdateState();

    if ( GetGotoState() != "" )
    {
        ChangeActiveState( GetGotoState() );
    }

    DrawState( window );
}

}
