#ifndef _PATHMANAGER
#define _PATHMANAGER

#include "../Utilities/Platform.hpp"

#include <string>

class PathManager
{
    public:
    static std::string basePath;
    static void Setup();
    static void CreateConfigs();
    static std::string GetPath();
};

#endif
