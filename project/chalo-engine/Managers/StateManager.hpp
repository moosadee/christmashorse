// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _STATE_MANAGER
#define _STATE_MANAGER

#include <map>
#include "../States/IState.hpp"

namespace chalo
{

class StateManager
{
public:
    static std::string CLASSNAME;

    void InitManager();
    void Cleanup();

    void AddInactiveState( std::string stateName, IState* state );
    void ClearState( std::string stateName );
    bool ChangeState( std::string stateName );
    bool HasState( std::string stateName ) const;
    bool HasActiveState();
    void ActivateState();
    void DeactivateState();
    void SetupState();
    void UpdateState();
    void DrawState( sf::RenderWindow& window );
    std::string GetGotoState();

    void ChangeActiveState( std::string stateName );
    void UpdateAndDraw( sf::RenderWindow& window );

    std::string SanitizeStateName( std::string original ) const;

private:
    //std::string CLASSNAME;
    std::map< std::string, IState* > m_states;  // TODO: Update to new ptr
    IState* m_ptrCurrentState;                  // TODO: Update to new ptr
    std::string m_nameCurrentState;
};

}

#endif
