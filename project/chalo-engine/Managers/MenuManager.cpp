#include "MenuManager.hpp"

#include "../Application/Application.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities/CsvParser.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "FontManager.hpp"
#include "ConfigManager.hpp"
#include "LanguageManager.hpp"

#include <fstream>
#include <exception>

namespace chalo
{

std::string MenuManager::CLASSNAME = "MenuManager";
std::string MenuManager::m_menuPathBase;
std::map< std::string, UILayer > MenuManager::m_layers;
std::stack< std::string > MenuManager::m_menuStack;
UITextBox* MenuManager::m_ptrActiveTextbox;

void MenuManager::Setup( const std::string& pathBase )
{
    Logger::OutFuncBegin( "pathBase=" + pathBase, CLASSNAME + "::" + std::string( __func__ ) );
    m_menuPathBase = pathBase;
    m_ptrActiveTextbox = nullptr;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::Cleanup()
{
    m_layers.clear();
}

void MenuManager::DyslexiaFontReload()
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    for ( auto& layer : MenuManager::m_layers )
    {
        for ( auto& button : layer.second.buttons )
        {
            button.second.ToggleDyslexiaFont();
        }
        for ( auto& label : layer.second.labels )
        {
            label.second.ToggleDyslexiaFont();
        }
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

std::string MenuManager::GetClickedButton( bool disableOnClick /*= true*/ )
{
    if ( chalo::InputManager::IsLeftClickRelease() )
    {
        // TODO: Could call the GetHoveredButton here
        sf::Vector2i localPosition = chalo::InputManager::GetMousePosition();

        for ( auto& layer : MenuManager::m_layers )
        {
            if ( !layer.second.visible ) { continue; }
            for ( auto& button : layer.second.buttons )
            {
                if ( SFMLHelper::PointInBoxCollision( localPosition, button.second.GetPositionRect() ) )
                {
                    if ( disableOnClick ) { chalo::InputManager::DisableClick(); }
                    return button.second.GetId();
                }
            }
        }
    }

    return "";
}

std::string MenuManager::GetHoveredButton( sf::Vector2f position )
{
    for ( auto& layer : MenuManager::m_layers )
    {
        if ( !layer.second.visible ) { continue; }
        for ( auto& button : layer.second.buttons )
        {
            if ( SFMLHelper::PointInBoxCollision( position, button.second.GetPositionRect() ) )
            {
//                chalo::InputManager::DisableClick();
                return button.second.GetId();
            }
        }
    }

    return "";
}

std::string MenuManager::GetTextboxValue( const std::string& textboxName )
{
    for ( auto& layer : MenuManager::m_layers )
    {
        if ( !layer.second.visible ) { continue; }
        if ( layer.second.textboxes.find( textboxName ) != layer.second.textboxes.end() )
        {
            // Found
            return layer.second.textboxes[ textboxName ].GetEnteredText();
        }
    }

    return "";
}

void MenuManager::Update()
{
    InputManager::Update();

    // Check to see if any textboxes have been selected
    if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_ptrActiveTextbox = nullptr;
        sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

        for ( auto& layer : MenuManager::m_layers )
        {
            if ( !layer.second.visible ) { continue; }
            for ( auto& element : layer.second.textboxes )
            {
                if ( SFMLHelper::PointInBoxCollision( localPosition, element.second.GetPositionRect() ) )
                {
                    // This textbox has been selected
                    Logger::Out( "Textbox " + element.second.GetId() + " received focus", "MenuManager::Update" );
                    element.second.SetFocus( true );
                    m_ptrActiveTextbox = &element.second;
                }
                else
                {
                    element.second.SetFocus( false );
                }
            }
        }
    }

    // Check for keyboard presses
    if ( m_ptrActiveTextbox != nullptr )
    {
        // Input method :(
        if ( InputManager::IsKeyPressed( sf::Keyboard::BackSpace ) )
        {
            m_ptrActiveTextbox->EraseCharacter();
        }

        std::string typedCharacter = InputManager::GetTypedLetter();
        if ( typedCharacter != "" )
        {
            m_ptrActiveTextbox->AddCharacter( typedCharacter );
        }
    }
}

void MenuManager::LoadCsvMenu( const std::string& filename )
{
    Logger::OutFuncBegin( "filename=" + filename + ", m_menuPathBase=" + m_menuPathBase, CLASSNAME + "::" + std::string( __func__ ) );
    Cleanup();
    m_menuStack.push( filename );

    rach::CsvDocument data;

    try
    {
        data = rach::CsvParser::Parse( m_menuPathBase + filename );
    }
    catch( const runtime_error& ex )
    {
        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        Logger::Error( ex.what(), CLASSNAME + "::" + std::string( __func__ ) );
    }

    Logger::Out( "Pull data from CSV file \"" + m_menuPathBase + filename + "\"", CLASSNAME + "::" + std::string( __func__ ) );
    for ( auto& row : data.rows )
    {
        std::map<std::string, std::string> keyVals;

        std::string logInfo = "Row: <ul>";
        for ( size_t i = 0; i < row.size(); i++ )
        {
            if ( row[i] == "" ) { continue; }
            keyVals[ data.header[i] ] = row[i];
            logInfo += "<li>" + Helper::ToString( i ) + ": " + data.header[i] + "=" + row[i], CLASSNAME + "::" + std::string( __func__ ) + "</li>";
        }
        logInfo += "</ul>";

        if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
        {
          Logger::Out( logInfo, CLASSNAME + "::" + std::string( __func__ ) );
          Logger::Out( "Create UI object from data. Element type: " + keyVals["ELEMENT_TYPE"] + ", name: " + keyVals["ELEMENT_NAME"], CLASSNAME + "::" + std::string( __func__ ) );
        }

        keyVals["ELEMENT_TEXT"] = Helper::Replace( keyVals["ELEMENT_TEXT"], "\"", "" );

        InitLayer( keyVals["LAYER_NAME"] );
        if ( keyVals["ELEMENT_TYPE"] == "rectangleshape" )
        {
            AddRectangleShape( keyVals );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "button" )
        {
            AddButton( keyVals );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "label" )
        {
            AddLabel( keyVals );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "image" )
        {
            AddImage( keyVals );
        }
        else if ( keyVals["ELEMENT_TYPE"] == "textbox" )
        {
            AddTextBox( keyVals );
        }
    }

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::AddRectangleShape( std::map<std::string, std::string>& keyVals )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );

    std::string elementName = keyVals["ELEMENT_NAME"];
    std::string layerName = keyVals["LAYER_NAME"];
    sf::Color borderColor = GetColorOrDefault( keyVals[ "ELEMENT_BORDER_COLOR_R" ], keyVals[ "ELEMENT_BORDER_COLOR_G" ], keyVals[ "ELEMENT_BORDER_COLOR_B" ], keyVals[ "ELEMENT_BORDER_COLOR_A" ], sf::Color::Black );
    sf::Color fillColor = GetColorOrDefault( keyVals[ "ELEMENT_FILL_COLOR_R" ], keyVals[ "ELEMENT_FILL_COLOR_G" ], keyVals[ "ELEMENT_FILL_COLOR_B" ], keyVals[ "ELEMENT_FILL_COLOR_A" ], sf::Color::Black );
    int borderThickness = Helper::StringToInt( keyVals["ELEMENT_BORDER_THICKNESS"] );
    sf::Vector2f position = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] ) );
    sf::Vector2f dimensions = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_WIDTH" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_HEIGHT" ] ) );

    UIRectangleShape rectangle;
    rectangle.Setup( elementName, borderColor, borderThickness, fillColor, position, dimensions );

    if ( Logger::GetLogItems().logMenuElements )
    {
        Logger::Out( "Adding new RectangleShape element: [" + layerName + "][" + elementName + "]", Logger::LogLocation( CLASSNAME, __func__ ), "file-parsing" );
    }

    m_layers[layerName].shapes[elementName] = rectangle;

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddButton( std::map<std::string, std::string>& keyVals )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );

    bool asciiText = false;
    std::string elementText = keyVals["ELEMENT_TEXT"];
    std::string fontName = "";
    CreateElementText( elementText, asciiText, fontName );
    if ( fontName == "" ) { fontName = keyVals["ELEMENT_TEXT_FONT"]; }
    bool visible = true;
    if ( keyVals[ "IS_VISIBLE" ] != "" )
    {
        visible = Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
    }

    std::string elementName = keyVals["ELEMENT_NAME"];
    std::string layerName = keyVals["LAYER_NAME"];

    sf::Vector2f position = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] ) );
    sf::IntRect dimensions = sf::IntRect( 0, 0, Helper::StringToInt( keyVals[ "ELEMENT_WIDTH" ] ), Helper::StringToInt( keyVals[ "ELEMENT_HEIGHT" ] ) );
    std::string backgroundTexture = keyVals[ "ELEMENT_BACKGROUND_TEXTURE" ];
    sf::Vector2f bgOffset = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_BACKGROUND_Y" ] ) );
    sf::IntRect frameRect = sf::IntRect( Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] ) );
    int textSize = Helper::StringToInt( keyVals[ "ELEMENT_TEXT_SIZE" ] );
    sf::Vector2f textOffset = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_Y" ] ) );
    sf::Color textColor = GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::White );

    std::string iconTexture = keyVals[ "ELEMENT_ICON_TEXTURE" ];
    sf::Vector2f iconOffset = sf::Vector2f( Helper::StringToInt( keyVals[ "ELEMENT_ICON_X" ] ), Helper::StringToInt( keyVals[ "ELEMENT_ICON_Y" ] ) );
    sf::IntRect iconClip = sf::IntRect( Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_X" ] ),
      Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_Y" ] ),
      Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_WIDTH" ] ),
      Helper::StringToInt( keyVals[ "ELEMENT_ICON_FRAME_HEIGHT" ] ) );

    UIButton button;

    button.Setup( elementName, position, dimensions );
    button.SetupBackground( chalo::TextureManager::Get( backgroundTexture ), bgOffset, frameRect );

    if ( elementText != "" )
    {
        button.SetOriginalFont( fontName );
        if ( asciiText )
        {
            button.SetupText( fontName, textSize, textColor, textOffset, elementText );
        }
        else
        {
            button.SetupTextW( fontName, textSize, textColor, textOffset, Helper::StringToWString( elementText ) );
        }
    }

    if ( iconTexture != "" )
    {
        button.SetupIcon( chalo::TextureManager::Get( iconTexture ), iconOffset, iconClip );
    }

    button.SetIsVisible( visible );

    if ( Logger::GetLogItems().logMenuElements )
    {
        Logger::Out( "Adding new Button element: [" + layerName + "][" + elementName + "]", Logger::LogLocation( CLASSNAME, __func__ ), "file-parsing" );
    }

    m_layers[layerName].buttons[elementName] = button;

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddLabel( std::map<std::string, std::string>& keyVals )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );

    bool asciiText = false;
    std::string elementText = keyVals["ELEMENT_TEXT"];
    std::string fontName = "";
    CreateElementText( elementText, asciiText, fontName );

    if ( fontName == "" ) { fontName = keyVals["ELEMENT_TEXT_FONT"]; }

    bool visible = true;
    if ( keyVals[ "IS_VISIBLE" ] != "" )
    {
        visible = Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
    }

    std::string layerName   = keyVals["LAYER_NAME"];
    std::string elementName = keyVals["ELEMENT_NAME"];
    int textSize          = Helper::StringToInt( keyVals[ "ELEMENT_TEXT_SIZE" ] );
    sf::Color textColor   = GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::White );
    sf::Vector2f position = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] ) );

    UILabel label;
    label.SetOriginalFont( fontName );
    if ( asciiText )
    {
        label.Setup( elementName, fontName, textSize, textColor, position, elementText );
    }
    else
    {
        label.SetupW( elementName, fontName, textSize, textColor, position, Helper::StringToWString( elementText ) );
    }
    label.SetIsVisible( visible );

    if ( Logger::GetLogItems().logMenuElements )
    {
        Logger::Out( "Adding new Label element: [" + layerName + "][" + elementName + "]", Logger::LogLocation( CLASSNAME, __func__ ), "file-parsing" );
    }

    m_layers[layerName].labels[elementName] = label;

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddImage( std::map<std::string, std::string>& keyVals )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );

    std::string elementName = keyVals["ELEMENT_NAME"];
    std::string layerName = keyVals["LAYER_NAME"];
    std::string textureName = keyVals["ELEMENT_BACKGROUND_TEXTURE"];
    sf::Vector2f position = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] ) );
    sf::IntRect frameRect = sf::IntRect( Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_X" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_Y" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_WIDTH" ] ), Helper::StringToInt( keyVals[ "ELEMENT_BACKGROUND_FRAME_HEIGHT" ] ) );

    bool visible = true;
    if ( keyVals[ "IS_VISIBLE" ] != "" )
    {
        visible = Helper::StringToInt( keyVals[ "IS_VISIBLE" ] );
    }

    UIImage image;

    image.Setup( elementName, chalo::TextureManager::Get( textureName ), position, frameRect );
    image.SetIsVisible( visible );

    if ( Logger::GetLogItems().logMenuElements )
    {
        Logger::Out( "Adding new Image element: [" + layerName + "][" + elementName + "]", Logger::LogLocation( CLASSNAME, __func__ ), "file-parsing" );
    }

    m_layers[layerName].images[elementName] = image;

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddTextBox( std::map<std::string, std::string>& keyVals )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );

    std::string elementName = keyVals["ELEMENT_TEXT"];
    std::string layerName = keyVals["LAYER_NAME"];

    sf::Vector2f position = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_Y" ] ) );
    sf::Vector2f textOffset = sf::Vector2f( Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_X" ] ), Helper::StringToFloat( keyVals[ "ELEMENT_TEXT_Y" ] ) );
    int textSize = Helper::StringToInt( keyVals[ "ELEMENT_TEXT_SIZE" ] );

    sf::IntRect dimensions = sf::IntRect( 0, 0, Helper::StringToInt( keyVals[ "ELEMENT_WIDTH" ] ), Helper::StringToInt( keyVals[ "ELEMENT_HEIGHT" ] ) );
    sf::Color borderColor = GetColorOrDefault( keyVals[ "ELEMENT_BORDER_COLOR_R" ], keyVals[ "ELEMENT_BORDER_COLOR_G" ], keyVals[ "ELEMENT_BORDER_COLOR_B" ], keyVals[ "ELEMENT_BORDER_COLOR_A" ], sf::Color::Black );
    int borderThickness = Helper::StringToInt( keyVals["ELEMENT_BORDER_THICKNESS"] );
    sf::Color fillColor = GetColorOrDefault( keyVals[ "ELEMENT_FILL_COLOR_R" ], keyVals[ "ELEMENT_FILL_COLOR_G" ], keyVals[ "ELEMENT_FILL_COLOR_B" ], keyVals[ "ELEMENT_FILL_COLOR_A" ], sf::Color::White );
    sf::Color textColor = GetColorOrDefault( keyVals[ "ELEMENT_TEXT_COLOR_R" ], keyVals[ "ELEMENT_TEXT_COLOR_G" ], keyVals[ "ELEMENT_TEXT_COLOR_B" ], keyVals[ "ELEMENT_TEXT_COLOR_A" ], sf::Color::Black );

    bool asciiText = false;
    std::string elementText = keyVals["ELEMENT_TEXT"];
    std::string fontName = "";
    CreateElementText( elementText, asciiText, fontName );

    UITextBox textbox;
    textbox.Setup( elementName, position, dimensions );
    textbox.SetupBackground( borderColor, borderThickness, fillColor, position, sf::Vector2f( dimensions.width, dimensions.height ) );
    textbox.SetupText( fontName, textSize, textColor, textOffset, elementText );

    if ( Logger::GetLogItems().logMenuElements )
    {
        Logger::Out( "Adding new TextBox element: [" + layerName + "][" + elementName + "]", Logger::LogLocation( CLASSNAME, __func__ ), "file-parsing" );
    }

    m_layers[layerName].textboxes[elementName] = textbox;

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::InitLayer( std::string key )
{
    if ( m_layers[key].name == "" )
    {
        m_layers[key].name = key;
        m_layers[key].visible = true;
    }
}

bool MenuManager::GoBack()
{
    if ( m_menuStack.size() > 1 )
    {
        m_menuStack.pop();
        LoadTextMenu( m_menuStack.top() );
        return true;
    }

    return false;
}

UILabel& MenuManager::GetLabel( const std::string& layer, const std::string& name )
{
    return m_layers[layer].labels[name];
}

UILabel& MenuManager::GetLabel( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.labels.find( name ) != layer.second.labels.end() )
        {
            return layer.second.labels[name];
        }
    }

    Logger::Error( "Cannot find label with key \"" + name + "\"", "MenuManager::GetLabel" );
    throw std::out_of_range( "Cannot find label with key \"" + name + "\"" );
}

UIImage& MenuManager::GetImage( const std::string& layer, const std::string& name )
{
    return m_layers[layer].images[name];
}

UIImage& MenuManager::GetImage( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.images.find( name ) != layer.second.images.end() )
        {
            return layer.second.images[name];
        }
    }

    Logger::Error( "Cannot find image with key \"" + name + "\"", "MenuManager::GetImage" );
    throw std::out_of_range( "Cannot find image with key \"" + name + "\"" );
}

UIButton& MenuManager::GetButton( const std::string& layer, const std::string& name )
{
    return m_layers[layer].buttons[name];
}

UIButton& MenuManager::GetButton( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.buttons.find( name ) != layer.second.buttons.end() )
        {
            return layer.second.buttons[name];
        }
    }

    Logger::Error( "Cannot find button with key \"" + name + "\"", "MenuManager::GetButton" );
    throw std::out_of_range( "Cannot find button with key \"" + name + "\"" );
}

UIRectangleShape& MenuManager::GetRectangleShape( const std::string& layer, const std::string& name )
{
    Logger::OutValue( "layer", layer );
    Logger::OutValue( "name", name );
    if ( m_layers.find( layer ) == m_layers.end() )
    {
        Logger::Error( "Layer \"" + layer + "\" not found!", "MenuManager::GetRectangleShape" );
        throw std::out_of_range( "Layer \"" + layer + "\" not found!" );
    }

    try
    {
        return m_layers.at( layer ).shapes.at( name );
    }
    catch( const std::logic_error& ex )
    {
        std::string error = std::string( "Logic error: " ) + ex.what();
        Logger::Error( error, "MenuManager::GetRectangleShape" );
        Logger::Error( "layer: \"" + layer + "\", name: \"" + name + "\"", "MenuManager::GetRectangleShape" );
    }
}

UIRectangleShape& MenuManager::GetRectangleShape( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.shapes.find( name ) != layer.second.shapes.end() )
        {
            return layer.second.shapes[name];
        }
    }

    Logger::Error( "Cannot find shape with key \"" + name + "\"", "MenuManager::GetRectangleShape" );
    throw std::out_of_range( "Cannot find shape with key \"" + name + "\"" );
}

UITextBox& MenuManager::GetTextBox( const std::string& layer, const std::string& name )
{
    return m_layers[layer].textboxes[name];
}

UITextBox& MenuManager::GetTextBox( const std::string& name )
{
    for ( auto& layer : m_layers )
    {
        if ( layer.second.textboxes.find( name ) != layer.second.textboxes.end() )
        {
            return layer.second.textboxes[name];
        }
    }

    Logger::Error( "Cannot find textbox with key \"" + name + "\"", "MenuManager::GetTextBox" );
    throw std::out_of_range( "Cannot find textbox with key \"" + name + "\"" );
}

void MenuManager::ShowAllLayers()
{
    for ( auto& layer : m_layers )
    {
        layer.second.visible = true;
    }
}


void MenuManager::HideAllLayers()
{
    for ( auto& layer : m_layers )
    {
        layer.second.visible = false;
    }
}

void MenuManager::TurnOnLayers( std::vector< std::string > layerNames )
{
    for ( auto& layer : m_layers )
    {
        for ( auto& name : layerNames )
        {
            if ( name == layer.second.name )
            {
                layer.second.visible = true;
            }
        }
    }
}

void MenuManager::TurnOffLayers( std::vector< std::string > layerNames )
{
    for ( auto& layer : m_layers )
    {
        for ( auto& name : layerNames )
        {
            if ( name == layer.second.name )
            {
                layer.second.visible = false;
            }
        }
    }
}

std::vector< std::string > MenuManager::GetVisibleLayerNames()
{
    std::vector< std::string > visibleLayers;

    for ( auto& layer : m_layers )
    {
        if ( layer.second.visible )
        {
            visibleLayers.push_back( layer.second.name );
        }
    }

    return visibleLayers;
}

bool MenuManager::IsLayerVisible( std::string name )
{
    if ( m_layers.find( name ) == m_layers.end() )
    {
        return false;
    }

    return ( m_layers[ name ].visible );
}

sf::Color MenuManager::GetColorOrDefault( std::string r, std::string g, std::string b, std::string a, sf::Color def )
{
    if ( r == "" ) { return def; }

    int num_r = 0;
    int num_g = 0;
    int num_b = 0;
    int num_a = 0;

    try
    {
      num_r = ( r == "" ) ? 0 : Helper::StringToInt( r );
      num_g = ( g == "" ) ? 0 : Helper::StringToInt( g );
      num_b = ( b == "" ) ? 0 : Helper::StringToInt( b );
      num_a = ( a == "" ) ? 0 : Helper::StringToInt( a );
    }
    catch( const invalid_argument& ex )
    {
      Logger::Error( "Exception caught: " + std::string( ex.what() ), CLASSNAME + "::" + std::string( __func__ ) );
    }

    return sf::Color( num_r, num_g, num_b, num_a );
}

void MenuManager::CreateElementText( std::string& elementText, bool& asciiText, std::string& fontName )
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    asciiText = false; // Just load everything to work with unicode??

    if ( elementText == "" )
    {
        // Nothing to build
        Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }
    elementText = Helper::Trim( elementText );

    if ( Helper::Contains( elementText, "<<" ) )
    {
        // Replace variable values with config data
        std::string original = elementText;
        int varStart = elementText.find( "<<" ) + 2;
        int varEnd = elementText.find( ">>" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = ConfigManager::Get( variableName );
        elementText = Helper::Replace( elementText, "<<" + variableName + ">>", value );
    }
    else if ( Helper::Contains( elementText, "{{" ) )
    {
        // Replace variable values with language items
        std::string original = elementText;
        int varStart = elementText.find( "{{" ) + 2;
        int varEnd = elementText.find( "}}" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), variableName );
        elementText = Helper::Replace( elementText, "{{" + variableName + "}}", value );

        // Change font
        fontName = chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" );
    }
    else if ( Helper::Contains( elementText, "[[" ) )
    {
        // Replace variable values with language items
        std::string original = elementText;
        int varStart = elementText.find( "[[" ) + 2;
        int varEnd = elementText.find( "]]" );
        int length = varEnd - varStart;
        std::string variableName = elementText.substr( varStart, length );
        std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), variableName );
        elementText = Helper::Replace( elementText, "[[" + variableName + "]]", value );

        // Change font
        fontName = chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" );
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

// DEPRECATED

void MenuManager::LoadTextMenu( const std::string& filename )
{
    Logger::Error( "THIS FUNCTION IS DEPRECATED! USE LoadCsvMenu INSTEAD!", CLASSNAME + "::" + std::string( __func__ ) );
}

void MenuManager::AddLabel( const std::string& layerName, const std::string& elementName, sf::Color textColor, const std::string& fontName, int textSize, sf::Vector2f position, const std::string& elementText, bool asciiText, bool visible )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddRectangleShape( const std::string& layerName, const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f dimensions )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddButton( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions, std::string backgroundTexture, sf::Vector2f bgOffset, sf::IntRect frameRect, std::string fontName, int textSize, sf::Color textColor, sf::Vector2f textOffset, std::string elementText, bool asciiText, std::string iconTexture, int iconX, int iconY, int iconLeft, int iconTop, int iconWidth, int iconHeight, bool visible /* = true */ )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddButton( const std::string& layerName, const std::string& elementName, UIButton button )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddImage( const std::string& layerName, const std::string& elementName, const std::string& textureName, sf::Vector2f position, sf::IntRect frameRect, bool visible )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void MenuManager::AddTextBox( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Color textColor, const std::string& fontName, int textSize, sf::Vector2f textOffset, const std::string& elementText )
{
    Logger::Error( "DEPRECATED FUNCTION", Logger::LogLocation( CLASSNAME, __func__ ) );
}

}
