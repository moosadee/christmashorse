// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _UIIMAGE_HPP
#define _UIIMAGE_HPP

#include "IWidget.hpp"

namespace chalo
{

// Specify a sprite for the background. Otherwise, default to colored rectangle
class UIImage : public IWidget
{
public:
    UIImage();
    virtual ~UIImage()
    {
        ;
    }

    void Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position );
    void Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position, sf::IntRect imageClip );
    void SetPosition( const sf::Vector2f& pos );

    sf::Sprite& GetSprite();

    sf::IntRect GetImageClipRect();
    void SetImageClipRect( sf::IntRect imageClip );

protected:
    sf::Sprite m_sprite;
};

}

#endif

