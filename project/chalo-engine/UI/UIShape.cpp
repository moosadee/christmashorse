// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "UIShape.hpp"

namespace chalo
{

UIRectangleShape::UIRectangleShape() : IWidget()
{
}

void UIRectangleShape::Setup( const std::string& key, sf::RectangleShape shape )
{
    m_shape = shape;
}

void UIRectangleShape::Setup( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size )
{
    IWidget::Setup( key );
    m_shape.setFillColor( fillColor );
    m_shape.setOutlineColor( borderColor );
    m_shape.setOutlineThickness( borderThickness );
    m_shape.setPosition( position );
    m_shape.setSize( size );
}

sf::RectangleShape UIRectangleShape::GetShape()
{
    return m_shape;
}

void UIRectangleShape::SetFillColor( sf::Color color )
{
    m_shape.setFillColor( color );
}

void UIRectangleShape::SetPosition( const sf::Vector2f& pos )
{
    m_shape.setPosition( pos );

}

}
