// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _UISHAPE_HPP
#define _UISHAPE_HPP

#include <string>

#include <SFML/Graphics.hpp>

#include "IWidget.hpp"

namespace chalo
{

class UIRectangleShape : public IWidget
{
public:
    UIRectangleShape();
    virtual ~UIRectangleShape()
    {
        ;
    }

    void Setup( const std::string& key, sf::RectangleShape shape );
    void Setup( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size );
    void SetFillColor( sf::Color color );
    void SetPosition( const sf::Vector2f& pos );

    sf::RectangleShape GetShape();
protected:
    sf::RectangleShape m_shape;
};

}

#endif

