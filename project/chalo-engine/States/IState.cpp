// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "IState.hpp"

#include "../Utilities/Logger.hpp"

namespace chalo
{

void IState::Init( const std::string& name )
{
    m_name = name;
    m_gotoState = "";
}

void IState::Setup()
{
    m_gotoState = "";
}

std::string IState::GetName()
{
    return m_name;
}

std::string IState::GetGotoState()
{
    return m_gotoState;
}

void IState::SetGotoState( const std::string& stateName )
{
    Logger::OutFuncBegin( "normal", "IState::" + std::string( __func__ ) );
    m_gotoState = stateName;
    Logger::OutValue( "m_gotoState", m_gotoState, "IState::" + std::string( __func__ ) );
    Logger::OutFuncEnd( "normal", "IState::" + std::string( __func__ ) );
}

}
