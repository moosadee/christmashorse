#ifndef _ERRORSTATE
#define _ERRORSTATE

#include "CursorState.hpp"
#include "../Managers/TextureManager.hpp"
#include "../Managers/FontManager.hpp"

#include <SFML/Audio.hpp>

#include <vector>

namespace chalo
{

class ErrorState : public CursorState
{
public:
    ErrorState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );
};

}

#endif
