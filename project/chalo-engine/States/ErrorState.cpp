#include "ErrorState.hpp"

#include "../Application/Application.hpp"
#include "../Managers/MenuManager.hpp"
#include "../Managers/InputManager.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Utilities/Messager.hpp"

namespace chalo
{

ErrorState::ErrorState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void ErrorState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    m_name = name;
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void ErrorState::Setup()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "bg",                      "Engine/Content/Graphics/UI/menubg.png" );
    chalo::TextureManager::Add( "button-long",             "Engine/Content/Graphics/UI/button-long.png" );

    std::map<std::string, std::string> labelProperties = {
      std::pair<std::string, std::string>( "ELEMENT_NAME",                "ohno1" ),
      std::pair<std::string, std::string>( "LAYER_NAME",                  "1" ),
      std::pair<std::string, std::string>( "ELEMENT_TYPE",                "label" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT",                "An error happened!" ),
      std::pair<std::string, std::string>( "ELEMENT_X",                   "10" ),
      std::pair<std::string, std::string>( "ELEMENT_Y",                   "10" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_FONT",           "main" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_SIZE",           "20" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_R",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_G",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_B",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_A",              "255" )
    };
    MenuManager::AddLabel( labelProperties );

    labelProperties = {
      std::pair<std::string, std::string>( "ELEMENT_NAME",                "ohno2" ),
      std::pair<std::string, std::string>( "LAYER_NAME",                  "1" ),
      std::pair<std::string, std::string>( "ELEMENT_TYPE",                "label" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT",                "Error message: \"" + Messager::Get( "ERROR" ) + "\"" ),
      std::pair<std::string, std::string>( "ELEMENT_X",                   "10" ),
      std::pair<std::string, std::string>( "ELEMENT_Y",                   "35" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_FONT",           "main" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_SIZE",           "20" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_R",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_G",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_B",              "255" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_A",              "255" )
    };
    MenuManager::AddLabel( labelProperties );

    std::map<std::string, std::string> buttonProperties = {
      std::pair<std::string, std::string>( "ELEMENT_NAME",                "btnBack" ),
      std::pair<std::string, std::string>( "LAYER_NAME",                  "1" ),
      std::pair<std::string, std::string>( "ELEMENT_TYPE",                "button" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT",                "Back to start" ),
      std::pair<std::string, std::string>( "ELEMENT_BACKGROUND_TEXTURE",  "button-long" ),
      std::pair<std::string, std::string>( "ELEMENT_X",                   "10" ),
      std::pair<std::string, std::string>( "ELEMENT_Y",                   "650" ),
      std::pair<std::string, std::string>( "ELEMENT_WIDTH",               "200" ),
      std::pair<std::string, std::string>( "ELEMENT_HEIGHT",              "50" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_X",              "5" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_Y",              "5" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_FONT",           "main" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_SIZE",           "20" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_R",              "0" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_G",              "0" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_B",              "0" ),
      std::pair<std::string, std::string>( "ELEMENT_TEXT_A",              "255" )
    };
    MenuManager::AddButton( buttonProperties );

    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void ErrorState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void ErrorState::Update()
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "StartupState" );
    }
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

void ErrorState::Draw( sf::RenderWindow& window )
{
    Logger::OutFuncBegin( "Function begin", Logger::LogLocation( CLASSNAME, __func__ ) );
    chalo::DrawManager::AddMenu();
    Logger::OutFuncEnd( "normal", Logger::LogLocation( CLASSNAME, __func__ ) );
}

}

