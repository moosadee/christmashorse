//#include "MapEditorState.hpp"
//
//#include "../Managers/MenuManager.hpp"
//#include "../Managers/InputManager.hpp"
//#include "../Managers/ConfigManager.hpp"
//#include "../Utilities/Messager.hpp"
//#include "../Application/Application.hpp"
//#include "../Data/ConfigLoader.hpp"
//
//MapEditorState::MapEditorState()
//{
//}
//
//void MapEditorState::Init( const std::string& name )
//{
//    Logger::OutFuncBegin( "name=" + name, std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//    CursorState::Init( name, true );
//    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//}
//
//void MapEditorState::Setup()
//{
//    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//    IState::Setup();
//    CursorState::Setup();
//    chalo::TextureManager::Add( "bg",           "../Content/Graphics/UI/mapeditor-bg.png" );
//    chalo::TextureManager::Add( "mapeditor-ui", "../Content/Graphics/UI/mapeditor-ui.png" );
//
//    std::map<std::string, std::string> textures = chalo::LoadConfig( "chalo_mapeditor.chacha" );
//
//    m_mapName = "working.csv";
//    InitNewMap();
//
//    for ( auto& tx : textures )
//    {
//        Logger::Out( "key=\"" + tx.first + "\", path=\"" + tx.second + "\"", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//        chalo::TextureManager::Add( tx.first, tx.second );
//    }
//
//    chalo::MenuManager::LoadCsvMenu( "mapeditor.csv" );
//    OpenTab( "Settings" );
//
//    chalo::InputManager::Setup();
//    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//}
//
//void MapEditorState::Cleanup()
//{
//    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//    chalo::MenuManager::Cleanup();
//    chalo::DrawManager::Reset();
//    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//}
//
//void MapEditorState::Update()
//{
//    CursorState::Update();
//    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );
//
//    if      ( clickedButton == "btnSettingsTab" )   { OpenTab( "Settings" ); }
//    else if ( clickedButton == "btnFloors1Tab" )    { OpenTab( "Floors1" ); }
//    else if ( clickedButton == "btnFloors2Tab" )    { OpenTab( "Floors2" ); }
//    else if ( clickedButton == "btnWalls1Tab" )     { OpenTab( "Walls1" ); }
//    else if ( clickedButton == "btnWalls2Tab" )     { OpenTab( "Walls2" ); }
//    else if ( clickedButton == "btnItems1Tab" )     { OpenTab( "Items1" ); }
//    else if ( clickedButton == "btnItems2Tab" )     { OpenTab( "Items2" ); }
//    else if ( clickedButton == "btnEventsTab" )     { OpenTab( "Events" ); }
//    else if ( clickedButton == "btnCollisionTab" )  { OpenTab( "Collision" ); }
//    else if ( clickedButton == "btnShadowsTab" )    { OpenTab( "Shadows" ); }
//
//    CheckClickTools();
//}
//
//void MapEditorState::Draw( sf::RenderWindow& window )
//{
//    chalo::DrawManager::AddMenu();
//    CursorState::Draw( window );
//}
//
//void MapEditorState::ResetTabs()
//{
//    std::vector< std::string > layerNames = { "Settings", "Floors", "Walls", "Items", "Events", "Collision", "Shadows" };
//    std::vector< std::string > buttonNames = { "Settings", "Floors1", "Floors2", "Walls1", "Walls2", "Items1", "Items2", "Events", "Collision", "Shadows" };
//    // Hide layers
//    // Change all tabs to "not active" image
//    for ( auto& name : layerNames )
//    {
//        std::string layerName = "tab" + name;
//        chalo::MenuManager::TurnOffLayers( { layerName } );
//    }
//    for ( auto& name : buttonNames )
//    {
//        std::string buttonName = "btn" + name + "Tab";
//        chalo::MenuManager::GetButton( "main", buttonName ).SetupBackgroundImageClipRect( sf::IntRect( 32, 30, 100, 30 ) );
//    }
//}
//
//void MapEditorState::OpenTab( std::string name )
//{
//    ResetTabs();
//    m_activeTab = name;
//    std::string layerName = "tab" + name;
//    if      ( name == "Floors1" || name == "Floors2" ) { layerName = "tabFloors"; }
//    else if ( name == "Walls1" || name == "Walls2" ) { layerName = "tabWalls"; }
//    else if ( name == "Items1" || name == "Items2" ) { layerName = "tabItems"; }
//    chalo::MenuManager::GetButton( "main", "btn" + m_activeTab + "Tab" ).SetupBackgroundImageClipRect( sf::IntRect( 32, 0, 100, 30 ) );
//    chalo::MenuManager::TurnOnLayers( { layerName } );
//}
//
//void MapEditorState::CheckClickTools()
//{
//    // TODO: Check which tab we're on. Check location of tileset we're clicking on. Set the brush texture & intrect to that region.
//    // I am at my sister's bday party and it's loud and I cannot concentrate so I guess I'm calling it here.
//
//    bool leftClickHappening = chalo::InputManager::IsLeftClick();
//    if ( leftClickHappening )
//    {
//        sf::Vector2i mousePosition = chalo::InputManager::GetMousePosition();
//        std::vector<std::string> visibleTabs = chalo::MenuManager::GetVisibleLayerNames();
////        if ( Helper::Contains( visibleTabs, "tabWalls1" ) )
////        {
////        }
//        Logger::Out( "visibleTabs:" + Helper::VectorToString( visibleTabs ), std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
//    }
//}
//
//void MapEditorState::SetupTileset( std::string name )
//{
//}
//
//void MapEditorState::InitNewMap()
//{
//}
//
//void MapEditorState::LoadMap()
//{
//}
//
//void MapEditorState::SaveMap()
//{
//}
//
//void MapEditorState::SetBrush( sf::IntRect imageClip )
//{
//}
//
////    chalo::TextureManager::Add( "floors",       "../Content/Graphics/Tilesets/floors.png" );
////    chalo::TextureManager::Add( "items",        "../Content/Graphics/Tilesets/items.png" );
////    chalo::TextureManager::Add( "walls",        "../Content/Graphics/Tilesets/walls.png" );
////    chalo::TextureManager::Add( "shadows",      "../Content/Graphics/Tilesets/shadows.png" );
//
//
//
