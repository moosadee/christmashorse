//// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine
//
//#ifndef _WRITABLEMAP_MAP_HPP
//#define _WRITABLEMAP_MAP_HPP
//
//#include "ReadableMap.hpp"
//#include "TilePlacementInfo.hpp"
//
//namespace chalo
//{
//
//namespace Map
//{
//
//class WritableMap : public ReadableMap
//{
//public:
//    void SaveTextMap( const std::string& mapPath, const std::string& filename );
//    void LoadTextMap( const std::string& mapPath, const std::string& filename );
//    void SetMapDimensions( int width, int height );
//    sf::IntRect GetMapRegion();
//
//    void Reset();
//    void Undo();
//    int GetTileCount( LayerType tileType = UNDEFINEDLAYER );
//    int GetUndoSize();
//    void RemoveTiles( int layer, sf::IntRect region, LayerType tileType = BELOWTILELAYER );
//
//    // Map updates
//    void AddTile( int layer, Map::Tile& tile, LayerType tileType = BELOWTILELAYER );
//    void AddShadow( int layer, Map::Shadow tile );
//    void AddCollision( Map::Collision collision );
//    void AddWarp( Map::Warp warp );
//    void AddPlacement( Map::Placement placement );
//
//    void SetTilesets(
//        const std::string& tilesetName,
//        const std::string& tilesetNameCollisions,
//        const std::string& tilesetNameShadows,
//        const std::string& tilesetNamePlacements
//    );
//
//protected:
//    // Undo stuff
//    void PushTilePlacementInfo( TilePlacementInfo info );
//    std::stack<TilePlacementInfo> m_lastMapUpdate;
//};
//
//}
//
//}
//
//#endif
